# Flexible Job-Shop Scheduling Simulation

This is an overview of the ``job-shop-simulator`` project. The repository of this project can be found here: [Job Shop Simulator, RWTH Gitlab](https://git.rwth-aachen.de/andreas.schmitz3/job-shop-simulator).

For more information, see the ``docs`` folder which contains a detailed documentation of the different project components. The documentation was setup using the Sphinx documentation generator.

Also, see the [Job Shop Evaluator, RWTH Gitlab](https://git.rwth-aachen.de/andreas.schmitz3/job-shop-evaluator) project for an evaluation tool that uses this simulator. The ``job-shop-evaluator`` project is separated from this project due to licensing reasons.

The project was developed for Python 3.6+ only, and it may not run properly with older Python 3 version. Most likely, it is not going to run with Python 2.7 at all.

## Installation

### Virtual Environment (Optional, Recommended)
If you want to use a virtual environment instead of installing the code globally, do:

  1. Create a Virtual Environment: ``python3.6 -m venv .venv``
  2. Activate environment: `source .venv/bin/activate` (To Deactivate environment run: deactivate)

Exit Virtual Environment

  1. `deactivate`


### Installation from Source
With a proper Python installation, the application can be installed as follows:

  - ``pip install .`` (recommended) or ``python setup.py install``
  - ``pip install -e .[dev,test]`` (editable mode including development and test dependencies)
  - ``pip install -e .`` (editable mode without development and test dependencies)

#### Linux
Most Linux versions bundle the Python 3.6 development files in a package that needs to be installed separately. It should be sufficient to install the packages: ``python3.6`` and ``python3.6-dev``.
It is also quite common that the virtual environment module is not part of the base installation, and part of a separate package, called  ``python3.6-venv``.

Furthermore, some Linux Python 3.6 installations do not pre-install the ``wheel`` Python package, which is required by some dependencies. Make sure that the ``wheel`` package is installed, e.g. by checking ``pip list``. It can be installed with ``pip install wheel``.

#### macOS
Simply install Python 3.6 with homebrew, via: ``brew install python3.6``.


### Dependency Problems

If the dependencies of the program could not be resolved for whatever reason, a tarball of every dependency can be found in the `dependency-backup` folder.


## Execution

The different scripts of this project are used to create or import test instances and start and visualize simulations. All scripts work on a common SQLite database which is created automatically and stored in application specific folder. The location depends on the operating system. If so desired, an arbitrary location can be provided with the ``--database=`` parameter. For more information, and the default location, start an arbitrary script with the ``--help`` parameter.

Note: Unfortunately, some parameters of the ``job-shop-simulator`` need an equal sign (=) after the parameter. That's because the ``job-shop-simulator`` script needs to use the option parser of the tornado framework.

### Scripts Overview

  - ``job-shop-simulator``: starts a web-server that allows to create new shops, start simulations and visualize simulation results. The default URL is: <http://localhost:8888>. The default user account is: **fjssp:fjssp$2017&RWTH**. To manage the user accounts, use the ``mange-simulator`` tool. Use the ``--help`` option for more information.
  - ``manage-simulator``: is used to create, delete and update user accounts for the ``job-shop-simulator`` web UI. See ``--help`` for more information.
  - ``new-simulation``: allows to create and run new simulation runs of different strategies. It is completely independent of the ``job-shop-simulator`` script, although both can use the same SQLite database (which they do by default). See ``--help`` for more information.
  - ``generate-shop``: generates new test instances (shops) that can be used by the ``job-shop-simulator`` or ``new-simulation`` tool to run simulations on. See ``--help`` for more information.
  - ``import-shop``: allows to import standard [OR-Lib](http://people.brunel.ac.uk/~mastjjb/jeb/info.html) job-shop, or OR-Lib compatible test instances, for example: <http://edoc.sub.uni-hamburg.de/hsu/volltexte/2012/2982/>. See ``--help`` for more information.

### New Simulation Examples

#### Example 1: Generated Shop

  - Create a new shop with 50 jobs, 5 machines and an operation flexibility between 2 and 3 with: ``generate-shop 50 5 2 3``.
    The output should return the id of the generated shop, e.g. 1.
  - Start a new simulation on the newly generated shop using the ``new-simulation`` script and the previously returned shop id (here 1).
    For example: ``new-simulation --simulation ShortestProcessingTime '{}' OperatorCommunication '{"scheduler_name":"Ziaee"}' --simulation ShortestProcessingTime '{}' MachineTimeout '{"timeout":5}' --distribution-fixed-chunk-size 30 --time-slice-fixed 60 --selection-percentage "1" --shop-id 1``.

#### Example 2: Imported Shop

  - Import a new academic test instance. E.g. ``import-shop <path-to-instance>``
    The output should return the id of the imported shop, e.g. 2.
  - Use the id of the imported shop to start a new simulation.
    For example: ``new-simulation --simulation ShortestProcessingTime '{}' OperatorCommunication '{"scheduler_name":"Ziaee"}' --simulation ShortestProcessingTime '{}' MachineTimeout '{"timeout":5}' --simulation FirstInFirstOut '{}' NoCommunication '{}' --distribution-fixed-chunk-size 1 --time-slice-poisson 50 --selection-percentage "1" --shop-id 2``
