# -*- coding: utf-8 -*-
"""Modules used to create a simulation environment and test the different
algorithmic approaches to simulate a Flexible Job-Shop Scheduling Problem
on real-world data.

"""
