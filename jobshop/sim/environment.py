# -*- coding: utf-8 -*-
"""This module contains the simulation environment that can execute multiple
strategies. It takes care of setting up the and tearing down everythong
necessary. For further information see the documentation of the
`SimulationEnvironment` class.


"""

import simpy
import logging
import random
from collections import namedtuple

from jobshop.sim.job_management import JobDistributor, JobSelector
from jobshop.sim.scheduler import SchedulerFactory
from jobshop.sim.executor import ExecutorFactory
from jobshop.sim.deviation import Deviation
from jobshop.common.database import CommonDatabase
from jobshop.common.result_data import SimulationMetadata


class SimulationEnvironment:
    """The `SimulationEnvironment` class is used to coordinate the different
    parts of the simulation e.g. selection of jobs, distribution of jobs,
    creating schedules and executing them. One `SimulationEnvironment` takes
    care of multiple `~jobshop.sim.scheduler.Scheduler` and `Executor`
    objects using exactly one job selection and distribution strategy. Those
    strategies are carried out by `~jobshop.sim.job_management.JobSelector`
    and `~jobshop.sim.job_management.JobDistributor` objects. which are
    created by the `SimulationEnvironment` object itself.

    `~jobshop.sim.scheduler.Scheduler` and `Executor` objects are also
    created by the `SimulationEnvironment` object. This is done via Abstract
    factory classes.

    To configure, the simulation environment, the following optional parameters
    can be used.

    An exhausted list of possible parameters and their default values can be
    found in the following:

    ==================  =================  =====================================
    **Parameter**        **Default**       **Remark**
    ------------------  -----------------  -------------------------------------
    `name`              `""`               An optional name for the simulation
                                           run.
    `seed`              `random.\          int value used to directly initialize
                        getrandbits(128)`  the random.Random object.

                                           If set to `None`, the default value
                                           is used.
    `database`          CommonDatabase(\   The database object used in the
                        tempfile)          simulation.

                                           By default, a new temp database is
                                           created and the schema is loaded.

                                           tempfile is generated via
                                           ``tempfile.mkstemp()[1]``.

    `shop_id`           1                  The shop (a collection of jobs) in
                                           the database that is used for the
                                           simulation.

    `no_deviations`     False              Whether resource performance
                                           deviations should be used or not.

    `job_selector`      `{'strategy': 1,   The parameters used with the
                        'absolute': 10,    job_names() function of the
                        'random': True}`   JobSelector class.

                                           Check the documentation there for
                                           further information.
    `job_time_slice`    `{'time_slice':    The values used for the `time_slice`
                        0, 'duration':     configuration of the JobDistributor
                        480}`              class.

                                           For more information, check the
                                           documentation of the JobDistributor
                                           class.
    `job_distribution`  `{'distribution':  The values used for the
                        1, 'chunk_size':   `distribution` configuration of the
                        3}`                JobDistributor class.

                                           For more information, check the
                                           documentation of the JobDistributor
                                           class.
    ==================  =================  =====================================

    Args:
        **settings: Setting arguments to configure the simulation environment
            for a complete list of parameters, see the table above.

    Attributes:
        name (str): An optional name for the simulation run.

    Raises:
        KeyError: if the provided settings dictionary has invalid fields.

    """
    def __init__(self, **settings):
        self._simulations = []
        self._env = simpy.Environment()
        self._simulation_metadata = SimulationMetadata()
        self._scheduled_operations = set()

        # Configure attributes
        self.name = settings.get('name', "")
        self._database = settings.get('database', CommonDatabase())
        self._shop_id = settings.get('shop_id', 1)
        self._seed = settings.get('seed') or random.getrandbits(128)
        self._no_deviations = settings.get('no_deviations', False)
        # Job selector related attributes
        job_sel_dict = settings.get('job_selector', {'strategy': 1, 'absolute': 10, 'random': True})
        self._job_selector = job_sel_dict['strategy']
        self._job_selector_parameters = {k: v for k, v in job_sel_dict.items() if k != 'strategy'}
        # Job time slice related attributes
        job_ts_dict = settings.get('job_time_slice', {'strategy': 0, 'duration': 480})
        self._job_time_slice = job_ts_dict['strategy']
        self._job_time_slice_parameters = {k: v for k, v in job_ts_dict.items() if k != 'strategy'}
        # Job distribution related attributes
        job_dist_dict = settings.get('job_distribution', {'strategy': 1, 'chunk_size': 3})
        self._job_distribution = job_dist_dict['strategy']
        self._job_distribution_parameters = {k: v for k, v in job_dist_dict.items() if k != 'strategy'}

        # Here we generate seeds for the different pseudo random number
        # generators (PRNGs) needed throughout the simulation.
        # The result needs to be reproducible which is why we use more than one
        # PRNG to prevent potential dependencies between the different parts
        # where PRNGs are needed.
        rand = random.Random(self.seed)
        self._js_seed = rand.getrandbits(128)
        self._jd_seed = rand.getrandbits(128)
        self._dev_seed = rand.getrandbits(128)
        self._scheduler_seed = rand.getrandbits(128)
        self._scheduler_rand = random.Random(self._scheduler_seed)

        # Initiate the objects that are needed immediately after instantiation.
        self._deviation = Deviation(
            random.Random(self._dev_seed),
            self._no_deviations
        )

    @property
    def simulations(self):
        """yield: Yields a list of `SimulationTuple` objects. Those objects
        contain the scheduler and executor objects that were used in the
        simulation run.

        This is useful to, for example, query the executor object for its
        simulation result.

        The property is **(read-only)**.
        """
        for simtup in self._simulations:
            yield simtup

    @property
    def scheduled_operations(self):
        """set: A set of operations that were scheduled in the simulation. Is
        mainly used for debugging and assertion purposes.

        Only access the set after the `run()` method terminated, since it can
        change in between.
        """
        return self._scheduled_operations

    @property
    def metadata(self):
        """SimulationMetadata: The metadata collected during the run of the
        simulation **(read-only)**.
        """
        return self._simulation_metadata

    @property
    def shop_id(self):
        """str: **(read-only)**.
        """
        return self._shop_id

    @property
    def database(self):
        """CommonDatabase: **(read-only)**.
        """
        return self._database

    @property
    def seed(self):
        """int: **(read-only)**.
        """
        return self._seed

    @property
    def seed_deviation(self):
        """int: **(read-only)**.
        """
        return self._dev_seed

    @property
    def seed_scheduler(self):
        """int: **(read-only)**.
        """
        return self._scheduler_seed

    @property
    def seed_job_distributor(self):
        """int: **(read-only)**.
        """
        return self._jd_seed

    @property
    def seed_job_selector(self):
        """int: **(read-only)**.
        """
        return self._js_seed

    @property
    def job_selector(self):
        """int: **(read-only)**.
        """
        return self._job_selector

    @property
    def job_selector_parameters(self):
        """dict: **(read-only)**.
        """
        return self._job_selector_parameters

    @property
    def job_time_slice(self):
        """int: **(read-only)**.
        """
        return self._job_time_slice

    @property
    def job_time_slice_parameters(self):
        """dict: **(read-only)**.
        """
        return self._job_time_slice_parameters

    @property
    def job_distribution(self):
        """int: **(read-only)**.
        """
        return self._job_distribution

    @property
    def job_distribution_parameters(self):
        """dict: **(read-only)**.
        """
        return self._job_distribution_parameters

    def add_simulation(self, scheduler_name, executor_name, scheduler_parameters=None, executor_parameters=None):
        """Registers a new simulation run for a given scheduler and executor.
        When run is executed, all registered schedulers are executed to create
        schedules which are then added to the executors that registered with
        them.

        If multiple executor objects use a common scheduler - including the
        configuration parameters - the schedule is only created once.

        Args:
            scheduler_name (str): The name of the scheduler to be added for this
                simulation run.
            executor_name (str): The name of the executor to be added for this
                simulation run.
            scheduler_parameters (dict, None): An optional config for the
                scheduler.
            executor_parameters (dict, None): An optional config for the executor.
                See the different executor classes for a list of parameters.

        Raises:
            ValueError: if the `scheduler_name` does not match an available
                Scheduler or the `executor_name` does not match an available
                Executor.
            RuntimeError: if the scheduler/executor combination was already
                registered before.

        """
        scheduler_parameters = scheduler_parameters or {}
        executor_parameters = executor_parameters or {}

        # check for duplicates simulations.
        for reg_sim in self._simulations:
            if (
                reg_sim.scheduler.name() == scheduler_name and
                reg_sim.scheduler_parameters == scheduler_parameters and
                reg_sim.executor.name() == executor_name and
                reg_sim.executor_parameters == executor_parameters
            ):
                raise RuntimeError("The combination of scheduler and executor was already registered before.")

        # Create the executor and scheduler objects and add them - including
        # their parameters - to the list of simulations.
        executor = ExecutorFactory.create_executor(
            executor_name,
            env=self._env,
            deviation=self._deviation,
            simulation_metadata=self.metadata,
            **executor_parameters
        )
        scheduler = SchedulerFactory.get_scheduler(scheduler_name)

        sim = SimulationTuple(
            scheduler=scheduler,
            scheduler_parameters=scheduler_parameters,
            executor=executor,
            executor_parameters=executor_parameters
        )
        self._simulations.append(sim)

    def run(self):
        """Starts the simulation run. Before run is executed, at least one
        scheduler and executor has to be added via `add_simulation()`.

        Raises:
            RuntimeError: if connecting to the database was not successful or
                no scheduler/executor object was added to the simulation or
                the returned list of names was empty.

                Also if anything during the execution of an executor fails.
            ValueError: if the provided settings for the
                `~jobshop.sim.job_management.JobSelector` and
                `~jobshop.sim.job_management.JobDistributor` were invalid.
            FileNotFoundError: if the specified setting for the database file
                could not be found.

        """
        if not self._simulations:
            raise RuntimeError("Please add at least one scheduler and executor to the simulation via add_simulation().")

        # Configure the different components of the simulation.
        js = JobSelector(
            database=self.database,
            shop_id=self.shop_id,
            rand=random.Random(self._js_seed)
        )
        names = js.job_names(strategy=self.job_selector, **self.job_selector_parameters)
        self._distributor = JobDistributor(
            database=self.database,
            shop_id=self.shop_id,
            job_names=names,
            time_slice=self.job_time_slice,
            time_slice_config=self.job_time_slice_parameters,
            distribution=self.job_distribution,
            distribution_config=self.job_distribution_parameters,
            rand=random.Random(self._jd_seed)
        )

        if not names:
            raise RuntimeError("The list of job names returned empty.")

        # Initialize the scheduler and executor configurations if necessary.
        for simtup in self._simulations:
            simtup.executor.prepare()

        # Prepare the simulation by add the run loop and events, and finally
        # start the simulation.
        self._env.process(self._run_loop())
        logging.info("%5d [Runtime] - Simulation started." % self._env.now)
        self._env.run()
        logging.info("%5d [Runtime] - Simulation ended." % self._env.now)

    def _run_loop(self):
        """The run loop extracts the data from the configured distributor,
        creates schedules for the configured schedulers and adds them to the
        executor objects, which will take care of simulating the schedule.
        """
        for t in self._distributor.timeslots():
            time_diff = t - self._env.now
            if time_diff > 0:
                yield self._env.timeout(time_diff)

            assert t == self._env.now
            jobs, resources = self._distributor.new_data_for_timeslot(t)
            logging.info("%5d [Runtime] - new data. Jobs: %s, Resources: %s." % (self._env.now, [j.name for j in jobs], [r.name for r in resources]))

            # Add the resources to the deviation pool that is used to retrieve
            # performance factors for the different resources.
            for res in resources:
                self._deviation.add_resource(resource=res, time=self._env.now)

            # Note the time, the job was added.
            ops = []
            for job in jobs:
                ops += job.operations
                self._scheduled_operations |= set(job.operations)
                self.metadata.add_job_entry_time(job, self._env.now)

            cached_schedules = {}
            for simtup in self._simulations:
                scheduler_key = (simtup.scheduler.name(), frozenset(simtup.scheduler_parameters.items()))
                try:
                    schedule = cached_schedules[scheduler_key]
                except KeyError:
                    create_parameters = {"operations": ops}
                    create_parameters.update(simtup.scheduler_parameters)
                    if simtup.scheduler.needs_random:
                        create_parameters['rand_gen'] = self._scheduler_rand
                    schedule = simtup.scheduler.create_schedule(**create_parameters)
                    cached_schedules[scheduler_key] = schedule

                logging.info("%5d [Runtime] - add schedule from scheduler %s to executor %s." % (self._env.now, simtup.scheduler.name(), simtup.executor.name()))
                simtup.executor.add_schedule(schedule)

        # Inform all executors, that no new data will be available.
        for simtup in self._simulations:
            simtup.executor.trigger_data_exhausted()


class SimulationTuple(namedtuple('SimulationTuple', 'scheduler scheduler_parameters executor executor_parameters')):
    """A class wrapper for a `namedtuple`.

    Used to store a scheduler executor combination (a simulation) plus
    additional configuration.

    """
    pass
