# -*- coding: utf-8 -*-
"""

Classes:
    - `FJSSPTextParser`
"""
from collections import OrderedDict
import logging

from .model import Job, Operation, Resource


class FJSSPTextParser:
    """Parser to load the FJSSP test instances.
    Eg the ones by Monaldo: http://people.idsia.ch/~monaldo/fjsp.html
    or http://edoc.sub.uni-hamburg.de/hsu/volltexte/2012/2982/
    """
    @staticmethod
    def parseInput(inputFile):
        """Opens the provided input file, parses its content and creates
        `~jobshop.sim.model.Job`, `~jobshop.sim.model.Operation` and
        `~jobshop.sim.model.Resource` objects which are then returned.

        Args:
            inputfile (str): A absolute or relative path to a FJSSP test
                instance.

        Returns:
            A tuple containing a list of `~jobshop.sim.model.Job` objects and
            a list of `~jobshop.sim.model.Resource` objects.

        """
        jobs = []
        resources = OrderedDict()

        # parse data
        with open(inputFile, 'r') as file:

            numJobs = 0
            numMachines = 0
            for i, line in enumerate(file):
                if i == 0:
                    firstLine = line.split()
                    numJobs = int(firstLine[0])
                    numMachines = int(firstLine[1])
                    logging.debug("[FJSSPTextParser] - NumJobs: %d, NumMachines: %d" % (numJobs, numMachines))
                    continue

                if i > numJobs:
                    continue

                njob = Job(str(i))
                jobs.append(njob)
                # print("Line %d: %s" % (i, line))
                job = line.split()
                numOps = int(job[0])
                colPos = 1
                for opid in range(1, numOps + 1):
                    op = Operation(opid)
                    njob.add_operation(op)

                    # The current colPos points to the number of possible machines an
                    # the current operation can use.
                    # From the position, in steps of two (machine,processing time)
                    # we store all the different machines the operation can use.
                    opFlexibility = int(job[colPos])
                    for m in range(opFlexibility):
                        resid = int(job[colPos + 1])

                        try:
                            res = resources[resid]
                        except KeyError:
                            res = Resource(resid)
                            resources[resid] = res
                        mins = int(job[colPos + 2])
                        op.add_resource(res, mins)
                        colPos += 2
                    colPos += 1

        return jobs, list(resources.values())
