# -*- coding: utf-8 -*-
"""A collection of classes to select and distribute jobs from a
`~jobshop.common.database.CommonDatabase`.

Classes
    - `JobSelector`
    - `JobDistributor`

"""
import random
from itertools import repeat
from collections import OrderedDict


class JobSelector:
    """The `JobSelector` class is used to select a subset of jobs from the list
    of all available jobs in a `~jobshop.common.database.CommonDatabase`.

    The `JobSelector` only returns a list of job names, which can then be used
    to retrieve `~jobshop.sim.model.Job`, `~jobshop.sim.model.Operation`
    and `~jobshop.sim.model.Resource` objects from the
    `~jobshop.common.database.CommonDatabase`.

    The only method available is the `job_names()` function which returns a list
    of job names according to a selected strategy. For more information consult
    the documentation of the function itself.

    Args:
        database (CommonDatabase): A database object used to retrieve job names
            from.
        shop_id (int): The id of the shop (collection of jobs) used for the
            simulation.
        rand (random.Random): A Random object that is used to select a random
            list of jobs, if the random parameter is used in a selected
            strategy.

            The parameter is optional. If it is left out, or is set to `None`,
            a new `random.Random` object with None as seed is created and used.

    """
    def __init__(self, database, shop_id, rand=None):
        self._database = database
        self._shop_id = shop_id
        if rand:
            self._random = rand
        else:
            self._random = random.Random()

    @staticmethod
    def arguments():
        """A combined list of arguments that can be used to retrieve jobs via
        the `job_names()` method.

        The main usage of this property is to provide an easy way for a GUI
        to retrieve and display a list of arguments. This allows the user to
        fine-tune the component.

        The schema of the dictionary is the following::

            {
                '<strategy_value>': {
                    'name': 'name of the strategy/option to be displayed'
                    'parameters': {
                        'parameter1': {
                            'name': 'visible name of the parameter,
                            'type': int,
                            'default': 12
                        },
                        'parameter2': {
                            'type': bool,
                            'default': True
                        }
                    }
                },
                '<other_strategy_value>': {
                    ...
                }
            }

        Returns:
            dict: A dictionary describing the arguments of the `job_names()`
            method.

        """
        return {
            0: {
                'name': 'List of Jobs',
                'parameters': {
                    'job_list': {
                        'name': 'Fixed list (comma separated)',
                        'type': list
                    }
                }
            },
            1: {
                'name': 'Absolute',
                'parameters': {
                    'absolute': {
                        'name': 'Absolute number',
                        'type': int,
                        'default': 20
                    },
                    'random': {
                        'name': 'Random selection',
                        'type': bool,
                        'default': True
                    }
                }
            },
            2: {
                'name': 'Percentage',
                'parameters': {
                    'percentage': {
                        'name': 'Percentage',
                        'type': float,
                        'default': 0.2
                    },
                    'random': {
                        'name': 'Random selection',
                        'type': bool,
                        'default': True
                    }
                }
            }
        }

    @property
    def database(self):
        """CommonDatabase: The database object that is used to retrieve job names
        from **(read-only)**.
        """
        return self._database

    @property
    def shop_id(self):
        """int: The id of the shop (collection of jobs) used for the
        simulation **(read-only)**.
        """
        return self._shop_id

    def job_names(self, strategy, *args, **kwargs):
        """Used to retrieve a list of job names according to a selected
        strategy.

        The strategy is selected based on the arguments of the function.
        All available arguments are listed below. For an example see the section
        **Example**.

        ==========  ===============  ===========================================
        `strategy`  `parameter`      **Remark**
        ----------  ---------------  -------------------------------------------
        0           `job_list`       Simply returns the specified list if the
                                     provided job names are available in the
                                     database.

        1           `absolute=N,     The absolute value of returned job names.
                    random=B`
                                     `N` is an `int` in the interval [1,].

                                     The `random` parameter is an optional
                                     boolean value (Default: False).

                                     It specifies whether the returned
                                     list of job names should be chosen randomly
                                     or not.

                                     Random operations happen according to the,
                                     the `Random` object of the `JobSelector`
                                     object.

        2           `percentage=P,   Returns a proportion of all available job
                    random=B`        names.

                                     This is based on the `percentage`
                                     parameter. `P` needs to be a `float` in
                                     the interval (0, 1].

                                     The calculation is done via ``int(#jobs *
                                     percentage)``.

                                     The `random` parameter is optional boolean
                                     value (Default: False).

                                     It specifies whether the returned
                                     list of job names should be chosen randomly
                                     or not.

                                     Random operations happen according to the,
                                     the `Random` object of the `JobSelector`
                                     object.
        ==========  ===============  ===========================================

        Args:
            strategy (int): The strategy that is used to select the jobs.
            *args: Variable length argument list.
            **kwargs: Arbitrary keyword arguments.

        Returns:
            list: A list of selected job names (str) according to the selected
            strategy.

        Raises:
            ValueError: if the provided parameter combination is not valid.
            RuntimeError: if querying the data from the database was not
                successful.

        Example:
            >>> jd = CommonDatabase("test.sqlite3")
            >>> js = JobSelector(database=jd, random.Random(23))
            >>> js.job_names(1, absolute=5, random=True)
            [
                '1', '5', '7', '3', '2'
            ]

        """
        try:
            allJobs = self.database.all_job_names(shop_id=self.shop_id)
        except RuntimeError:
            raise

        selections = {
            0: self._strategy_selected_jobs,
            1: self._strategy_absolute,
            2: self._strategy_percentage
        }
        try:
            return selections[strategy](list(allJobs), *args, **kwargs)
        except KeyError:
            raise ValueError("A strategy with the value: %s is not supported" % strategy)

    def _strategy_selected_jobs(self, jobs, job_list):
        """Checks if from the provided parameter an iterator can be generated.
        Mirrors/Returns the value if that is the case, otherwise raises an
        exception.
        Additionally it checks whether the job names provided in the given list
        exist in the database. If at least one job does not match, an exception
        is raised.
        """
        try:
            iter(job_list)
        except TypeError:
            raise ValueError("The provided job_list is not iterable.")

        for job in job_list:
            if job not in jobs:
                raise ValueError("There is no job: %s in the database (shop_id: %s)." % (str(job), self.shop_id))

        return job_list

    def _strategy_absolute(self, jobs, absolute, random=False):
        """Returns an `absolute` number of jobs from the database. If the
        optional random parameter validates to `True`, a random proportion is
        returned.
        """
        try:
            absolute = int(absolute)
        except ValueError:
            raise

        if absolute <= 0:
            raise ValueError("Only positive integer values are allowed.")

        if random:
            self._random.shuffle(jobs)

        if absolute > len(jobs):
            return jobs
        return jobs[0:absolute]

    def _strategy_percentage(self, jobs, percentage, random=False):
        """Selects and returns a percentage of all available job names from the
        database. If the optional random parameter validates to True, a random
        proportion is returned.
        """
        try:
            percentage = float(percentage)
        except ValueError:
            raise

        if percentage <= 0.0 or percentage > 1.0:
            raise ValueError("The provided percentage value is not in the valid interval: (0,1].")

        if random:
            self._random.shuffle(jobs)

        numJobs = len(jobs)
        result = jobs[0:int(numJobs * percentage)]

        if len(result) == 0:
            raise ValueError("The resulting list of jobs turned out to be empty. Pick a bigger percentage value.")

        return result


class JobDistributor:
    """Used to generate and distribute `~jobshop.sim.model.Job`,
    `~jobshop.sim.model.Resource` and `~jobshop.sim.model.Operation`
    objects and make them available to the simulation.

    The strategies set during object instantiation are final and cannot be
    changed afterwards.

    If a new strategy is desired, a new `JobDistributor` object needs to be
    created. Therefore, all attributes of the object are read-only.

    **Time Slicing Strategy**:

        ============  ===================  =====================================
        `time_slice`  `time_slice_config`  **Remark**
        ------------  -------------------  -------------------------------------
        0             `{"duration": N}`    The duration by which the time
                                           increases (linearly).

                                           `N` must be a number greater than
                                           zero.

        1             `{"min_days": N,     The interval [N,M] from
                      "max_days": M}`      which the increase in time
                                           is randomly taken.

                                           `N,M` are specified in days
                                           whereas a (work) day counts
                                           for 480 minutes.

                                           `N,M` are integers greater
                                           than 0 with `M >= N`.
        2             `{"average": N}`     The average number of jobs per day.
        ============  ===================  =====================================

    **Distribution Strategy**:

        ==============  =====================  =================================
        `distribution`  `distribution_config`  **Remark**
        --------------  ---------------------  ---------------------------------
        0               None                   The jobs are distributed
                                               all at once without
                                               splitting them into chunks
        1               `{"chunk_size": N}`    The size of all generated
                                               chunks of jobs.

                                               The last chunk can be less
                                               than the specified size,
                                               but is at least of size 1.
        2               `{"min_size": N,       The size of the chunks of
                        "max_size": M}`        jobs is taken from the
                                               interval [N,M].

                                               `N,M` are integers greater
                                               than or equal to 0
                                               with `M >= N`.
        ==============  =====================  =================================

    Args:
        database (CommonDatabase): The database the list of job names
            `job_names` is from. It is queried for `~jobshop.sim.model.Job`
            and `~jobshop.sim.model.Resource` objects with the list of job
            names.
        job_names (list): A list of job names used to query the database.
        time_slice (int): The strategy used to generate the time slots.
        distribution (int): The strategy used to divide and distribute the
            list of all `~jobshop.sim.model.Job` objects.
        time_slice_config (dict): Config dict specific to the selected
            `time_slice` strategy. See the table above for details.
        distribution_config (dict): Config dict specific to the selected
            `distribution` strategy. See the table above for details.
        rand (random.Random, `None`): The random number generator object used
            to create all randomness the object needs. If `None` is provided
            the a new `random.Random` instance is created with `None` as a
            seed.

    Raises:
        RuntimeError: if the distribution could not be calculated due to an
            error when connecting to the database.
        ValueError: if the provided arguments for the distribution or
                time_slice strategies are invalid.

    """
    def __init__(self, database, shop_id, job_names, time_slice, distribution, time_slice_config=None, distribution_config=None, rand=None):
        self._database = database
        self._shop_id = shop_id
        self._job_names = job_names
        self._random = rand or random.Random()

        # strategy parameters
        self._time_slice = time_slice
        self._time_slice_config = time_slice_config or {}
        self._distribution = distribution
        self._distribution_config = distribution_config or {}

        # for the distribution calculation.
        self._data = OrderedDict()
        self._seen_resources = set()

        self._calculate_distribution()

    @staticmethod
    def time_slice_arguments():
        """A combined list of arguments that can be used to configure a new
        `JobDistributor` object.

        This is only one part of the arguments, i.e. the time slicing related
        onces. The remaining arguments can be found in the
        `distribution_arguments()` method.

        The main usage of this property is to provide an easy way for a GUI
        to retrieve and display a list of arguments. This allows the user to
        fine-tune the component.

        The schema of the dictionary is the following::

            {
                '<strategy_value>': {
                    'name': 'name of the strategy/option to be displayed'
                    'parameters': {
                        'parameter1': {
                            'name': 'visible name of the parameter,
                            'type': int,
                            'default': 12
                        },
                        'parameter2': {
                            'type': bool,
                            'default': True
                        }
                    }
                },
                '<other_strategy_value>': {
                    ...
                }
            }

        Returns:
            dict: A dictionary describing the time_slice related arguments that
            can be used to instantiate a `JobDistributor` object.

        """
        return {
            0: {
                'name': 'Fixed time-slices',
                'parameters': {
                    'duration': {
                        'name': 'Fixed duration',
                        'type': int,
                        'default': 480
                    }
                }
            },
            1: {
                'name': 'Day interval',
                'parameters': {
                    'min_days': {
                        'name': 'Minimal number of days',
                        'type': int,
                        'default': 1
                    },
                    'max_days': {
                        'name': 'Maximal number of days',
                        'type': int,
                        'default': 5
                    }
                }
            },
            2: {
                'name': 'Poisson Distributed',
                'parameters': {
                    'average': {
                        'name': 'Average number of jobs per day',
                        'type': int,
                        'default': 10
                    }
                }
            }
        }

    @staticmethod
    def distribution_arguments():
        """A combined list of arguments that can be used to configure a new
        `JobDistributor` object.

        This is only one part of the arguments, i.e. the chunk-size
        distribution related onces. The remaining arguments can be found in the
        `time_slice_arguments()` method.

        The main usage of this property is to provide an easy way for a GUI
        to retrieve and display a list of arguments. This allows the user to
        fine-tune the component.

        The schema of the dictionary is the following::

            {
                '<strategy_value>': {
                    'name': 'name of the strategy/option to be displayed'
                    'parameters': {
                        'parameter1': {
                            'name': 'visible name of the parameter,
                            'type': int,
                            'default': 12
                        },
                        'parameter2': {
                            'type': bool,
                            'default': True
                        }
                    }
                },
                '<other_strategy_value>': {
                    ...
                }
            }

        Returns:
            dict: A dictionary describing the chunk-size distribution related
            arguments that can be used to instantiate a `JobDistributor` object.

        """
        return {
            0: {
                'name': 'All at once'
            },
            1: {
                'name': 'Fixed chunk-size',
                'parameters': {
                    'chunk_size': {
                        'name': 'Fixed chunk size',
                        'type': int,
                        'default': 20
                    },
                }
            },
            2: {
                'name': 'Interval chunk-size',
                'parameters': {
                    'min_size': {
                        'name': 'Minimal chunk size',
                        'type': int,
                        'default': 20
                    },
                    'max_size': {
                        'name': 'Maximal chunk size',
                        'type': int,
                        'default': 50
                    }
                }
            }
        }

    @property
    def database(self):
        """CommonDatabase:  **(read-only)**
        """
        return self._database

    @property
    def shop_id(self):
        """int: The id of the shop (collection of jobs) used for the
            simulation **(read-only)**.
        """
        return self._shop_id

    @property
    def job_names(self):
        """list:  **(read-only)**
        """
        return self._job_names

    @property
    def time_slice_strategy(self):
        """int: **(read-only)**
        """
        return self._time_slice

    @property
    def distribution_strategy(self):
        """int: **(read-only)**
        """
        return self._distribution

    def timeslots(self):
        """A generator listing all the time slots for which new data will be
        available. The yielded values can be used to query the
        `data_for_timeslot()` function for new `~jobshop.sim.model.Job` and
        `~jobshop.sim.model.Resource` objects.

        Yields:
            int: A timeslot for which new data can be retrieved.

        """
        for timeslot in self._data.keys():
            yield timeslot

    def new_data_for_timeslot(self, timeslot):
        """Used to retrieve the data for a time slot generated by the
        `timeslot()` generator function. The data consists of lists of
        `~jobshop.sim.model.Job` and `~jobshop.sim.model.Resource` objects.

        The list of `~jobshop.sim.model.Resource` objects only contains objects
        that are new compared to the previous time slot.

        If all resources of the new operations are needed, iterating over all
        operations of all jobs querying for the resources is the way to go.

        .. NOTE::
            The returned objects are shared between all callers of this
            function and should therefore not be altered.


        Args:
            timeslot (int):

        Returns:
            A tuple consisting of a list of `~jobshop.sim.model.Job` objects
            and a combined list of `~jobshop.sim.model.Resource` objects that
            the different `~jobshop.sim.model.Operation` objects can be
            processed on.

        Raises:
            ValueError: if no data could be retrieved for the specified time
                slot.

        Example:
            >>> db = CommonDatabase("test.sqlite3")
            >>> ts_config = {"duration": 240}
            >>> dist_config = {'chunk_size': 1}
            >>> jd = JobDistributor(db, ['1', '2'], time_slice=0,
            ...     distribution=1, time_slice_config=ts_config,
            ...     distribution_config=dist_config
            ... )
            >>> for ts in jd.timeslots():
            ...     jd.data_for_timeslot(ts)
            (
                [
                    Job("1", "")
                ],
                [
                    Resource("1", ""),
                    Resource("2", ""),
                    Resource("3", ""),
                    Resource("4", ""),
                    Resource("5", "")
                ]
            )
            (
                [
                    Job("2", "")
                ],
                [
                    Resource("6", ""),
                    Resource("7", ""),
                    Resource("8", ""),
                    Resource("9", "")
                ]
            )

        """
        try:
            return self._data[timeslot]
        except KeyError:
            raise ValueError("No data available for the specified time slot: %s" % (timeslot))

    # -------------------------------------------------------------------------
    # PRIVATE
    # -------------------------------------------------------------------------
    def _calculate_distribution(self):
        """Pre-calculates the distribution of the data among the time slots
        and stores the result internally in the _data variable. The
        distribution occurs according to the strategies set during the
        initialization of the object.

        Raises:
            RuntimeError: if querying for the job names was not successful.
            ValueError: if the provided arguments for the distribution or
                time_slice strategies are invalid.

        """
        # Evaluate if the selected strategy and the provided needed
        # function parameters are available.
        try:
            distribution_gen = self._distribution_strategy_gen()
        except KeyError:
            raise ValueError("The selected distribution strategy: %s is not valid" % (self.distribution_strategy))
        except (TypeError, ValueError) as e:
            raise ValueError("The provided config dictionary for the distribution strategy: %s is not valid. Error: %s" % (self.distribution_strategy, e))

        try:
            time_slice_gen = self._time_slice_strategy_gen()
        except KeyError:
            raise ValueError("The selected time_slice strategy: %s is not valid" % (self.time_slice_strategy))
        except (TypeError, ValueError) as e:
            raise ValueError("The provided config dictionary for the time_slice strategy: %s is not valid. Error: %s" % (self.time_slice_strategy, e))

        # Try checks the exceptions thrown of the generators. They are only
        # thrown when the generators are used not when they are created.
        try:
            for names in distribution_gen:
                timeslot = next(time_slice_gen)
                jobs, resources = self.database.jobs_resources_for_names(self.shop_id, names)

                # The jobs are returned ordered by their names. We change the
                # order back to the one that was provided by the names list
                input_order = {val: i for i, val in enumerate(names)}
                sorted_jobs = [None] * len(jobs)
                for job in jobs:
                    sorted_jobs[input_order[job.name]] = job
                for name, job in zip(names, sorted_jobs):
                    assert name == job.name

                newResources = []
                for res in resources:
                    if res not in self._seen_resources:
                        self._seen_resources.add(res)
                        newResources.append(res)
                self._data[timeslot] = (sorted_jobs, newResources)
        except (ValueError, AssertionError) as e:
            raise ValueError("One of the provided config parameters is not valid. Error: %s" % e)

    # -------------------------------------------------------------------------
    # DATA DISTRIBUTION STRATEGIES
    # -------------------------------------------------------------------------
    def _distribution_strategy_gen(self):
        """Selects the strategy to create the distribution of jobs according to
        the constructor parameters `time_slice` and `time_slice_config`.

        Returns
            A generator which yields a list of `~jobshop.sim.model.Job`
            objects. Those, combined with the time_slice generator make up the
            distribution of the jobs.

        Raises:
            KeyError: if the `time_slice` attribute has no proper value.
            TypeError: if the provided distribution config is invalid.

        """
        strategies = {
            0: self._distr_all_at_once,
            1: self._distr_fixed_chunk_size,
            2: self._distr_interval_chunk_size,
        }
        return strategies[self.distribution_strategy](**self._distribution_config)

    def _distr_all_at_once(self):
        """Handle all selected jobs at once."""
        yield self._job_names

    def _distr_fixed_chunk_size(self, chunk_size):
        """Split the list of all jobs into chunks of the specified size."""
        int(chunk_size)
        assert chunk_size > 0, "chunk_size needs to be greater than 0"

        for i in range(0, len(self._job_names), chunk_size):
            names = self._job_names[i:i + chunk_size]
            yield names

    def _distr_interval_chunk_size(self, min_size, max_size):
        """Selects each chunk size according to a provided interval."""
        int(min_size)
        int(max_size)
        assert min_size >= 0, "min_size needs to be greater than or equal to 0."
        assert max_size >= 0, "max_size needs to be greater than or equal to 0."
        assert max_size >= min_size, "max_size needs to be greater than or equal to min_size."
        assert max_size + min_size > 0, "the interval of of min and max size cannot be [0,0]."

        numJobs = len(self._job_names)
        i = 0
        while numJobs > 0:
            chunk_size = self._random.randint(min_size, max_size)
            endIndex = i + chunk_size
            yield self._job_names[i:endIndex]
            i = endIndex
            numJobs -= chunk_size

    # -------------------------------------------------------------------------
    # TIME SLICE STRATEGIES
    # -------------------------------------------------------------------------
    def _time_slice_strategy_gen(self):
        """Selects the time slots according to the strategy selects when the
        object was initialized. The strategy selection is based on the
        parameters `distribution` and `distribution_config`.

        Returns:
            A generator which yields time slots. Those, combined with the job
            list generator, make up the job distribution.

        Raises:
            KeyError: if the `time_slice` attribute has no proper value.
            TypeError: if the provided time_slice config is invalid.

        """
        strategies = {
            0: self._time_slice_fixed_duration,
            1: self._time_slice_random_interval,
            2: self._time_slice_poisson
        }
        return strategies[self.time_slice_strategy](**self._time_slice_config)

    def _time_slice_fixed_duration(self, duration):
        """Yields a sequence of time slots which increase according to the
        provided duration.
        """
        int(duration)
        assert duration > 0, "duration needs to be greater than 0."

        t = 0
        for wait_time in repeat(duration):
            yield t
            t += wait_time

    def _time_slice_random_interval(self, min_days, max_days):
        """Yields a sequence of time slots. The time increases in steps of days
        which are randomly taken from the provided interval.

        A (work) day is assumed to be 480 minutes.
        """
        int(min_days)
        int(max_days)
        assert min_days > 0, "min_days needs to be greater than 0."
        assert max_days > 0, "max_days needs to be greater than 0."
        assert max_days >= min_days, "max_days needs to be greater than or equal to min_days."
        assert max_days + min_days > 0, "the interval of of min and max days cannot be [0,0]."

        t = 0
        while True:
            # days to wait
            days = self._random.randint(min_days, max_days)
            yield t
            t += days * 480  # An 8 hour day has 480 minutes.

    def _time_slice_poisson(self, average):
        """Yields a sequence of time slots. The time increases in steps that
        follow a poisson distribution based on a given average number of jobs
        per day.
        """
        int(average)
        assert average > 0, "average needs to be greater than 0."
        # An 8 hour day has 480 minutes.
        day = 480
        lambd = average / day  # average number of jobs per day
        t = 0
        while True:
            yield t
            # add at least one minute to the next timeslot.
            t += int(self._random.expovariate(lambd)) or 1
