# -*- coding: utf-8 -*-
"""This module provides functions to generate new test instances and load and
parse tables containing distributions from the file system / app bundle.

"""
from jobshop.sim.model import Job, Operation, Resource
from jobshop.common.result_data import SimulationResult, ResultTuple, SimulationMetadata

from enum import Enum
from collections import namedtuple
import random
import math
import csv


class DistributionTuple(namedtuple('DistributionTuple', 'mean stddev count')):
    """A class wrapper for a `namedtuple` that stores the parameters for a
    normal distribution.

    Example:
        >>> ScheduleTuple(mean=0.692, stddev=0.153, count=155)

    """
    pass


class DistributionColumns(Enum):
    """The positions / columns of the different values in a csv file that stores
    data for a distribution.
    """
    Key = 0
    Count = 1
    Mean = 2
    Stddev = 3


def load_distribution(inputfile):
    """Loads a normal distribution from a csv file.

    If a cell could not be parsed properly, e.g. because the values could not
    be casted to int or float values, the whole row is ignored.

    Args:
        inputfile (str): A path to a csv file.

    Returns:
        dict: A list of `DistributionTuple` objects, each of which represents
        the normal distribution of one entity of the domain.

    Raises:
        FileNotFoundError: if the specified inputfile couldn't be opened.

    """
    with open(inputfile, 'r') as file:
        rows = csv.reader(file, delimiter=';')
        data = []
        line = 0

        for row in rows:
            line += 1
            if line == 1:
                continue

            try:
                mean = float(row[DistributionColumns.Mean.value])
                stddev = float(row[DistributionColumns.Stddev.value])
                count = int(row[DistributionColumns.Count.value])
            except ValueError:
                continue

            if count < 10:
                continue

            data.append(DistributionTuple(mean, stddev, count))

        return data


def create_simulation_result_metadata(seed=None, num_resources=5, num_jobs=6, min_ops=4, max_ops=10, op_min_res=1, op_max_res=4):
    """Creates simulation result and metadata test data. This function is solely
    used by unittests.

    Args:
        seed (int): A seed used for the random number generator.
        num_resources (int): The number of resources generated.
        num_jobs (int): Number of jobs to be created.
        min_ops (int): Minimum number of operations per job.
        max_ops (int): Maximum number of operations per job.

    Returns:
        A tuple consisting of the SimulationResult and SimulationMetadata
        object.

    Raises:
        AssertionError: if the specified arguments are invalid. e.g. if min_ops
            is greater than max_ops.

    """
    assert max_ops >= min_ops, "Max ops must be greater than or equal to min_ops."
    assert num_resources > 0, "Number of resources must be greater 0."
    assert num_jobs > 0, "Number of jobs must be greater 0."

    rand = random.Random(seed)
    simres = SimulationResult()
    metadata = SimulationMetadata()

    # Create jobs and resources for the test simulation result
    jobs, resources = create_jobs_and_resources(
        seed=rand.getrandbits(128),
        num_jobs=num_jobs,
        num_resources=num_resources,
        min_ops=min_ops,
        max_ops=max_ops,
        op_min_res=op_min_res,
        op_max_res=op_max_res
    )

    # Track the earliest time the resources can be used.
    resource_free_time = {}
    for res in resources:
        resource_free_time[res] = 0

    def add_results(start, end, entry_time, buffer=20):
        last_finished = 0
        for j in jobs[start:end]:
            metadata.add_job_entry_time(j, entry_time)
            f = 0
            for i, op in enumerate(j.operations):
                res = rand.choice(op.resources)
                res_start = resource_free_time[res]
                earliest_start = max(res_start, f)
                s = earliest_start + buffer
                f = s + op.processing_time(res)
                resource_free_time[res] = f
                if f > last_finished:
                    last_finished = f
                simres.append(res, ResultTuple(op=op, start=s, finish=f, planned_start=None, planned_finish=None))
                simres.add_operation_unblock_time(op, s)
                simres.add_mean_resource_queue_length(0, 1)
        return last_finished

    # Simulate two splits of the created jobs by starting to add 2/3 of the
    # generated jobs first and afterwards the rest.
    split_point = math.ceil(num_jobs * (2 / 3))
    last_finished = add_results(
        start=0,
        end=split_point,
        entry_time=0,
        buffer=10
    )
    add_results(
        start=split_point,
        end=num_jobs,
        entry_time=last_finished + 240,
        buffer=20
    )
    simres.simulation_metadata = metadata

    return simres


def create_jobs_and_resources(seed=None, num_resources=5, num_jobs=6, min_ops=4, max_ops=10, op_min_res=1, op_max_res=4, mean_proc_time=90, stddev_proc_time=30, resource_performance_distr=None, processing_time_distr=None, num_operations_distr=None):
    """Generates random jobs and the corresponding operations and resources.
    The number of jobs, resources and operations as well as the flexibility of
    the operations can be controlled via function parameters.

    It allows to create those objects via specific values in form of function
    parameters as well as distributions. If the three distribution parameters
    are not used, reasonable defaults distributions will be used. But it is
    highly recommended to provide distribution lists created with
    `load_distribution()`.

    If `num_operations_distr` is provided, the parameters `min_ops` and
    `max_ops` are ignored.

    If `processing_time_distr` is provided, the parameters `mean_proc_time` and
    `stddev_proc_time` are ignored.

    Args:
        seed (int): A seed used for the random number generator.
        num_resources (int): The number of resources generated.
        num_jobs (int): Number of jobs to be created.
        min_ops (int): Minimum number of operations per job.
        max_ops (int): Maximum number of operations per job.
        op_min_res (int): The minimum flexibility of an operation.
        op_max_res (int): The maximum flexibility of an operation.
        mean_proc_time (int): Mean processing time.
        stddev_proc_time (int): Processing time standard deviation.
        resource_performance_distr (list): A list containing
            `DistributionTuple` objects that make up a resource performance
            distribution (Optional).
        processing_time_distr (list): A list containing `DistributionTuple`
            objects that make up a processing time distribution (Optional).
        num_operations_distr (list): A list containing `DistributionTuple`
            objects that make up a distribution of the flexibility of an
            operation (Optional).

    Returns:
        A tuple consisting of a list of jobs and a list of resources generated
        by the function.

    Raises:
        AssertionError: if the specified arguments are invalid. e.g. if min_ops
            is greater than max_ops.

    """
    assert num_resources > 0, "Number of resources must be greater 0."
    assert num_jobs > 0, "Number of jobs must be greater 0."
    assert op_max_res <= num_resources, "The number of maximum resources (flexibility of a resource) cannot exceed the number of total resources."
    assert op_max_res >= op_min_res, "The maximal number of resources per operation cannot be greater than or equal to the minimal number of resources per operation."
    assert min_ops > 0, "The minimum number of operations per job needs to be greater than 0."
    assert max_ops >= min_ops, "The maximum number of operations per job needs to be greater than or equal to the minimum number of operations per job."

    rand = random.Random(seed)
    # Create jobs and resources for the test simulation result
    jobs = []

    resources = []
    for i in range(num_resources):

        if resource_performance_distr:
            performance_mean = rand.choice(resource_performance_distr).mean
            performance_stddev = rand.choice(resource_performance_distr).stddev
        else:
            performance_mean = rand.uniform(0.1, 0.9)
            performance_stddev = rand.uniform(0.01, 0.3)

        r = Resource(
            name=i + 1,
            description="",
            performance_mean=performance_mean,
            performance_stddev=performance_stddev
        )
        resources.append(r)

    for i in range(num_jobs):
        job = Job(i + 1)

        if num_operations_distr:
            op_mean = rand.choice(num_operations_distr).mean
            op_stddev = rand.choice(num_operations_distr).stddev
            num_ops = int(rand.gauss(op_mean, op_stddev))
            if num_ops <= 0:
                num_ops = 1
        else:
            num_ops = rand.randint(min_ops, max_ops)

        ops = [Operation(i + 1) for i in range(num_ops)]
        for op in ops:
            # flexibility of the operation
            pot_resources = resources.copy()
            for i in range(rand.randint(op_min_res, op_max_res)):
                res = rand.choice(pot_resources)
                pot_resources.remove(res)

                # Calculate processing time
                if processing_time_distr:
                    proctime_mean = rand.choice(processing_time_distr).mean
                    proctime_stddev = rand.choice(processing_time_distr).stddev
                    proc_time = int(rand.gauss(proctime_mean, proctime_stddev))
                else:
                    proc_time = int(rand.gauss(mean_proc_time, stddev_proc_time))
                if proc_time <= 0:
                    proc_time = 5  # Minimum 5 minutes processing time.

                op.add_resource(res, proc_time)
        job.operations = ops
        jobs.append(job)

    return jobs, resources
