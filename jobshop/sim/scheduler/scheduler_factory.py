# -*- coding: utf-8 -*-
"""This module provides factory class that is used to return a
specific scheduler given its name.

Classes
    - `SchedulerFactory`

"""
from jobshop.sim.scheduler import Scheduler


class SchedulerFactory:
    """Used to create specific `Scheduler` subclass via its class object or the
    name of the scheduler.

    """
    @staticmethod
    def available_schedulers():
        """Allows retrieving a list of all scheduler names.
        Every entry in the list can be used to generate a scheduler with the
        `SchedulerFactory.get_scheduler()` function.

        Returns:
            list: A list of all available scheduler names (str).

        """
        schedulers = []
        for SchedClz in Scheduler.__subclasses__():
            schedulers.append(SchedClz.__name__)
        return schedulers

    @staticmethod
    def get_scheduler(className):
        """Creates an object of a `Scheduler` subclass that matches the provided
        `className` parameter. Raises a `ValueError` exception in case the no
        subclass with the name could be found.

        Args:
            className (str): class name of a scheduler

        Returns:
            An object of the `Scheduler` subclass that matches the provided
            name.

        Raises:
            ValueError: if the `className` parameter does not match the name of
                a scheduler.
        """
        for SchedClz in Scheduler.__subclasses__():
            if className == SchedClz.__name__:
                return SchedClz()

        raise ValueError("No Scheduler found for the provided class string.")
