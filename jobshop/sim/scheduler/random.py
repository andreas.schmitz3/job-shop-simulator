from orderedset import OrderedSet

from jobshop.sim.scheduler import Scheduler
from jobshop.sim.model import Schedule


class Random(Scheduler):
    """A priority rule that randomly selects the order in which jobs are
    prioritized. Furthermore, it also selects the a resource for an operation
    at random.

    """
    needs_random = True

    @staticmethod
    def create_schedule(operations, rand_gen):
        """Creates a `Schedule` for the provided list of
        `~jobshop.sim.model.Operation` objects.

        All the `Operation` objects are assigned to a queue of a resource the
        operations can be processed on.

        Args:
            operations (list): A list of `~jobshop.sim.model.Operation`
                objects.
            rand_gen (random.Random): A `random.Random` object that is used as
                random source.

        Returns:
            Schedule: A `Schedule` object that represents an assignment of  all
            `Operation` objects.

        """
        jobs = OrderedSet()
        for op in operations:
            jobs.add(op.job)

        sorted_jobs = list(jobs)
        rand_gen.shuffle(sorted_jobs)

        schedule = Schedule()
        for job in sorted_jobs:
            for op in job.operations:
                # Ignore operations that were not part of the provided list of
                # operations.
                if op not in operations:
                    continue

                # Select the resource the operation can be processed on the
                # fastest
                selected_resource = rand_gen.choice(op.resources)
                schedule.append(op, selected_resource, None, None)

        return schedule
