from distutils.core import setup, Extension
from Cython.Build import cythonize

extensions = [
     Extension(
        name='ziaee',
        sources=['ziaee.py']
    )
]

setup(
    ext_modules=cythonize(extensions)
)
