from orderedset import OrderedSet

from jobshop.sim.scheduler import Scheduler
from jobshop.sim.model import Schedule


class FirstInFirstOut(Scheduler):
    """A simple priority rule that queues every operation to the resource
    it can be executed on the cheapest. This is done by the first come first
    serve principle. i.e. the first operation that wants to be processed by a
    resource the first in the queue. Therefore, the order of the provided list
    of operations matters.

    The scheduler iterates over all operations and extracts an ordered set of
    jobs from those operations.
    Afterwards, the scheduler iterates over the set of jobs and handles all
    operations of the job that appear in the provided list of operations first,
    before continuing with the next job.

    This schedule simulates a simple implicit priority rule that occurs when
    every job that enters the system selects the resource that it can be
    processed on the fastest. This is a reasonable industry 4.0 strategy where
    no central authority controls the priority of a job.
    """
    @staticmethod
    def create_schedule(operations):
        """Creates a `Schedule` for the provided list of
        `~jobshop.sim.model.Operation` objects.

        All the `Operation` objects are assigned to a queue of a resource the
        operations can be processed on.

        Args:
            operations (list): A list of `~jobshop.sim.model.Operation`
                objects.

        Returns:
            Schedule: A `Schedule` object that represents an assignment of  all
            `Operation` objects.

        """
        sorted_jobs = OrderedSet()
        for op in operations:
            sorted_jobs.add(op.job)

        schedule = Schedule()

        for job in sorted_jobs:
            for op in job.operations:
                # Ignore operations that were not part of the provided list of
                # operations.
                if op not in operations:
                    continue

                # Select the resource the operation can be processed on the
                # fastest
                selected_resource = op.fastest_resource()
                schedule.append(op, selected_resource, None, None)

        return schedule
