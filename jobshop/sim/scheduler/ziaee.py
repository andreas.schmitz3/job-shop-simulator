from orderedset import OrderedSet
import math
import itertools
import multiprocessing as mp
import os
import logging


from jobshop.common.helpers import chunk_list
from jobshop.sim.model import Schedule
from jobshop.sim.scheduler import Scheduler

mp.set_start_method('fork')


class Ziaee(Scheduler):
    """An implementation of the heuristic proposed in "A heuristic algorithm
    for solving flexible job shop scheduling problem" by M. Ziaee (2013).
    URL: https://doi.org/10.1007/s00170-013-5510-z.
    """
    needs_random = False

    L_x1 = 1
    L_x2 = 0
    L_x3 = -1
    L_x4 = -3
    L_x5 = -1
    L_x6 = -1
    U_x1 = 4
    U_x2 = 1
    U_x3 = 0
    U_x4 = -1
    U_x5 = 0
    U_x6 = 0

    @staticmethod
    def create_schedule(operations, configuration=None):
        """Creates a `Schedule` for the provided list of
        `~jobshop.sim.model.Operation` objects.

        All the `Operation` objects are assigned to a queue of a resource the
        operations can be processed on.

        If a configuration is provided, only one schedule for it is generated
        and returned, instead of iterating over all possible configurations.

        Args:
            operations (list): A list of `~jobshop.sim.model.Operation`
                objects.
            configuration (tuple): An 8-tuple that is used to create a schedule.

        Returns:
            Schedule: A `Schedule` object that represents an assignment of  all
            `Operation` objects.

        """
        helper_class = Ziaee.HelperClass(operations)
        if configuration is not None:
            schedule = helper_class._schedule_for_parameters(configuration)
        else:
            schedule, _, _ = helper_class.construct_schedule()
        return schedule

    class HelperClass:
        """A helper class that implements the Ziaee. See
        `Ziaee` for more information.

        """
        def __init__(self, operations):
            self.operations = operations
            # Creates lists of resources and jobs from the given list of
            # operations. This is done, because the heuristic works with jobs
            # instead of operations.
            logging.debug("[Ziaee] - Number of operations: %d" % len(operations))
            resources = OrderedSet()
            jobs = OrderedSet()
            self._jobs_op_indices = {}
            for op in operations:
                job = op.job
                jobs.add(job)
                job_ops = job.operations
                op_index = job_ops.index(op)
                try:
                    min_op_index, _ = self._jobs_op_indices[job]
                except KeyError:
                    min_op_index = op_index
                    self._jobs_op_indices[job] = (min_op_index, len(job_ops) - 1)
                else:
                    if op_index < min_op_index:
                        self._jobs_op_indices[job] = (op_index, len(job_ops) - 1)
                for res in op.resources:
                    resources.add(res)

            self.jobs = list(jobs)
            self.resources = list(resources)
            # Used for caching purposes to speed up the heuristic
            self._total_mean_processing_time_of_job = {}
            self._total_weighted_processing_time_on_machine = {}
            self._mean_operation_processing_time = {}
            # Sort the jobs according to their total mean processing time and
            # the resources according to their total weighted processing time.
            self._sorted_jobs = self.sorted_jobs()
            self._sorted_resources = self.sorted_resources()

        def sorted_jobs(self):
            """Returns a list of jobs sorted ascending by their total mean
            processing time.

            """
            job_order = {}
            for job in self.jobs:
                job_order[job] = self.total_mean_processing_time_of_job(job)
            return sorted(job_order, key=job_order.get)

        def sorted_resources(self):
            """Returns a list of resources sorted ascending by their weighted
            processing time.

            """
            resource_order = {}
            for res in self.resources:
                resource_order[res] = self.total_weighted_processing_time_on_machine(res)
            return sorted(resource_order, key=resource_order.get)

        def construct_schedule(self):
            """Constructs a schedule using the Ziaee Heuristic.

            Returns:
                tuple: A schedule created by the Ziaee Heuristic as well as the
                configuration of the x1 - x8 parameters that was used to
                generate the schedule.

            """
            configurations = [
                (i, x) for i, x in enumerate(
                    itertools.product(
                        range(Ziaee.L_x1, Ziaee.U_x1 + 1),
                        range(Ziaee.L_x2, Ziaee.U_x2 + 1),
                        range(Ziaee.L_x3, Ziaee.U_x3 + 1),
                        range(Ziaee.L_x4, Ziaee.U_x4 + 1),
                        range(Ziaee.L_x5, Ziaee.U_x5 + 1),
                        range(Ziaee.L_x6, Ziaee.U_x6 + 1),
                        range(0, 1 + 1),
                        range(0, 1 + 1)
                    )
                )
            ]

            num_procs = mp.cpu_count()
            chunks = chunk_list(configurations, num_procs)
            mp_result_queue = mp.Queue()
            procs = []
            for i in range(num_procs):
                p = mp.Process(
                    target=self._worker_process,
                    args=(chunks[i], mp_result_queue)
                )
                procs.append(p)
                p.start()

            # Wait for all worker processes to finish.
            for proc in procs:
                proc.join()

            # Aggregates all schedules with the same objective value, and
            # selects the one with the highest index. This guarantees that the
            # result of the non-parallel and parallel versions are the same.
            best_obj_value = math.inf
            best_configs = []
            while not mp_result_queue.empty():
                i, config, obj_val = mp_result_queue.get()
                if obj_val == best_obj_value:
                    best_configs.append((i, config))
                if obj_val < best_obj_value:
                    best_obj_value = obj_val
                    best_configs = []
                    best_configs.append((i, config))
            best_configs.sort()
            i, best_config = best_configs[0]
            logging.debug("[Ziaee] - Found %d schedules with the best objective time: %d. Selected configuration: %s with index: %d." % (len(best_configs), best_obj_value, best_config, i))

            # Rebuild the best schedule again from the best configuration.
            best_schedule = self._schedule_for_parameters(best_config)
            return best_schedule, best_obj_value, best_config

        def _worker_process(self, partial, result_queue):
            """Executed by one process. Carries out a partial list of the
            different configurations.

            Args:
                partial (list): A list of tuples. The first object in the tuple
                    is the index of the configuration, and the second the
                    configuration itself.
                result_queue (multiprocessing.Queue): A queue object where the
                    result configurations are stored in.

            """
            # Create schedules for every configuration in the partial list.
            # Collect all schedules with the best found objective value so far,
            # and send the index, the configuration and the objective value to
            # the parent process.
            best_obj_value = math.inf
            schedules = []
            for i, x_tuple in partial:
                schedule = self._schedule_for_parameters(x_tuple=x_tuple)
                obj_val = schedule.objective_value()
                if obj_val == best_obj_value:
                    schedules.append((i, x_tuple, obj_val))
                elif obj_val < best_obj_value:
                    best_obj_value = obj_val
                    schedules = []
                    schedules.append((i, x_tuple, obj_val))
            # We cannot put the schedule object itself in the queue.
            # There seems to be a problem when sending large amounts of
            # big objects to the main process.
            for s_tuple in schedules:
                i, x_tuple, obj_val = s_tuple
                result_queue.put((i, x_tuple, obj_val))

            result_queue.close()
            result_queue.join_thread()

        def _schedule_for_parameters(self, x_tuple):
            """Creates a schedule for the provided list of jobs and resources
            and the x-parameters x1 - x8.

            Args:
                sorted_jobs (list): A list of jobs sorted by `sorted_jobs()`.
                sorted_resources (list): A list of resources sorted by
                    `sorted_resources()`.
                x_tuple (tuple): A tuple that contains the parameters x_1 till
                    x_8.

            Returns:
                Schedule: A schedule.

            """
            sorted_jobs = self._sorted_jobs
            sorted_resources = self._sorted_resources
            x1, x2, x3, x4, x5, x6, x7, x8 = x_tuple
            schedule = Schedule()
            selected_res = None
            selected_op = None
            resource_free_time = {}
            for res in sorted_resources:
                resource_free_time[res] = 0
            completion_time = {}  # Reset the completion time of prev. runs
            scheduled_ops = set()
            num_jobs = len(sorted_jobs)
            num_resources = len(sorted_resources)
            max_j = max([len(job.operations) for job in sorted_jobs])
            for j in range(max_j):
                round_j_remaining_jobs = set(sorted_jobs)
                while round_j_remaining_jobs:
                    # Finding the best next operation to schedule.
                    tc_star = math.inf
                    for i_dash in range(num_jobs):
                        # if x7 is 1, the sorted list is processed in
                        # increasing order, otherwise in decreasing.
                        job_i = sorted_jobs[i_dash] if (x7 == 1) else sorted_jobs[num_jobs - i_dash - 1]
                        # If there is no operation for position j in job i,
                        # remove the job from the list of remaining jobs to be
                        # processed and continue to the next job.
                        min_index, max_index = self._jobs_op_indices[job_i]
                        if min_index + j > max_index:
                            try:
                                round_j_remaining_jobs.remove(job_i)
                            except KeyError:
                                pass
                            continue
                        else:
                            op_ij = job_i.operations[j + min_index]
                            assert op_ij in self.operations
                            # skip already scheduled operations, or operations
                            # that are not part of the initially given set of
                            # operations.
                            if op_ij in scheduled_ops:
                                continue

                            for k_dash in range(num_resources):
                                # if x8 is 1, the list of machines is processed
                                # in increasing order, otherwise in decreasing.
                                res_k = sorted_resources[k_dash] if (x8 == 1) else sorted_resources[num_resources - k_dash - 1]
                                try:
                                    proc_time = op_ij.processing_time(res_k)
                                except KeyError:
                                    continue
                                else:
                                    tc = self._calculate_tc(completion_time, schedule, res_k, op_ij, proc_time, x1, x2, x3, x4, x5, x6)
                                    if tc < tc_star:
                                        tc_star = tc
                                        selected_op = op_ij
                                        selected_res = res_k

                    if tc_star < math.inf:
                        # Get the completion time of the predecessor. If the
                        # operation is the first operation of the job, this is
                        # simply 0.
                        pre_comp_time = 0
                        predecessor = selected_op.predecessor()
                        if predecessor is not None:
                            try:
                                pre_comp_time = completion_time[predecessor]
                            except KeyError:
                                pass
                        # Calculate the start time of the operation by choosing
                        # comparing the earliest time the operation can be
                        # processed on the resources with the completion time
                        # of the predecessor. The maximum of both values marks
                        # the start time of the operation.
                        start_time = max(
                            resource_free_time[selected_res],
                            pre_comp_time
                        )
                        finish_time = start_time + selected_op.processing_time(selected_res)
                        # Save the completion / finish time of the operation
                        # and set the earliest free time of the resource to
                        # this time as well.
                        completion_time[selected_op] = finish_time
                        resource_free_time[selected_res] = finish_time
                        # Finally schedule the operation and remove it from the
                        # unscheduled list. Furthermore the job is done for
                        # this round and only needs to be checked again after j
                        # increased.
                        schedule.append(
                            op=selected_op,
                            resource=selected_res,
                            start=start_time,
                            finish=finish_time
                        )
                        round_j_remaining_jobs.remove(selected_op.job)
                        scheduled_ops.add(selected_op)

            return schedule

        def _calculate_tc(self, completion_time, schedule, res_k, op_ij, proc_time, x1, x2, x3, x4, x5, x6):
            """Calculates the tc value for the current resource and operation.

            """
            try:
                res_queue = schedule.schedule_tuples(res_k)
            except KeyError:
                c_max_y = 0
            else:
                c_max_y = res_queue[-1].finish

            c_i_pre = 0
            predecessor = op_ij.predecessor()
            if predecessor is not None:
                try:
                    c_i_pre = completion_time[predecessor]
                except KeyError:
                    pass

            # (4, 1, 0, -1, 0, -1, 0, 0)
            # The TC calculation is inlined for speed reasons.
            # w1 = 2, w2 = 1, w3 = 1, w4 = 2, w5 = 1, w6 = 1
            tc = (2 * x1 * max(c_max_y, c_i_pre) + proc_time) + (x2 * max(0, (c_i_pre - c_max_y))) + (x3 * max(0, (c_max_y - c_i_pre))) + (2 * x4 * proc_time) + (x5 * self.total_mean_processing_time_of_job(op_ij.job)) + (x6 * self.total_weighted_processing_time_on_machine(res_k))
            return tc

        def mean_operation_processing_time(self, operation):
            """Calculates the mean processing time of all resources an
            operation can be processed on.

            """
            try:
                return self._mean_operation_processing_time[operation]
            except KeyError:
                proc_sum = sum([operation.processing_time(res) for res in operation.resources])
                ret = proc_sum / len(operation.resources)
                self._mean_operation_processing_time[operation] = ret
                return ret

        def total_mean_processing_time_of_job(self, job):
            """Calculates the mean processing time of a job by calculating
            the mean operation processing time of all its operatins and summing
            them up.

            """
            try:
                return self._total_mean_processing_time_of_job[job]
            except KeyError:
                sj_i = 0
                for op in job.operations:
                    if op not in self.operations:
                        continue
                    sj_i += self.mean_operation_processing_time(op)
                self._total_mean_processing_time_of_job[job] = sj_i
                return sj_i

        def total_weighted_processing_time_on_machine(self, resource):
            """Calculates the total weighted processing time of a machine.

            """
            try:
                return self._total_weighted_processing_time_on_machine[resource]
            except KeyError:
                sk_y = 0
                for job in self.jobs:
                    for op in job.operations:
                        if op not in self.operations:
                            continue
                        for r in op.resources:
                            if r == resource:
                                sk_y += op.processing_time(r) / self.mean_operation_processing_time(op)
                self._total_weighted_processing_time_on_machine[resource] = sk_y
                return sk_y
