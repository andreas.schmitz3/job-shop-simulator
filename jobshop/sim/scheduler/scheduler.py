# -*- coding: utf-8 -*-
"""This module comprises scheduler objects that are used to create a heuristic
solution to the flexible job-shop scheduling problem for a given list of
operations.

For further information, see the documentation of the following classes.

Classes
    - `~jobshop.sim.scheduler.fifo.FirstInFirstOut`
    - `~jobshop.sim.scheduler.spt.ShortestProcessingTime`
    - `~jobshop.sim.scheduler.random.Random`
    - `~jobshop.sim.scheduler.ziaee.Ziaee`

"""
from abc import ABCMeta, abstractmethod


class Scheduler(metaclass=ABCMeta):
    """Abstract base class to create concrete scheduler classes.

    `create_schedule()` is the only method the subclasses have to implement.

    """
    needs_random = False

    @classmethod
    def name(cls):
        """Returns the class name of the scheduler. This is a convenience
        method. Since the name is used quiet heavily, it prevents an excessive
        use of accessing the name via: ``__name__``.

        Returns:
            str: The name of the class as string.
        """
        return cls.__name__

    @staticmethod
    def arguments():
        """A list of optional arguments that can be used to pass to the
        `create_schedule()` method of a specific scheduler. This differs from
        sub-class to sub-class.

        The main usage of this property is to provide an easy way for a GUI
        to retrieve and display a list of arguments. This allows the user to
        fine-tune the component.

        The schema of the dictionary is the following::

            {
                "argument_1": {
                    "name": "a visible name shown in the UI",
                    "type": "native type, e.g. int, float, str",
                    "default": "default value (Optional)"
                },
                "argument_2": {
                    "name": "a visible name shown in the UI",
                    "enum": ["list", "of", "possible", "values"]
                }
            }

        (Can/Should be overwritten by subclasses.)

        Returns:
            dict: A dictionary describing the arguments added to the
            `create_schedule()` method of a specific scheduler.

        """
        return {}

    @staticmethod
    @abstractmethod
    def create_schedule(operations):
        """Creates a `Schedule` for the provided list of
        `~jobshop.sim.model.Operation` objects.

        All the `Operation` objects are assigned to a queue of a resource the
        operations can be processed on.

        Args:
            operations (list): A list of `~jobshop.sim.model.Operation`
                objects.

        Returns:
            Schedule: A `Schedule` object that represents an assignment of  all
            `Operation` objects.

        """
        pass
