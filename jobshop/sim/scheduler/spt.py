from orderedset import OrderedSet

from jobshop.sim.scheduler import Scheduler
from jobshop.sim.model import Schedule


class ShortestProcessingTime(Scheduler):
    """A priority rule that schedules those jobs with the least remaining time, on the fastest available resource, needed first.

    """
    @staticmethod
    def create_schedule(operations):
        """Creates a `Schedule` for the provided list of
        `~jobshop.sim.model.Operation` objects.

        All the `Operation` objects are assigned to a queue of a resource the
        operations can be processed on.

        Args:
            operations (list): A list of `~jobshop.sim.model.Operation`
                objects.

        Returns:
            Schedule: A `Schedule` object that represents an assignment of  all
            `Operation` objects.

        """
        # Calculate the total time needed to finish one job and sort the list
        # of jobs according to this metric.
        # Furthermore, the resource which results in the fastest processing
        # time is chosen for an operation.
        jobs = {}
        op_resources = {}
        for op in operations:
            selected_resource = op.fastest_resource()
            op_resources[op] = selected_resource
            proctime = op.processing_time(selected_resource)
            try:
                jobs[op.job] += proctime
            except KeyError:
                jobs[op.job] = proctime
        sorted_jobs = sorted(jobs, key=jobs.get)
        # Note: for debugging purposes un-comment and print the following.
        # sorted_with_values = sorted(jobs.items(), key=operator.itemgetter(1))
        # print(sorted_with_values)

        # Schedule all operations, starting with the operations that make up the
        # job with the least processing time remaining.
        schedule = Schedule()
        for job in sorted_jobs:
            for op in job.operations:
                # Ignore operations that were not part of the provided list of
                # operations.
                if op not in operations:
                    continue

                selected_resource = op.fastest_resource()
                schedule.append(op, selected_resource, None, None)
        return schedule
