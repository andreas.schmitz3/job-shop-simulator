# -*- coding: utf-8 -*-
"""Contains model classes which represent the data of the application.
The following lists the different elements of this module.

Exceptions
    - `StateError`

Enums
    - `JobState`
    - `OperationState`

Classes
    - `StatePool`
    - `Job`
    - `Operation`
    - `Resource`
    - `ScheduleTuple`
    - `Schedule`
"""
from collections import Counter, namedtuple, OrderedDict
from enum import Enum
from operator import attrgetter
from orderedset import OrderedSet


class StateError(Exception):
    """An error indicating that an error occurred when changing the state
    of an `Operation` or `Job` instance.
    """
    pass


class JobState(Enum):
    """Represents the different states a job can be in."""
    Pending = 0
    Queued = 1
    Processing = 2
    Done = 3


class OperationState(Enum):
    """Represents the different states an operation can be in."""
    Pending = 0
    Queued = 1
    Running = 2
    Done = 3
    Interrupted = 4


class StatePool:
    """The `StatePool` allows tracking the state changes of `Job` and
    `Operation` objects withing an `~jobshop.sim.executor.Executor` object.

    Since `Job` and `Operation` objects are shared between all the different
    objects in one `SimulationEnvironment`, the different states the objects
    go through during the simulation need to be tracked some how. This is done
    by an object of the `StatePool` class.

    The class has the following states for `Operation` objects: Pending,
    Queued, Running, Done and Interrupted.
    `Job` objects go through the states Pending, Queued, Processing and Done.

    The states each operation goes through can be described by the following
    simple state machine (DFA)::

        +--->Pending--->Queued--->Running--->Done
                         |  ^
                         |  |
                         v  |
                      Interrupted

    All missing transitions in the DFA will result in the raise of an
    `StateError`.

    The state machine can be interpreted as follows. After an operation is
    added to the state pool, it gets initialized with the state `Pending`.

    Once the operation was handled by the run time and added to a queue of a
    resource to be processed in the future, its state changes to `Queued`.

    If the operation is removed from the queue of a resource, it is marked as
    `Interrupted` and needs to be `Queued` again.

    When the resource gets free, all the dependencies (the preceding
    operations) are finished and the optional planned start time is reached,
    the operation state changes to `Running`.

    After the resource finished processing the operation, the operation reaches
    its final state: `Done`.

    """
    def __init__(self):
        self._opsAll = OrderedSet()  # Set of all ops.
        self._opsPending = OrderedSet()
        self._opsQueued = OrderedSet()
        self._opsRunning = OrderedSet()
        self._opsDone = OrderedSet()
        self._opsInterrupted = OrderedSet()
        self._opsRunningResource = {}
        self._opsState = {}

    def operations(self):
        """The list of all unique operations that were registered via
        `add_job()`, `add_operations()` or `add_operation()`.

        Returns:
            list: A new list of all added operations. The list is not shared
                between callers.

        """
        return list(self._opsAll)

    def pending_operations(self):
        """All operations that are in state `OperationState.Pending`.

        Returns:
            list: A new list of all operations that are in state
                `OperationState.Pending`. The returned list is not shared
                between callers.

        """
        return list(self._opsPending)

    def queued_operations(self):
        """All operations that are in state `OperationState.Queued`.

        Returns:
            list: A new list of all operations that are in state
                `OperationState.Queued`. The returned list is not shared
                between callers.

        """
        return list(self._opsQueued)

    def running_operations(self):
        """All operations that are in state `OperationState.Running`.

        Returns:
            list: A new list of all operations that are in state
                `OperationState.Running`. The returned list is not shared
                between callers.

        """
        return list(self._opsRunning)

    def done_operations(self):
        """All operations that are in state `OperationState.Done`.

        Returns:
            list: A new list of all operations that are in state
                `OperationState.Done`. The returned list is not shared between
                callers.

        """
        return list(self._opsDone)

    def interrupted_operations(self):
        """All operations that are in state `OperationState.Interrupted`.

        Returns:
            list: A new list of all operations that are in state
                `OperationState.Interrupted`. The returned list is not shared
                between callers.

        """
        return list(self._opsInterrupted)

    def add_job(self, job):
        """Adds new operations of a given job to the StatePool.

        All operations are initialized with the `OperationState.Pending` state.

        Args:
            job (Job): A job whose operations are to be added to the StatePool

        Raises:
            StateError: if the operations of the job are already processed
                e.g. in any state except for `OperationState.Pending`.

        """
        for op in job.operations:
            self.add_operation(op)

    def add_operations(self, ops):
        """Adds new operations to the StatePool. All operations are initialized
        with the `OperationState.Pending` state.

        Args:
            ops (list): List of operations to be added to the StatePool

        Raises:
            StateError: if the operation is already processed e.g. in any
                state except for `OperationState.Pending`.

        """
        for op in ops:
            self.add_operation(op)

    def add_operation(self, op):
        """Adds a new operation into the `StatePool`.

        Args:
            op (Operation): New Operation to be added to the `StatePool`

        Raises:
            StateError: if the operation is already processed e.g. in any
                state except for `OperationState.Pending`.

        """
        if op in self._opsAll - self._opsPending:
            raise StateError("Operation %s is already processed and cannot be set to state Pending again." % op)

        self._opsState[op] = OperationState.Pending
        self._opsPending.add(op)
        self._opsAll.add(op)

    def operation_state(self, op):
        """Returns the state of the provided operation in O(1).

        Args:
            op (Operation): Operation the state is queried for.

        Returns:
            OperationState: the state the operation is in

        Raises:
            KeyError: if the operation is not available

        """
        return self._opsState[op]

    def set_operation_state(self, op, state, *args, **kwargs):
        """Sets the state of the given operation.
        Convenience functions which simply binds the different public functions
        to the `OperationState` Enum.

        Args:
            op (Operation): Operation which state is changed
            state (OperationState): An `OperationState` the operation should be
                set to.

        Raises:
            StateError: if the desired state could not be set.

        """
        handler = {
            OperationState.Pending: self.add_operation,
            OperationState.Queued: self.operation_queue,
            OperationState.Running: self.operation_run,
            OperationState.Done: self.operation_done,
            OperationState.Interrupted: self.operation_interrupt
        }
        handler[state](op, *args, **kwargs)

    def operation_queue(self, op):
        """Tries to set the state of the given operation to
        `OperationState.Queued`.
        This indicates that the operation was handled by the
        `~jobshop.sim.executor.Executor` object and is queued to be
        processed by a resource.

        Args:
            op (Operation): Operation to be put into the Queued state

        Raises:
            StateError: if the operation was not in state
                `OperationState.Pending` or `OperationState.Interrupted`.

        """
        if op in self._opsPending:
            self._opsPending.remove(op)
        elif op in self._opsInterrupted:
            self._opsInterrupted.remove(op)
        else:
            raise StateError("Operation: %s cannot be set to Queued, since it is not in state Pending or Interrupted" % op)

        self._opsState[op] = OperationState.Queued
        self._opsQueued.add(op)

    def operation_run(self, op, res):
        """Tries to set the state of the given operation to
        `OperationState.Running`.

        Also stores the resource the operation is running on.
        The operation needs to be in state `OperationState.Queued`.

        It is not checked whether the operation can indeed run on the resource
        this needs to be done manually.

        Args:
            op (Operation): Operation to be put into the Running state
            res (Resource): The resource the operation is running on.

        Raises:
            StateError: if the operation was not in state
                `OperationState.Queued`.

        """
        try:
            self._opsQueued.remove(op)
        except KeyError:
            raise StateError("Operation: %s cannot be set to Running, since it is not in state Queued" % op)

        self._opsRunningResource[op] = res
        self._opsState[op] = OperationState.Running
        self._opsRunning.add(op)

    def operation_done(self, op):
        """Tries to set the state of the operation to `OperationState.Done`
        The operation needs to be in state `OperationState.Running`.

        Args:
            op (Operation): Operation to be put into the Done state

        Raises:
            StateError: if the operation was not in state
                `OperationState.Running`.

        """
        try:
            self._opsRunning.remove(op)
        except KeyError:
            raise StateError("Operation: %s cannot be set to state Done, since is not in state Running" % op)

        del self._opsRunningResource[op]
        self._opsState[op] = OperationState.Done
        self._opsDone.add(op)

    def operation_interrupt(self, op):
        """Tries to set the state of the given operation to
        `OperationState.Interrupted`.
        The operation needs to be in state `OperationState.Queued`.

        Args:
            op (Operation): Operation to be put in Interrupted state.

        Raises:
            StateError: if the operation was not in state
                `OperationState.Queued`.

        """
        try:
            self._opsQueued.remove(op)
        except KeyError:
            raise StateError("Operation: %s cannot be Interrupted since it is not in state Queued." % op)

        self._opsState[op] = OperationState.Interrupted
        self._opsInterrupted.add(op)

    def operation_resource(self, op):
        """Returns the resource the operation is running on. This is only
        available if the operation is in state running.

        Args:
            op (Operation): Operation the resource is wanted for

        Returns:
            Resource: the resource the operation is running on

        Raises:
            StateError: if the operation was not in state
                `OperationState.Running`.

        """
        if op not in self._opsRunning:
            raise StateError("Operation: %s needs to be in state Running to be queried for its resource" % op)

        return self._opsRunningResource[op]

    def job_state(self, job):
        """Returns the state of the given job.
        The state of a job depends on the state of its operations.
        If all operations of a job are in `OperationState.Pending` or
        `OperationState.Pending` state, the job is in state `JobState.Pending`.

        If at least one operation is in state `OperationState.Running` or
        `OperationState.Done`, the job is in state `JobState.Processing`.

        If all operations are in state `OperationState.Done`, the job is in
        state `JobState.Done`.

        Otherwise the job is in state `JobState.Queued`

        Args:
            job (Job): An `Job` object

        Returns:
            JobState: the state the job is in.

        Raises:
            KeyError: if at least one operation was not previously added to
                the `StatePool`

        """
        opStates = [self.operation_state(op) for op in job.operations]
        numOps = len(job.operations)
        status = Counter(opStates)

        pending = status[OperationState.Pending]
        queued = status[OperationState.Queued]
        done = status[OperationState.Done]
        interrupted = status[OperationState.Interrupted]
        running = status[OperationState.Running]

        if interrupted + pending == numOps:
            return JobState.Pending

        if done == numOps:
            return JobState.Done

        if done or running:
            return JobState.Processing

        return JobState.Queued


class Job:
    """The `Job`  class represents a Job in the Flexible Job-Shop Scheduling
    Problem. It is comprised of a name, and a non-empty list of `Operation` objects.

    Args:
        name (str): The name of the new Job. Usually a simple digit.
            Every input is converted to its string representation.
        description (str): An optional description of the job.
        operations (list): Optional list of `Operation` objects.
            Has the same effect than using the `operations` attribute setter.

    Attributes:
        description (str): Description of the job.

    """
    def __init__(self, name, description="", operations=None):
        self._name = str(name)
        self.description = str(description)
        self._operations = []  # order gives sequence
        if operations:
            self.operations = operations  # using the attribute setter

    def __repr__(self):
        return 'Job("%s", "%s")' % (self.name, self.description)

    def __str__(self):
        return self.name

    @property
    def operations(self):
        """list: A list of `Operation` objects that belong to this job.

        Setting the attribute automatically sets the job attribute of the
        `Operation` objects to this `Job` object.

        Setting does not copy the list of operations. Additionally it can
        change the order of the operations within if they are not sorted by
        their ``sequence_number`` attribute.

        """
        return self._operations

    @operations.setter
    def operations(self, operations):
        for op in operations:
            op.job = self
        operations.sort(key=attrgetter('sequence_number'))
        self._operations = operations

    @property
    def name(self):
        """str: Unique representation of the job within the application
        **(read-only)**

        """
        return self._name

    def add_operation(self, op):
        """Adds a new `Operation` object to the list of operations.
        Also takes care that the list of operations is properly sorted by the
        ``sequence_number`` attribute of the `Operation` object.
        It also sets the ``job`` attribute of the `Operation` object to this
        object.

        Args:
            op (Operation): An `Operation` object to be added to the list of
                operations.

        """
        self._operations.append(op)
        op.job = self
        self._operations.sort(key=attrgetter('sequence_number'))

    def operation_sequence_numbers(self):
        """Yields ``sequence_number`` attribute of the job's operations in
        sequential order.

        Yields:
            The sequence numbers of the registered `Operation` objects.

        """
        for op in self._operations:
            yield op.sequence_number


class Operation:
    """The `Operation` class represents an Operation in the Flexible Job-Shop
    Scheduling Problem. It has an unique name which is comprised of the name
    of the job and a sequence number. Additionally it has an optional
    description and a reference to the corresponding `Job` object. Lastly, it
    stores a list of resources and processing times needed to perform the
    operation on the resource.

    If the ``job`` parameter is `None`, it has to be set by the caller
    manually.

    Args:
        sequence_number (`int`): A number inducing the sequence in the list of
            all operations in the corresponding `Job` object.
        job (`Job`, optional): The `Job` object the operation is part of.
            If not set during the construction of the object, it has to be set
            afterwards via the `job` attribute.
        resource_cost (`dict`, optional): A dictionary with `Resource` objects
            and the corresponding costs (int) to process the operation on
            the resource. The content of the dictionary is copied, the
            dictionary itself is therefore not referenced.
            If the parameter is not provided via the constructor, the
            `Resource` objects and costs have to be added manually via the
            `add_resource()` method.
        description (`str`, optional): A text describing the purpose of the
            operation.

    Raises:
        ValueError: if the ``sequence_number`` parameter is not integer
            convertible.

    Attributes:
        job (Job): The `Job` object the operation belongs to.
        description (str): A text that describes the purpose of the operation.

    """
    def __init__(self, sequence_number, job=None, resource_cost=None, description=''):
        self.job = job
        # if description is None, it is not converted to "None".
        if description:
            self.description = str(description)
        else:
            self.description = ''
        self._sequence_number = int(sequence_number)
        self._resource_cost = OrderedDict()

        # Converts all values to floats and guarantees that the values are
        # indeed numbers. Raises ValueError if that is not the case.
        if resource_cost:
            for res, cost in resource_cost.items():
                self.add_resource(res, cost)

        # Set by the predecessor method after the first call, used to cache the
        # result and speed up the program.
        self._predecessor_set = False
        self._predecessor = None

    def __repr__(self):
        return 'Operation(%d, Job("%s"), %s, "%s")' % (self._sequence_number, self.job, self._resource_cost, self.description)

    def __str__(self):
        return self.name or ""

    # Ready-only properties
    @property
    def name(self):
        """str, None: Unique representation of the operation within the
        application. If no job is set None is returned. **(read-only)**

        """
        try:
            return "%s-%s" % (self.job.name, self._sequence_number)
        except AttributeError:
            return None

    @property
    def resources(self):
        """list: A list of the different `Resource` objects the operation can
        be processed on. **(read-only)**

        The returned `Resource` objects can be used to query the cost to
        process the operation on the resource by using the `processing_time()`
        method.

        """
        return list(self._resource_cost.keys())

    @property
    def sequence_number(self):
        """int: Unique number of the operation within the list of all
        operations of the corresponding job. **(read-only)**

        """
        return self._sequence_number

    def add_resource(self, resource, cost):
        """Adds a new resource to the list of resources and sets the cost it
        takes to process the operation on the resource.

        Args:
            resource (Resource): A resource the operation can be processed on
            cost (int): The cost of process the operation on the resource

        Raises:
            ValueError: if the cost cannot be converted into a int

        """
        self._resource_cost[resource] = int(cost)

    def fastest_resource(self):
        """Returns the resource which can process the operation the fastest.

        Returns:
            int: the resource that can process the operation the fastest.

        """
        return min(self._resource_cost, key=self._resource_cost.get)

    def processing_time(self, resource):
        """Returns the costs of processing the operation on the given resource.

        Args:
            resource (Resource): A resource the `Operation` object can be
                processed on.

        Returns:
            int: The costs to process the `Operation` object on the given
            `Resource` object.

        Raises:
            KeyError: if the operation cannot be processed by the resource

        """
        return self._resource_cost[resource]

    def predecessor(self):
        """Returns the `Operation` object from the common `Job` that has to be
        processed successfully before the operation can be started i.e. it
        depends on.

        Returns:
            The `Operation` object that has to be processed before this
            operation can be started or `None` if the operation has no
            dependency i.e. it is the first operation of the corresponding job.

        Raises:
            RuntimeError: if the `job` attribute was not set.

        """
        try:
            ops = self.job.operations
        except AttributeError:
            raise RuntimeError("The job attribute of the operation is not set, but predecessor was called.")
        else:
            if self._predecessor_set:
                return self._predecessor
            else:
                self.is_predecessor = True
                pos = ops.index(self)
                pred = pos - 1
                if pred < 0:
                    self._predecessor = None
                    return None
                else:
                    self._predecessor = self.job.operations[pred]
                    return self._predecessor

    def is_predecessor(self, operation):
        """Checks if the given operation belongs to the same job as this
        operation and if the sequence number of it is lower. In this case
        the given operation is a predecessor of this one.

        Args:
            operation (Operation): An operation that is tested of being an
                predecessor to this operation.

        Returns:
            bool: True if the given operation is a predecessor of this
                operation, False otherwise.

        """
        return self.job == operation.job and self.sequence_number > operation.sequence_number

    def is_successor(self, operation):
        """Checks if the given operation belongs to the same job as this
        operation and if the sequence number of it is higher. In this case
        the given operation is a successor of this one.

        Args:
            operation (Operation): An operation that is tested of being an
                successor to this operation.

        Returns:
            bool: True if the given operation is a successor of this operation,
            False otherwise.

        """
        return self.sequence_number < operation.sequence_number and self.job == operation.job

    def has_successor(self):
        """Checks if the operation is the last operation of the job. If that is
        the case, `False` is returned, otherwise `True`.

        Returns:
            bool: `True` if the operation has successors, `False` otherwise.

        """
        return not (self.job.operations[-1] == self)


class Resource:
    """An object representing a resource in the Flexible Job-Shop Scheduling
    Problem. It is used by `Operation` objects.

    The Resource object is immutable. It implements __hash__, __eq__ and
    __neq__ which enables to use the objects for dictionary keys and treat
    multiple instances of the object with the same name and description as
    equal.

    Args:
        name (str): A string representing the resource.
        description(str): An optional argument which allows for a description
            of the resource.

    """
    def __init__(self, name, description="", performance_mean=None, performance_stddev=None):
        self._name = str(name)
        self._description = str(description)
        self._mean = performance_mean
        self._stddev = performance_stddev

    def __str__(self):
        return self._name

    def __repr__(self):
        return 'Resource("%s", "%s")' % (self._name, self._description)

    def __eq__(self, other):
        return self.name == other.name and self.description == other.description and isinstance(other, Resource)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self._name, self._description))

    @property
    def name(self):
        """str: A string representing the resource. **(read-only)**"""
        return self._name

    @property
    def description(self):
        """str: A string describing the resource. **(read-only)**
        """
        return self._description

    @property
    def performance_mean(self):
        """float: A float value in the interval [0,1] which represents the mean
        performance of the resource. **(read-only)**

        Raises:
            RuntimeError: if the value wasn't properly set during instantiation
                of the object.

        """
        if type(self._mean) != float and self._mean is not None:
            raise RuntimeError("The mean of resource %s cannot be retrieved, because it was not properly set during instantiation." % self)
        return self._mean

    @property
    def performance_stddev(self):
        """float: A float value in the interval [0,1] which represents the
        resource's standard deviation performance. **(read-only)**

        Raises:
            RuntimeError: if the value wasn't properly set during instantiation
                of the object.

        """
        if type(self._stddev) != float and self._stddev is not None:
            raise RuntimeError("The standard deviation of resource %s cannot be retrieved, because it was not properly set during instantiation." % self)
        return self._stddev


class ScheduleTuple(namedtuple('ScheduleTuple', 'op start finish')):
    """A class wrapper for a `namedtuple`.

    Used to store the (planned) start and finish time of an `Operation` in a
    schedule.

    Example:
        >>> ScheduleTuple(op=operation, start=0, finish=0)

    """
    pass


class Schedule:
    """The `Schedule` class bundles resources with a list of `ScheduleTuple`
    which represent the operations to be processed by the resources.

    Attributes:
        schedule (dict): Dictionary representing a list of `ScheduleTuple` to
            be run on the different resources.

    """
    def __init__(self):
        self.schedule = {}

    @property
    def resources(self):
        """list: A list of the resources stored in the schedule. They can be
        used to query the `operations()` function to return the list of
        scheduled `ScheduleTuple` objects. **(read-only)**
        """
        return self.schedule.keys()

    def append(self, op, resource, start=None, finish=None):
        """Schedules an operation on a resource.

        Optionally an ``start`` and ``finish`` parameter can be set which
        represent the planned start and finish time of the operation on the
        resource.

        The order in which the operations are queued matters, since they are
        handled in the order they are inserted.

        Args:
            op (Operation): The operation to be queued to the resource
            resource (Resource): The resource the operation is queued
            start (`int`, `None`, optional): The planned start of the operation
            finish (`int`, `None`, optional): The planned finish of the
                operation

        Raises:
            AssertionError: if the operation to be added is a predecessor of an
                already scheduled operation, or the start time of the operation
                is less than the finish time of a previous scheduled operation
                on the same resource.

        """
        try:
            reslist = self.schedule[resource]
        except KeyError:
            reslist = []
            self.schedule[resource] = reslist

        # Make sure that the operation to be added is not a predecessor of a
        # already scheduled operation. Commented out due to performance reasons.
        # for res in self.resources:
        #     for st in self.schedule_tuples(res):
        #         assert st.op != op, "The operation to be scheduled was already scheduled before. This can't be the case."
        #         assert not st.op.is_predecessor(op), "The operation to be added to a schedule is the predecessor of an already scheduled operation. That can't be the case."

        st = ScheduleTuple(op=op, start=start, finish=finish)
        reslist.append(st)

    def schedule_tuples(self, resource):
        """Returns a list of scheduled `ScheduleTuple` objects for a given
        resource.

        Args:
            resource (Resource): A resource

        Returns:
            A list of `ScheduleTuple` tuples.

        Raises:
            KeyError: if no operations for the given resource could be found.

        """
        return self.schedule[resource]

    def objective_value(self):
        """The time in which the last operation was finished in the simulation.
        Also called: makespan.

        Returns:
            int: The time the last operation was finished.

        Raises:
            TypeError: if the finish time is missing for at least one scheduled
                operation.
            ValueError: if no operation was scheduled.

        """
        # Get the max value from every machines last operation's finish time
        return max([self.schedule[m][-1].finish for m in self.schedule])
