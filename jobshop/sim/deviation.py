# -*- coding: utf-8 -*-
"""

Classes
    - `Deviation`

"""
import random
import logging
import math
from collections import OrderedDict


class Deviation:
    """This class allows for reproducible performance deviations of resources.

    The `resource_performance()` method returns factors which can be multiplied
    to the time a resource needs to process an operation, thus introducing
    errors / deviations to the simulation.

    Args:
        rand (random.Random, None): A `random.Random` object that is used to
            generate the performance factors for the registered resources.
        no_deviations (boolean): If set to `True` `resource_performance()`
            returns `1.0` (100%). Otherwise, the function returns the
            calculated performance factors for a resource. (Default: False).

            By changing the `change_interval` attribute, this behavior can be
            changes at runtime.
        change_interval (int): The time after which new values are drawn from
            the underlying distribution (Default: 480).

    Attributes:
        no_deviations (boolean): Indicates whether the deviation object simply
            returns 0 for all deviation queries (True) or the deviation that are
            normally returned.

            Can be changed during run time.
        change_interval (int): The time after which new values are drawn from
            the underlying distribution. (Default: 480 i.e. after each day.)

    Examples:
        >>> r = Resource(1, "Welding", 0.654, 0.143)
        >>> dev = Deviation(rand=random.Random(77466))
        >>> dev.resource_performance(r, 120)
        0.6477034820283879

    """
    def __init__(self, rand=None, no_deviations=False, change_interval=480):
        self.no_deviations = no_deviations
        self.change_interval = change_interval

        self._random = rand or random.Random()
        self._cache = OrderedDict()
        self._current_interval = 0

    def add_resource(self, resource, time):
        """Adds a resource to the deviation class s.t. the executors can
        retrieve the performance / delays for a given resource.

        Args:
            resource (Resource): A resource in the simulation run.
            time (int): The current simulation time, the resource was added.

        """
        # Update all already registered resources to the specified time.
        interval = self._interval_for_time(time)
        self._update_to_interval(interval)

        # Create a cache for the resource to store its performance for the
        # different time intervals.
        try:
            self._cache[resource]
        except KeyError:
            res_cache = {}
            self._cache[resource] = res_cache
            # Add the first performance value for the resource.
            if resource.performance_mean is not None and resource.performance_stddev is not None:
                res_deviation = self._random.gauss(resource.performance_mean, resource. performance_stddev)
            else:
                res_deviation = 1
            res_cache[interval] = res_deviation
        else:
            logging.warning("Resource %s was already added to the deviation class." % resource)

    def resource_performance(self, resource, time):
        """The performance of an resource at a given time.

        The performance is given as factor that can be multiplied to processing
        time of an operation.

        The performance of the resource is normal distributed. The performance
        value extracted from the distribution is re-drawn every simulated day
        (every 480 minutes).

        If the `no_deviations` attribute is `True` the method always returns
        `1.0`.

        .. NOTE::
            The performance factor of a resource can only be requested for time
            slots (time interval) after the resource was added.

            For example: If `change_interval` is set to 480, and a resource is
            added a time 960 (interval 2), it is not possible to query a value
            for smaller intervals i.e. time slots < 960. This will raise an
            `ValueError`.

        Args:
            resource (Resource): The resource which performance factor should
                be returned.
            time (int): The current time in the simulation.

        Returns:
            float: The performance of the machine in percentage (i.e. 95% is
            represented as 0.95).

        Raises:
            ValueError: If the requested resource was not registered or the
                time slot (interval) lies before the time the resource was
                added. See the note above for details.

        Example:
            >>> r = Resource(1, "Welding", 0.654, 0.143)
            >>> dev = Deviation(rand=random.Random(77466))
            >>> dev.resource_performance(r, 120)
            0.6477034820283879

            >>> r = Resource(1, "Welding", 0.654, 0.143)
            >>> dev = Deviation(rand=random.Random(77466))
            >>> dev.no_deviations = True
            >>> dev.resource_performance(r, 240)
            1.0

        """
        # Get the resource cache in which the performance factors of the
        # different time intervals are stored.
        try:
            res_cache = self._cache[resource]
        except KeyError:
            raise ValueError("Resource %s was queried for a performance factor, but was not added before." % resource)

        # Check whether all values for the queried interval are available or
        # we have to update to the specified interval.
        interval = self._interval_for_time(time)
        self._update_to_interval(interval)

        # Get the simulation time interval, the time is in and retrieve
        # the resource deviation factor.
        try:
            res_deviation = res_cache[interval]
        except KeyError:
            raise ValueError("There is no value stored in the cache for resource %s and interval %s (time: %s). A possible reason is that the resource was added after the specified time. This is not allowed." % (resource, interval, time))
        else:
            # Return 100% performance if no deviation is desired or the
            # retrieved factor.
            # The value is returned here, and not before performing all those
            # calculations s.t. we can keep track of the time that passes
            # in the simulation to update the performance factors.
            # Otherwise changing the no_deviations attribute at runtime would
            # influence the performance factors of the resources compared
            # to other simulations with the same seeds.
            if self.no_deviations:
                return 1
            else:
                return res_deviation

    def _update_to_interval(self, interval):
        """Used to calculate new performance values for all registered
        resources and the given interval.

        This is done to guarantee that as long as the same resources are used,
        the performance values are always the same for different simulations
        with the same random seeds.

        Args:
            interval (int): An interval calculated by `_interval_for_time()`.

        """
        if interval <= self._current_interval:
            return

        # generate new values for the missing intervals
        for i in range(self._current_interval + 1, interval + 1):
            for res, cache in self._cache.items():
                if res.performance_mean is not None and res.performance_stddev is not None:
                    cache[i] = self._random.gauss(res.performance_mean, res. performance_stddev)
                else:
                    cache[i] = 1

        self._current_interval = interval

    def _interval_for_time(self, time):
        r"""Calculates the interval for the given time.

        The interval is calculated from the `change_interval` attribute and
        ``time`` as follows:

        .. math::
            interval = \lfloor time \div change\_interval \rfloor

        Args:
            time (int): The time for which the corresponding interval is
                returned.

        Returns:
            int: the interval in which the time resided.

        Examples:
            >>> dev = Deviation(change_interval=240)
            >>> dev._interval_for_time(120)
            0
            >>> dev._interval_for_time(240)
            1

        """
        return math.floor(time / self.change_interval)
