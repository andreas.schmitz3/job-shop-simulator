# -*- coding: utf-8 -*-
import logging
import simpy

from jobshop.sim.executor import Executor, NoAlternativeException
from jobshop.sim.model import Schedule


class OperationTimeout(Executor):
    """A simple executor scheme that processes the scheduled operation according
    to the added schedules.

    Additionally, it implements a timeout for every operation. Once the timeout
    is reached, the operation checks if it can be processed on other, unvisited,
    resources. If that is the case, it chooses the the one that has the shortest
    processing time (SPT) for the operation itself.

    It is possible though that the operation cannot be queued to the resource
    since there are already succeeding operations queued on the resource.
    In this case, the operation simply stays on its current resource at the same
    queue position. In case another timeout occurs, the selected resource is
    not visited again. The reason for this is that the operation would always
    try the same resource, since it always picks the one it is processed the
    fastest on. And it could very well occur that it gets rejected again and
    again sine the succeeding operations were still not processed. With
    excluding the resource, it is possible that it finds another resource it
    can be processed on faster.

    Args:
        timeout (int): Duration in simulation time after which a component
            tries to be processed by another resource instead of staying in the
            queue any longer (Default: **240**, which equal half a day).
        max_hops (int): The maximum number of changes (hops to a different
            resource) the component is allowed to perform (Default: **1**).

            E.g. for 4, it can be the case that an operation queued on 5
            different resources (the first one and four more changes).
        watchdog_autorestart (boolean): Indicates whether the watch dog should
            automatically restart after it has been triggered, opposed to only
            being started after an operation has been finished (Default: True).

    Attributes:
        timeout (int): Simulation time after which a component tries another
            resource instead of waiting any longer.
        max_hops (int): The maximum number of changes (hops to a different
            resource) the component is allowed to perform (Default: **1**).

            E.g. for 4, it can be the case that an operation queued on 5
            different resources (the first one and four more changes).
        watchdog_autorestart (boolean): If set to `True`, a operations`s watch
            dog will restart automatically once it timed out. If set to `False`
            the watch dog is only triggered once and can only be started again
            if called for explicitly.


    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.timeout = kwargs.get('timeout', 240)
        self.max_hops = kwargs.get('max_hops', 50)
        self.watchdog_autorestart = kwargs.get('watchdog_autorestart', True)
        # The different resources an operation already visited.
        self._visited_resources = {}
        self._operation_wd_event = {}
        self._resource_idle_time = {}

    @staticmethod
    def arguments():
        return {
            'timeout': {
                'name': 'Timeout',
                'type': int,
                'default': 240
            },
            'max_hops': {
                'name': 'Maximal number of hops',
                'type': int,
                'default': 50
            },
            'watchdog_autorestart': {
                'name': 'Auto-restart when timed out',
                'type': bool,
                'default': True
            }
        }

    def _visited_resources_list(self, op):
        """Returns a list of visited resources for a given operation. If no
        list exists yet, an empty one is created.
        """
        try:
            op_list = self._visited_resources[op]
        except KeyError:
            op_list = []
            self._visited_resources[op] = op_list
        return op_list

    def _operation_watchdog_trigger(self, operation):
        """Tries to find another resource for the operation that can potentially
        process the operation faster, compared to staying in the queue and
        waiting for the resource to get free.

        Sub-classes can overwrite the `_get_alternative_resource()` method to
        implement different selection strategies for alternative resources.
        """
        self._log("Watch Dog: operation %s timed out." % (operation), level=logging.DEBUG)
        # List all other resources the operation can be processed on and
        # exclude the ones that it has already visited.
        potential_resources = {}
        for res in operation.resources:
            if res not in self._visited_resources_list(operation):
                potential_resources[res] = operation.processing_time(res)

        try:
            if len(potential_resources) == 0:
                raise NoAlternativeException("There are no further alternative resources for operation %s." % operation)

            # Get a new resource according to the strategy of the (sub-)class.
            new_resource = self._get_alternative_resource(
                operation=operation,
                potential_resources=potential_resources
            )

            assert new_resource is not None, "None was returned by the alternative resource selection strategy."

            # check the newly selected resource again for deadlocks, since
            # simple selection strategies do not prevent this.
            # Exclude all resources that could result in a deadlock.
            # Since deadlock prevention in this communication scheme is not
            # possible, we only allow rescheduling an operation if it has no
            # more successors or if a resource's queue is empty.
            # Otherwise, this is not solveable without global knowledge (i.e.
            # operation can communicate with all maschines)
            for op in self._states.queued_operations():
                op_proc = self.operation_process(op)
                if (op_proc.resource == new_resource and operation != op) and operation.has_successor():
                    self._log("Operation %s: Re-scheduling to the newly selected resource %s is not possible, because it could potentially result in a deadlock." % (op, new_resource))
                    return

            self._log("Alternative resource found. Rescheduling to %s" % new_resource)
            new_schedule = Schedule()
            oproc = self.operation_process(operation)
            # (Copies the original planned start and finish times.)
            new_schedule.append(operation, new_resource, oproc.start, oproc.finish)
            self.reschedule(new_schedule)
        except NoAlternativeException as e:
            self._log(e)
        except AssertionError as e:
            self._log(e, level=logging.ERROR)

    def _get_alternative_resource(self, operation, potential_resources):
        """Choose an alternative resource that processes the operation the
        fastest.

        Override this method in sub-classes to simulate different strategies.

        Args:
            operation (Operation): An operation.
            potential_resources (dict): A dictionary with potential `Resource`
                objects as keys, and the processing time for the operation as
                value.

        Returns:
            Resource: a new resource the operation can be queued on without a
            deadlock.

        Raises:
            NoAlternativeException: if there are no more alternatives the
                operation can be queued on.

        """
        try:
            new_resource = min(potential_resources, key=potential_resources.get)
        except ValueError:
            raise NoAlternativeException("There are no further alternative resources for operation %s." % operation)
        else:
            return new_resource

    def _operation_watchdog_process(self, operation):
        """The process function that is added to simpy's event queue.
        Can be interrupted. If not interrupted, _operation_watchdog_trigger
        is executed.
        """
        try:
            # Only start the timeout after all dependencies are resolved.
            # Since it is possible to interrupt a process, we may have to
            # yield the dependency again, if it was interrupted temporarily.
            # Unfortunately there is no other way to achieve this behavior
            # with simpy.
            yield self._env.process(self._resolve_dependencies(operation))

            # Only start the timeout after the planned start time has been
            # passed.
            oproc = self.operation_process(operation)
            if oproc.start:
                if self._env.now < oproc.start:
                    self._log("Watch dog: waiting before starting time out for operation %s" % (operation))
                    yield self._env.timeout(oproc.start - self._env.now)

            # Start the time out of the watch dog. If the watch dog is not
            # interrupted, the trigger function is executed which tries to
            # find other resources the operation can be potentially processed
            # faster.
            yield self._env.timeout(self.timeout)
            self._operation_watchdog_trigger(operation)
        except simpy.Interrupt:
            self._log("Watch dog: stopped for operation %s." % (operation))
        else:
            if self.watchdog_autorestart:
                self._start_operation_watchdog(operation)

    def _start_operation_watchdog(self, operation):
        """Starts a watch dog for an operation that is triggered after a certain
        timeout.
        """
        ev = self._operation_wd_event.get(operation)
        if ev is None or ev.triggered:
            self._log("Watch Dog: started for operation %s." % (operation))
            self._operation_wd_event[operation] = self._env.process(self._operation_watchdog_process(operation))

    def _stop_operation_watchdog(self, operation):
        """Interrupts the watch dog of the specified operation.
        """
        try:
            op_event = self._operation_wd_event[operation]
        except KeyError:
            pass
        else:
            if not op_event.triggered:
                op_event.interrupt()

    # --------------------------------------------------------------------------
    # RESOURCE METRIC COLLECTION
    # --------------------------------------------------------------------------
    def _mean_resource_idle_time(self, resource):
        """Returns the mean resource idling time of the provided resource in
        percentage. For example, 0.1 means that the resource spend 10% of the
        whole simulation time idling.
        """
        try:
            idle_time, total_time = self._resource_idle_time[resource]
        except KeyError:
            return 0.0
        else:
            try:
                return idle_time / total_time
            except ZeroDivisionError:
                return 0.0

    def _resource_starts_idling(self, resource, current_time):
        """Note that the resource is in idling mode and save the current time.
        """
        try:
            self._resource_idle_time[resource]
        except KeyError:
            self._resource_idle_time[resource] = [0.0, current_time]
        else:
            self._resource_idle_time[resource][1] = current_time

    def _resource_stops_idling(self, resource, current_time):
        """Stops the idling state for the resource and adds the idling time.
        """
        try:
            _, prev_time = self._resource_idle_time[resource]
        except KeyError:
            # when stop is executed before start was executed, we initialize
            # it this way.
            self._resource_starts_idling(resource, current_time)
        else:
            assert current_time > 0.0, "0 cannot be given as current_time, once it is initialized (after the first call of this method)."
            assert current_time >= prev_time, "The given current time must be greater than or equal to the previous current_time."
            idle_time = current_time - prev_time
            self._resource_idle_time[resource][0] += idle_time
            self._resource_idle_time[resource][1] = current_time

    # --------------------------------------------------------------------------
    # SUBCLASS HOOKS
    # --------------------------------------------------------------------------
    def _queued_hook(self, stuple, res):
        op_list = self._visited_resources_list(stuple.op)
        assert res not in op_list, "Operation %s was queued a second time on resource %s. This should not be the case" % (stuple.op, res)
        op_list.append(res)

        # Start the watch dog as long as the number of visited resources
        # does not exceed the specified amount of maximum hops / changes.
        if len(op_list) <= self.max_hops:
            self._start_operation_watchdog(stuple.op)

    def _wait_block_hook(self, stuple, res):
        self._stop_operation_watchdog(stuple.op)

    def _running_hook(self, stuple, res, start):
        self._stop_operation_watchdog(stuple.op)
        self._resource_stops_idling(res, self._env.now)

    def _done_hook(self, stuple, res, start, finish):
        self._resource_starts_idling(res, self._env.now)
