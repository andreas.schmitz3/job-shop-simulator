# -*- coding: utf-8 -*-
"""This module contains fundamental classes to build executor strategies.
This includes the `Executor` base class, that implements a simple no
communication strategy. Furthermore, it provides rescheduling capabilities and
hooks that provide information for that can simulate communication between
different parts of the environment. This allows sub-classes to implement
decision making and rescheduling based on these information.

Additionally, this module contains some helper classes: `OperationProcess`,
`OperationReturnCode`, `NoDependencyException` and `NoAlternativeException`
that are mainly used internally and in some `Executor` sub-classes.

Classes
    - `Executor`
    - `OperationProcess`

Enums
    - `OperationReturnCode`

Exceptions
    - `NoDependencyException`
    - `NoAlternativeException`


"""
import simpy
import logging
from collections import OrderedDict
from enum import Enum
import statistics

from jobshop.sim.model import StatePool, OperationState, StateError
from jobshop.common.result_data import SimulationResult, ResultTuple


class OperationReturnCode(Enum):
    """The return codes a `simpy.Process` object of an `OperationProcess`
    returns.

    Is only used internally. There is no need to use it anywhere else than in
    the `Executor` base class.

    """
    Success = 0
    Interrupted = 1


class NoDependencyException(Exception):
    """Thrown by an internal function to indicate that the operation has no
    dependency and only has to wait for the resource to get free.

    Should not be used in any other scenario.

    """
    pass


class NoAlternativeException(Exception):
    """Raised by an internal function to indicate that there is no alternative
    resource an operation can be rescheduled on. At the moment this only occurs
    for the `Executor` sub-classes `OperationCommunication` and
    `OperationTimeout`.

    Should not be used in any other scenario.
    """
    pass


class OperationProcess:
    """Bundles the relevant information to an operation: the operation object,
    start and finish time as well as the resource it is processed on together
    with an `~simpy.Event` object.

    Args:
        operation (Operation): The operation that is being processed.
        resource (Resource): The resource the operation is processed on.
        process (simpy.Process): A simpy process for the operation in the
            current simulation. Optional argument with default value `None`.
        start (int): Optional start time. Can be `None` if not available in a
            a schedule.
        finish (int): Optional finish time. Can be `None` if not available in
            a schedule.

    Attributes:
        operation (Operation): The operation that is being processed.
        resource (Resource): The resource the operation is processed on.
        process (simpy.Process): The simpy process for the operation in the
            current simulation. The process to be set should already be added
            to the simulation event loop. The reference to the process is
            primarily used to interrupt and reschedule the operation.
        start (int): The planned start time of the operation on the resource.
            `None` if no start time is available e.g. the scheduler did not
            provide one when creating the schedule.
        finish (int): The planned finish time of the operation on the resource.
            Is `None` if no finish time is available.

    Example:
        >>> op = Operation(0)
        >>> res = Resource("12345")
        >>> oproc = OperationProcess(operation=op, resource=res, start=0, finish=230)
        >>> oproc.process = environment.process(simpy.Process())

    """
    def __init__(self, operation, resource, process=None, start=None, finish=None):
        self.resource = resource
        self.operation = operation
        self.process = process
        self.start = start
        self.finish = finish


class Executor:
    """Base executor class for specific sub executor classes which implement
    different strategies to react to errors during the simulation run.

    The base class simply adds for all operations in a given scheduled a
    corresponding event in the simulation environment.

    When being executed the operations are processed in the order given by the
    added schedules. On top of the costs to process a operation on a resource,
    a error / deviation is added to simulate the deviations of real-world
    production environments.

    Every executor object performs one "simulation" of scheduled jobs and their
    operations.

    Args:
        env (simpy.Environment): The environment in which the simulation takes
            place.
        deviation (Deviation): A `~jobshop.sim.deviation.Deviation` object
            which is used to set the performance of the different resources
            during the execution.
        simulation_metadata (SimulationMetadata): A
            `jobshop.common.result_data.SimulationMetadata` object that is
            maintained by the simulation environment that created the executor.

            The executor should, under no circumstances alter it, since all
            executors share the same object.

            This can be used for decision making and is also used for the
            performance analysis of the simulation result.

    Example:
        >>> te = Executor(environment, deviation, simulation_metadata)
        >>> te.prepare()
        >>> te.add_schedule(schedule)

    """
    def __init__(self, env, deviation, simulation_metadata, *args, **kwargs):
        self._env = env
        self._states = StatePool()
        self._result = SimulationResult()
        self._result.simulation_metadata = simulation_metadata
        self._operation_processes = OrderedDict()
        self._resource_processes = {}
        self._resource_performance = {}
        self._deviation = deviation

    @staticmethod
    def arguments():
        """A list of optional arguments that can be used to initialize the
        executor. This differs from sub-class to sub-class.

        The main usage of this property is to provide an easy way for a GUI
        to retrieve and display a list of arguments. This allows the user to
        fine-tune specific executors.

        The schema of the dictionary is the following::

            {
                "argument_name": {
                    "name": "a visible name shown in the UI",
                    "type": "native type, e.g. int, float, str",
                    "default": "default value (Optional)"
                },
                "argument_name_2": {
                    "name": "a visible name shown in the UI",
                    "enum": ["list", "of", "possible", "values"]
                }
            }

        (Can/Should be overwritten by subclasses.)

       Returns:
            dict: A dictionary describing the arguments used to instantiate a
            specific executor sub-class.

        """
        return {}

    @property
    def result(self):
        """SimulationResult: The result of the execution one or more
        `~jobshop.sim.model.Schedule` objects. This attribute ca be queried
        at any point in time to retrieve the
        `~jobshop.commom.simulation_result.SimulationResult` object that is
        maintained by the executor.

        The executor fills the object with executed operations over time.

        The attribute is **(read-only)**.
        """
        return self._result

    def name(self):
        """Returns the class name of the object. This is a convenience
        method. Since the name is used quiet heavily, it prevents an excessive
        use of accessing the name via: ``__class__.__name__``.

        Returns:
            str: The name of the class as a string.
        """
        return self.__class__.__name__

    def reschedule(self, schedule):
        """Reschedules already queued operations.
        Only operations that are in `OperationState.Queued` can be rescheduled.

        All operations contained in the schedule need to already exist.
        For resources that were not used before, corresponding objects  will be
        created.

        Args:
            schedule (Schedule): Rescheduled operations.

        Raises:
            RuntimeError: if the operations in the provided `Schedule` object
                are not in state `OperationState.Queued` or the provided
                schedule object contains an invalid schedule that could not
                properly be executed.

        """
        # First Interrupt all operations in the schedule
        for res in schedule.resources:
            stuples = schedule.schedule_tuples(res)
            for stuple in stuples:
                # Check if an process exist for the operation and that it is in
                # state Queued.
                if self._states.operation_state(stuple.op) != OperationState.Queued:
                    raise RuntimeError("Operation: %s cannot be rescheduled, because it is not in state 'Queued'." % stuple.op)
                try:
                    oproc = self._operation_processes[stuple.op]
                except KeyError:
                    raise RuntimeError("Operation: %s cannot be rescheduled, because it was not scheduled before." % stuple.op)
                # Stop the currently running process and put the resource in
                # interrupt state
                oproc.process.interrupt()

                # Delete the whole process context from the list of queued
                # operations.
                try:
                    del self._operation_processes[stuple.op]
                except KeyError:
                    raise RuntimeError("OperationProcess for operation %s could not be deleted." % (stuple.op))

        # ... and afterwards reschedule the interrupted operations
        self._process_schedule(schedule)

    def add_schedule(self, schedule):
        """Adds the `~jobshop.sim.model.Operation` objects of a
        `~jobshop.sim.model.Schedule` object, generated by a
        `~jobshop.sim.model.Scheduler` object to the list of operations to
        be executed.

        Args:
            schedule (Schedule): A `~jobshop.sim.model.Schedule` object.

        Raises:
            RuntimeError: if at least one operation was already scheduled
                before or the schedule object contained an invalid schedule
                that could not properly be executed on the executor.

        """
        # First, add the operations to the StatePool which tracks the state of
        # the different operations.
        for res in schedule.resources:
            stuples = schedule.schedule_tuples(res)
            for stuple in stuples:
                try:
                    self._states.add_operation(stuple.op)
                except StateError as e:
                    raise RuntimeError(e)

        # ... and afterwards, handle the whole schedule.
        self._process_schedule(schedule)
        self._schedule_added_hook(schedule)

    def runtime_resource_performance(self, resource):
        """Returns the current mean performance of a resource. This metric is based on the measurements done at runtime, and not directly retrieved from the resource itself.

        The main purpose of this function is to simulate the runtime performance
        measurement of a resource. This can, for example, be used to decide on
        which resource an operation is to be rescheduled at runtime.

        Args:
            resource (Resource): The resource which mean performance factor
                be returned.

        Returns:
            float: the mean performance of the resource. If not available, 1.0
            is returned (i.e. the performance is expected to be 100%).

        """
        try:
            mean_performance, _ = self._resource_performance[resource]
        except KeyError:
            return 1.0
        else:
            return mean_performance

    def add_runtime_resource_performance(self, resource, performance):
        """Adds a performance value to the mean performance of a resource.
        For usage details, see the documentation of
        `runtime_resource_performance()`.

        Args:
            resource (Resource): The resource which mean performance factor
                be returned.
            performance (float): The measured performance of the resource.

        """
        try:
            # Get the current performance value and the count of the performance
            # values that were added before.
            mean_performance, count = self._resource_performance[resource]
        except KeyError:
            # If no mean performance is available for the resource, we
            # initialize it with the given performance.
            self._resource_performance[resource] = [performance, 1]
        else:
            # calculate mean on streaming values:
            # m_k = m_k-1 + (x_k - m_k-1) / k
            self._resource_performance[resource][0] = mean_performance + ((performance - mean_performance) / (count + 1))
            self._resource_performance[resource][1] += 1

    def resource_process(self, res):
        """Retrieve the resources process for a given resource.
        The method takes care of instantiating the requested resource process
        if it is not yet available.

        This method should be used wherevery the `simpy.Resource` for a
        `~jobshop.sim.model.Resource` is needed.

        Args:
            res (Resource): A `~jobshop.sim.model.Resource` object.

        Returns:
            simpy.Resource: A simpy resource object that can be used to queue
            an operation on.

        """
        try:
            reproc = self._resource_processes[res]
        except KeyError:
            reproc = simpy.Resource(self._env)
            self._resource_processes[res] = reproc
            self._resource_added_hook(res)
        return reproc

    def resource_queue_length(self, res):
        """Get the queue length of a resource.

        Args:
            res (Resource):

        Returns:
            int: The length of the resources`s queue.

        Raises:
            KeyError: if the provided resource is not registered.

        """
        return len(self.resource_process(res).queue)

    def prepare(self):
        """A function that can be implemented by subclasses and acts as a hook
        to add events into the event queue.

        The function is executed before the simulation event loop is started.

        """
        pass

    def operation_process(self, operation):
        """Return the `OperationProcess` object for the operation. This stores
        all the runtime information about the operation.

        This getter should only be used by subclasses.

        Args:
            operation (Operation): An operation.

        Returns:
            OperationProcess, None: The corresponding `OperationProcess` object
            for the provided operation or `None` if no operation process is
            stored for the operation e.g. because it finished.

        """
        try:
            return self._operation_processes[operation]
        except KeyError:
            return None

    def _resource_queue_length_snapshot(self):
        """Calculates the current average queue length of all resources that are
        currently in the simulation and stores the value for the current
        simulation time in the simulations meta data storage.
        """
        queue_lengths = [self.resource_queue_length(res) for res in self._resource_processes]
        mean_len = statistics.mean(queue_lengths)
        self.result.add_mean_resource_queue_length(
            time=self._env.now,
            mean_len=mean_len
        )

    def _log(self, msg, level=logging.DEBUG):
        """Used internally to write logs prefixed with the name of the executor.

        Args:
            msg (str): A string or string convertible object that is logged.
            level (int): A log level for the message. (Default: logging.DEBUG).

        """
        logging.log(level, "%5d [%s] - %s" % (self._env.now, self.__class__.__name__, msg))

    def _process_schedule(self, schedule):
        """Common routine to create objects to store the runtime objects
        necessary to process an operation in the simulation environment.

        This is used by the `add_schedule()` and `reschedule()` methods to
        add the operations to the executor.
        """
        for res in schedule.resources:
            # Handle all operations that were queued for the specific resource
            stuples = schedule.schedule_tuples(res)
            for stuple in stuples:
                # Create an simpy.Process object for every operation and add it
                # to the event queue. To be able to interrupt the processes, in
                # case an executor wants to reschedule operations, the event
                # objects are stored in the operation_processes dictionary.
                # Alongside, all the information to recreate the process in
                # case of an interrupt are stored.
                proc = self._env.process(self._handle_schedule_tuple(stuple, res))
                oproc = OperationProcess(stuple.op, res, proc, stuple.start, stuple.finish)
                self._operation_processes[stuple.op] = oproc

    def _resolve_dependencies(self, operation):
        """A process that yields the dependency as long as it was interrupted.
        Only after the dependencies of the specified operation are resolved,
        does the function terminate.
        """
        while True:
            try:
                dependency = self._operation_dependency(operation)
            except NoDependencyException:
                break
            else:
                yield dependency.process
                if dependency.process.value == OperationReturnCode.Success:
                    break

    def _handle_schedule_tuple(self, stuple, res):
        """Creates a generator for one `~jobshop.sim.model.ScheduleTuple`
        on the scheduled resource. The generator is wrapped into a
        simpy.Process by the caller and thus added to the simulation event loop.
        """
        try:
            # A hook that can be overwritten by subclasses to execute code
            # before requesting the resource.
            self._pending_hook(stuple, res)

            # Request the resource for the operation and queue them.
            # An order in the queue is imposed by the order in which they are
            # requested. Therefore, we have to request them here immediately
            # and not only after the dependencies are resolved.
            # If the resource is not yet available, it is created via the
            # `resource_process()` method.
            with self.resource_process(res).request() as req:
                # Mark that the operation has requested the resource and is now
                # in state Queued. This indicates that the executor has handled
                # the operation properly.
                # Also execute a hook to inform about the state change and
                # store the average queue length of all resources, which is
                # later on used to calculate performance metrics.
                self._log("Queued operation %s on %s. Checking dependencies..." % (stuple.op, res))
                self._states.operation_queue(stuple.op)
                self._queued_hook(stuple, res)
                self._resource_queue_length_snapshot()

                # Wait for the dependencies to finish.
                # Since it is possible to interrupt a process, we may have to
                # yield the dependency again, if it was interrupted temporarily.
                # Unfortunately there is no other way to achieve this behavior
                # with simpy.
                yield self._env.process(self._resolve_dependencies(stuple.op))

                # Wait until the start time of the operation is reached before
                # proceeding.
                if stuple.start:
                    if self._env.now < stuple.start:
                        self._log("Waiting: operation %s needs to wait for planned start time: %s." % (stuple.op, stuple.start))
                        self._wait_block_hook(stuple, res)
                        yield self._env.timeout(stuple.start - self._env.now)

                # Note the time the operations dependencies are resolved into
                # the meta data. This is used to calculate performance metrics
                # later on.
                self.result.add_operation_unblock_time(stuple.op, self._env.now)

                # Now that the dependencies are resolved and the start time
                # has been reached, we can wait for the next open spot on the
                # resource.
                yield req

                # Now the planned start time was reached or exceeded and the
                # resource can start processing the operation.
                self._log("Started operation %s on resource %s." % (stuple.op, res))
                self._states.operation_run(stuple.op, res)
                # A hook for subclasses that indicates that the resource
                # started processing the operation.
                self._running_hook(stuple, res, self._env.now)

                # Note the time the operation started being processed on the
                # resource.
                start = self._env.now
                performance = self._deviation.resource_performance(res, self._env.now)
                self._log("Performance of resource %s is %s" % (res.name, performance))
                processing_time = stuple.op.processing_time(res)
                delay = int(((1 - performance) * processing_time))
                delayed_time = processing_time + delay
                yield self._env.timeout(max(delayed_time, 1))

                # Note the time the resource finished processing the operation
                # add save the collected metrics in the simulation result.
                finish = self._env.now
                self._log("Finished operation %s on resource %s. Delay: %s." % (stuple.op, res, delay))
                self._states.operation_done(stuple.op)
                restup = ResultTuple(op=stuple.op, start=start, finish=finish, planned_start=stuple.start, planned_finish=stuple.finish)
                self._result.append(res, restup)
                # Save the "measured/persieved" performance of the resource.
                # This is used by sub-classes to base decision making on the
                # performance of resources. This simulates that the machine
                # or someone else is tracking the machines performance.
                self.add_runtime_resource_performance(res, performance)

                # A hook for subclasses that indicates that the resource
                # finished processing the operation. The operations was already
                # added to the simulation result.
                self._done_hook(stuple, res, start, finish)

                # Since the operation is now done and will not be used anymore
                # we can delete the reference to the operation process s.t.
                # the garbage collector can delete the old objects.
                try:
                    del self._operation_processes[stuple.op]
                except KeyError:
                    raise RuntimeError("OperationProcess for operation %s could not be deleted." % (stuple.op))
                # Take another average queue length snapshot.
                self._resource_queue_length_snapshot()
                return OperationReturnCode.Success

        except simpy.Interrupt:
            # Check if the interrupted operation was not running and change
            # the state of the operation accordingly.
            if self._states.operation_state(stuple.op) != OperationState.Queued:
                raise RuntimeError("Only operations in state: Queued are allowed to be interrupted.")
            self._log("Interrupted operation %s." % (stuple.op))
            self._states.operation_interrupt(stuple.op)
            # A hook for subclasses to react to the interruption of the
            # operation.
            self._interrupted_hook(stuple, res)
            return OperationReturnCode.Interrupted

    def _operation_dependency(self, operation):
        """Used to retrieve the dependency `OperationProcess` for a given
        `~jobshop.sim.model.Operation` object.

        Args:
            operation(Operation): The operation which dependency should be
                returned (if any).

        Returns:
            OperationProcess: The `OperationProcess` object the operation
            depends on.

        Raises:
            NoDependencyException: if the given operation has no predecessor.
            AssertionError: if the given operation has a predecessor that is not
                done yet, but no `OperationProcess` is available to return.

        """
        predecessor = operation.predecessor()
        if predecessor is None:
            raise NoDependencyException()

        # Get the predecessors operation process.
        # Runtime test: In the case None is returned, assure that really
        # all preceding operations are in StatePoole done. Otherwise, the added
        # schedule is invalid e.g. because the added operations are added
        # in a wrong order.
        dependency = self._operation_processes.get(predecessor)
        if dependency is None:
            assert predecessor in self._states.done_operations(), "There is no OperationProcess object available for operation %s. The object is needed as dependency by operation %s." % (predecessor, operation)
            raise NoDependencyException()

        return dependency

    def _resource_added_hook(self, res):
        """A hook for a subclass to react to a newly added resource.

        At this point a `simpy.Resource` object was already created for the
        resource.

        Args:
            res (Resource): A `~jobshop.sim.model.Resource` object that was
                just added to the simulation.

        """
        pass

    def _pending_hook(self, stuple, res):
        """A hook for subclasses that is triggered before the a request is send
        to a resource for the operation of the
        `~jobshop.sim.model.ScheduleTuple`.

        At this point the operation of the `~jobshop.sim.model.ScheduleTuple`
        is still in state `OperationState.Pending` or
        `OperationState.Interrupted`.

        Args:
            stuple (ScheduleTuple): A `~jobshop.sim.model.ScheduleTuple` object
                that was added to the executor via a
                `~jobshop.sim.model.Schedule` object.
            res (Resource): A `~jobshop.sim.model.Resource` object that was
                just added to the simulation.

        """
        pass

    def _queued_hook(self, stuple, res):
        """A hook for subclasses that is triggered right before the operation
        of the `~jobshop.sim.model.ScheduleTuple` waits for its
        dependencies to finish and the resource to get an open slot.

        At this point the operation of the `~jobshop.sim.model.ScheduleTuple`
        is in state `OperationState.Queued`

        Args:
            stuple (ScheduleTuple): A `~jobshop.sim.model.ScheduleTuple` object
                that was added to the executor via a
                `~jobshop.sim.model.Schedule` object.
            res (Resource): A `~jobshop.sim.model.Resource` object that was
                just added to the simulation.

        """
        pass

    def _wait_block_hook(self, stuple, res):
        """A hook for subclasses that is triggered if the operation was granted
        the resource, and blocks it, but has to wait until the simulation time
        reached the planned start time of the operation.

        At this point the operation of the `~jobshop.sim.model.ScheduleTuple`
        is still in state `OperationState.Queued`.

        Args:
            stuple (ScheduleTuple): A `~jobshop.sim.model.ScheduleTuple` object
                that was added to the executor via a
                `~jobshop.sim.model.Schedule` object.
            res (Resource): A `~jobshop.sim.model.Resource` object that was
                just added to the simulation.

        """
        pass

    def _interrupted_hook(self, stuple, res):
        """A hook for subclasses that is triggered after an operation was
        interrupted from waiting for a resource, its dependency or the
        simulation time to reach the scheduled start time.

        At this point the operation of the `~jobshop.sim.model.ScheduleTuple`
        is in state `OperationState.Interrupted`

        Args:
            stuple (ScheduleTuple): A `~jobshop.sim.model.ScheduleTuple` object
                that was added to the executor via a
                `~jobshop.sim.model.Schedule` object.
            res (Resource): A `~jobshop.sim.model.Resource` object that was
                just added to the simulation.

        """
        pass

    def _running_hook(self, stuple, res, start):
        """A hook for subclasses that is triggered right before the resources
        starts processing the operation.

        At this point the operation of the `~jobshop.sim.model.ScheduleTuple`
        is in state `OperationState.Running`.

        Args:
            stuple (ScheduleTuple): A `~jobshop.sim.model.ScheduleTuple` object
                that was added to the executor via a
                `~jobshop.sim.model.Schedule` object.
            res (Resource): A `~jobshop.sim.model.Resource` object that was
                just added to the simulation.
            start (int): The simulated start time of the operation on the
                resource. For planned start time see ``stuple.start``.

        """
        pass

    def _done_hook(self, stuple, res, start, finish):
        """A hook for subclasses that is triggered after a resource finished
        processing an operation.

        The information about the executor are already added to the
        `~jobshop.commom.simulation_result.SimulationResult` object of the
        executor and can be accessed via the `result` attribute.

        At this point the operation of the `~jobshop.sim.model.ScheduleTuple`
        is in state `OperationState.Done`.

        Args:
            stuple (ScheduleTuple): A `~jobshop.sim.model.ScheduleTuple` object
                that was added to the executor via a
                `~jobshop.sim.model.Schedule` object.
            res (Resource): A `~jobshop.sim.model.Resource` object that was
                just added to the simulation.
            start (int): The simulated start time of the operation on the
                resource. For planned start time see ``stuple.start``.
            finish (int): The simulated finish time of the operation on the
                resource. For planned finish time see ``stuple.finish``.

        """
        pass

    def _schedule_added_hook(self, schedule):
        """A hook for subclasses that is triggered right after a new schedule
        was added to the executor.

        Args:
            schedule (Schedule): A `~jobshop.sim.model.Schedule` object.

        """
        pass

    def trigger_data_exhausted(self):
        """A hook for subclasses that is triggered after all jobs were added.
        """
        pass
