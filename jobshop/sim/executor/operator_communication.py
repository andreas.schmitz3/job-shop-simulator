# -*- coding: utf-8 -*-
from jobshop.sim.executor import Executor
from jobshop.sim.scheduler import SchedulerFactory


class OperatorCommunication(Executor):
    """The `OperatorCommunication` class is a executor that has complete
    knowledge over the system and can use this information to replan the queues
    on all machines.

    At the moment the executor triggers every `trigger_interval` minutes and
    runs the specified scheduler (`scheduler_name`) to create a new schedule
    based on all remaining operations.

    Args:
        scheduler_name (str): Name of a `~jobshop.sim.scheduler.Scheduler`
            subclass that is used to for rescheduling purposes.
        trigger_interval (int): The interval in which the priority rule should
            be applied to all remaining operations.
        stop_after_data_exhausted (bool): Stops replanning after the
            environment signaled that no new data will be added.

    Attributes
        scheduler_name (str): Name of a `~jobshop.sim.scheduler.Scheduler`
            subclass that is used to for rescheduling purposes.

            Can be change at runtime. Is used for every new reschedule that is
            triggered after it was changed (even if a watch dog that started
            earlier triggered the rescheduling).
        trigger_interval (int): The interval in which the priority rule should
            be applied to all remaining operations.

            Can be changed at runtime.
        stop_after_data_exhausted (bool): Stops replanning after the
            environment signaled that no new data will be added.

            Can be changed at runtime.

    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scheduler_name = kwargs.get('scheduler_name', 'Ziaee')
        self.trigger_interval = kwargs.get('trigger_interval', 480)
        self.stop_after_data_exhausted = kwargs.get('stop_after_data_exhausted', False)
        self._replan_proc = None
        self._data_exhausted = False
        self._triggered_ones = False
        self._continue_replanning = True

    @staticmethod
    def arguments():
        return {
            'trigger_interval': {
                'name': 'Trigger interval (in minutes)',
                'default': 480,
                'type': int
            },
            'scheduler_name': {
                'name': 'Scheduler',
                'enum': SchedulerFactory.available_schedulers()
            },
            'stop_after_data_exhausted': {
                'name': 'Stop replanning after all jobs are distributed.',
                'default': False,
                'type': bool
            }
        }

    def _schedule_added_hook(self, schedule):
        # Add a process to regularly replan the current queue of remaining
        # operations.
        if self._replan_proc is None:
            self._replan_proc = self._env.process(self._replan())

    def trigger_data_exhausted(self):
        self._data_exhausted = True
        if self.stop_after_data_exhausted:
            self._continue_replanning = False

    def _replan(self):
        """A process in the simpy.Environment that replans after a specified
        time interval.
        """
        while self._continue_replanning or not self._triggered_ones:
            self._triggered_ones = True
            # Wait for the specified trigger interval and replan based on the
            # specified scheduler.
            yield self._env.timeout(self.trigger_interval)
            # Terminate if no more operations are queued. This can happen if
            # the executor is waiting for new operations added via a schedule,
            # or if no new data will be added to the executor.
            ops = self._states.queued_operations()
            if len(ops) == 0:
                break
            self._log("replanning triggered at %d. Number of queued operations: %d." % (self._env.now, len(ops)))

            # Rescheduling phase
            scheduler = SchedulerFactory.get_scheduler(self.scheduler_name)
            if self.scheduler_name == "Ziaee":
                new_schedule = scheduler.create_schedule(ops, (4, 1, 0, -1, 0, -1, 0, 0))
            else:
                new_schedule = scheduler.create_schedule(ops)
            self.reschedule(new_schedule)
        self._replan_proc = None
