# -*- coding: utf-8 -*-
import simpy
import logging

from jobshop.sim.executor import Executor
from jobshop.sim.model import Schedule


class MachineTimeout(Executor):
    """An `~jobshop.sim.executor.Executor` subclass that implements a
    communication pattern in which the different building parts (operations) in
    the manufacturing process can communicate with machines (resources) they
    are scheduled on i.e. the resource has knowledge about the currently
    processing operation as well as all queued ones.

    With this knowledge the machine can reschedule operations.

    A machine tries to trigger a rescheduling if there are still operations
    queued on the machine, but it has to wait for an operation that is scheduled
    to be processed next. If the wait time exceeds a specified `timeout` value,
    the machine can try to find an operation in its that can be processed
    immediately.

    Additionally to the arguments of the parent
    `~jobshop.sim.executor.Executor` class, the following optional keyword
    arguments can be provided.

    Args:
        timeout (int): Duration in simulation time after which a rescheduling
            evaluation on a resource is triggered. (Default: **240**, which is
            half a day).
        watchdog_autorestart (boolean): Indicates whether the watch dog should
            automatically restart after it has been triggered, opposed to only
            being started after an operation has been finished (Default: True).

    Attributes:
        timeout (int): Simulation time after which a rescheduling evaluation on
            a resource is triggered. (Default: **240**, which is half a day).
            Will be used whenever a new watch dog is started.

            It therefore can be changed at runtime.

            A new watch dog is started a new operation is queued or a resource
            finished processing an operation. The watch dog is only started if
            at least one operation is queued.
        watchdog_autorestart (boolean): If set to `True`, a resource`s watch
            dog will restart automatically once it timed out. If set to `False`
            the watch dog is only triggered once and can only be started again
            if called for explicitly.

            Can be changed at runtime.

    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._resource_wd_event = {}
        self._resource_wd_enabled = {}
        self.timeout = kwargs.get('timeout', 240)
        self.watchdog_autorestart = kwargs.get('watchdog_autorestart', True)

    @staticmethod
    def arguments():
        return {
            'timeout': {
                'name': 'Timeout',
                'type': int,
                'default': 240
            },
            'watchdog_autorestart': {
                'name': 'Watchdog Auto-restart',
                'type': bool,
                'default': True
            }
        }

    def _reschedule_operations(self, res, ops):
        """Reschedules the specified list of operations on the resource
        """
        self._log("Trying to reschedule operations of resource %s: %s" % (res, [op.name for op in ops]), level=logging.DEBUG)

        # for each of the operations find out if their preceding operations
        # have been processed already. Only in this case can we consider this
        # operation for being executed next.
        independ_ops = []
        for op in ops:
            # try to figure out dependency.
            pred = op.predecessor()
            # Operations without predecessor are automatically independent and
            # can be immediately processed.
            if pred is None:
                independ_ops.append(op)
            # Operations which predecessor is already processed have no
            # dependency and can be processed immediately.
            elif pred in self._states.done_operations():
                independ_ops.append(op)

        # If no operation can be preferred, rescheduling not necessary, it would
        # lead to the same schedule.
        if len(independ_ops) == 0:
            self._log("No operation ca be preferred. Nothing to reschedule.", level=logging.DEBUG)
            return

        # Create schedule from all operations preferring the ones that can be
        # executed immediately.
        self._log("Found Independent Operations that can be preferred: %s" % ([op.name for op in independ_ops]), level=logging.DEBUG)
        new_sched = Schedule()
        remaining_ops = [op for op in ops if op not in independ_ops]
        for op in independ_ops + remaining_ops:
            # Preserve the old planned start and finish
            opProc = self.operation_process(op)
            new_sched.append(op, res, start=opProc.start, finish=opProc.finish)
        self.reschedule(new_sched)

    # --------------------------------------------------------------------------
    # WATCHDOG
    # --------------------------------------------------------------------------
    def _resource_watchdog_process(self, res):
        """The process that is added to the simulation environment.

        It takes care of triggering the watch dog once it is timed out.
        The process can also be interrupted to prevent the watch dog from
        firing.

        If `watchdog_autorestart` is set to true, the watch dog restarts
        automatically once it was triggered (but not if it was interrupted).

        """
        try:
            yield self._env.timeout(self.timeout)
            self._trigger_resource_watchdog(res)
        except simpy.Interrupt:
            self._log("Watch dog: stopped for resource %s." % (res))
        else:  # Auto restart watch dog.
            if self.watchdog_autorestart:
                self._start_resource_watchdog(res)

    def _trigger_resource_watchdog(self, res):
        """Executed if the watch dog triggered after the specified time out.

        Gathers all queued operations on the resource and reschedules them if
        a rescheduling looks promising.

        """
        self._log("Watch Dog: resource %s timed out." % (res), level=logging.DEBUG)

        # Get all queued operations on the resource.
        ops = self._states.queued_operations()
        res_ops = []
        for op in ops:
            opProc = self.operation_process(op)
            if opProc and opProc.resource == res:
                res_ops.append(op)

        # It does not really make sense to reschedule only one operation.
        if len(res_ops) > 1:
            self._reschedule_operations(res, res_ops)

    def _start_resource_watchdog(self, res):
        """Start a watch dog for a given resource.

        Once the specified time out is reached, the method
        `_trigger_resource_watchdog()` is triggered.

        The watch dog can be disabled s.t. no new watch dog is started.
        This is used for preventing the watch dog to fire while it is processing
        an operation.

        Furthermore a watch dog is only started if at least one operation is
        queued for the resource.

        """
        # Only start a watch dog if at least one resource is in the queue.
        # The watch dog would be useless otherwise, since the queue of the
        # resource cannot be rearranged anyway.
        # Furthermore, the watch dog is not triggered if it was previously
        # disabled via `_disable_resource_watchdog()`.
        # Finally, a watch dog is only started if no watch dog is running.
        watchdog_enabled = self._resource_wd_enabled.get(res, True)
        ev = self._resource_wd_event.get(res)
        if (self.resource_queue_length(res) > 0 and
                watchdog_enabled and
                (ev is None or ev.triggered)):
            self._log("Watch Dog: started for resource %s." % (res))
            self._resource_wd_event[res] = self._env.process(self._resource_watchdog_process(res))

    def _stop_resource_watchdog(self, res):
        """Interrupts the watch dog of the specified resource and stops it
        from firing after it timed out
        """
        # The first operation that requests the resource is not put in a queue
        # but directly assigned to the resource.
        # Therefore, start_resource_watchdog does not create a watch dog process
        # which results in an key error here, since the loop immediately tries
        # to stop the watch dog. This is not a problem and just a bootstrapping
        # problem.
        try:
            res_event = self._resource_wd_event[res]
        except KeyError:
            pass
        else:
            if not res_event.triggered:
                res_event.interrupt()

    def _disable_resource_watchdog(self, res):
        """Disables the watch dog of a resource and prevents it from being
        started again until `_enable_resource_watchdog()` is triggered.

        This is used when a resource starts processing an operation.
        """
        self._resource_wd_enabled[res] = False

    def _enable_resource_watchdog(self, res):
        """(Re-)enables a watch dog for a given resource which makes it
        possible to start a watch dog. This is for example used when a resource
        finished.
        """
        self._resource_wd_enabled[res] = True

    # --------------------------------------------------------------------------
    # SUBCLASS HOOKS
    # --------------------------------------------------------------------------
    def _queued_hook(self, stuple, res):
        self._start_resource_watchdog(res)

    def _running_hook(self, stuple, res, start):
        self._disable_resource_watchdog(res)
        self._stop_resource_watchdog(res)

    def _done_hook(self, stuple, res, start, finish):
        self._enable_resource_watchdog(res)
        self._start_resource_watchdog(res)
