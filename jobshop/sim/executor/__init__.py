# -*- coding: utf-8 -*-
"""This sub-package includes the `~jobshop.sim.executor.base_class.Executor`
base class as well as sub-classes that make up the list of available executor
strategies.

Executor base class module and shared components can be found here:
    - `~jobshop.sim.executor.base_class`

At the moment the following executor strategies are available:

Executor sub-classes sorted by their communication capabilities
    - `~jobshop.sim.executor.no_communication.NoCommunication`
    - `~jobshop.sim.executor.machine_timeout.MachineTimeout`
    - `~jobshop.sim.executor.operation_timeout.OperationTimeout`
    - `~jobshop.sim.executor.operation_communication.OperationCommunication`
    - `~jobshop.sim.executor.operator_communication.OperatorCommunication`

Additionally, the sub-package provides a factory class:
`~jobshop.sim.executor.executor_factory.ExecutorFactory` to help create and
initialize specific executor objects by their name.

"""
from .base_class import Executor, NoAlternativeException, NoDependencyException
from .executor_factory import ExecutorFactory
from .no_communication import NoCommunication
from .machine_timeout import MachineTimeout
from .operation_timeout import OperationTimeout
from .operation_communication import OperationCommunication
from .operator_communication import OperatorCommunication
