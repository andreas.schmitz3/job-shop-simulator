# -*- coding: utf-8 -*-
from jobshop.sim.executor import Executor


class ExecutorFactory:
    """The `ExecutorFactory` allows to create and initialize
    `~jobshop.sim.model.executor.Executor` objects and its sub-classes by
    their class name. It is a completely static class.

    A list of all available executors can be retrieved via
    `ExecutorFactory.available_executors()`. To create a new executor object, use the
    `ExecutorFactory.create_executor()` method.

    """
    @staticmethod
    def available_executors():
        """Allows retrieving a list of all executor names.
        Every entry in the list can be used to generate a executor with the
        `ExecutorFactory.create_executor()` function.

        Returns:
            list: A list of all available executor names (str).

        """
        queue = Executor.__subclasses__()
        executors = []
        while len(queue) > 0:
            ExecClz = queue.pop()
            for SubClz in ExecClz.__subclasses__():
                queue.append(SubClz)
            executors.append(ExecClz.__name__)
        return executors

    @staticmethod
    def executor_arguments(class_name):
        """Executes the executors argument() method and returns the result.
        Convenience method s.t. it is not necessary to create an executor object
        with the `create_executor` method.

        Returns:
            dict: The result of the executors `arguments()` method.

        Raises:
            ValueError: if the ``class_name`` parameter does not match the name
                of a executor.

        """
        queue = Executor.__subclasses__()
        while len(queue) > 0:
            ExecClz = queue.pop()
            if class_name == ExecClz.__name__:
                return ExecClz.arguments()
            for SubClz in ExecClz.__subclasses__():
                queue.append(SubClz)

        raise ValueError("No Executor found for the provided string: %s." % (class_name))

    @staticmethod
    def create_executor(class_name, *args, **kwargs):
        """Creates an executor object for the provided (sub-)class name and
        returns it.

        Args:
            class_name (str): class name of a executor.
            *args (list): A list of positional arguments for the `Executor`
                class to be instantiated.
            **kwargs (dict): A dictionary of keyword arguments for the
                `Executor` class to be instantiated.

        Returns:
            An object of the `Executor` subclass that matches the provided
            ``class_name`` parameter.

        Raises:
            ValueError: if the ``class_name`` parameter does not match the name
                of a executor.

        """
        queue = Executor.__subclasses__()
        while len(queue) > 0:
            ExecClz = queue.pop()
            if class_name == ExecClz.__name__:
                return ExecClz(*args, **kwargs)
            for SubClz in ExecClz.__subclasses__():
                queue.append(SubClz)

        raise ValueError("No Executor found for the provided string: %s." % (class_name))
