# -*- coding: utf-8 -*-

from jobshop.sim.executor import OperationTimeout, NoAlternativeException


class OperationCommunication(OperationTimeout):
    """The `OperationCommunication` class behaves almost the same as the `OperationTimeout` class, but with the slight difference that the different operations (components) have the ability to communicate with the resources
    in the system.

    The resources on the other hand monitor their status and their performance.

    This is used to take other variables into account when making a
    rescheduling decision. At the moment the operation picks the resource that
    has the lowest estimated work remaining. More precisely it picks the
    resource that is estimated to finish the operation the earliest.

    This metric is determined by calculating the sum of the estimated
    processing time of all queued operations. To do so, the average performance
    of the resource (which is monitored at runtime) is used and combined with
    the given processing time of the operation.

    Additionally, the mean idling time of a resource is incorporated to give a
    better estimate of the time needed until the operation can be processed.

    The metric is also calculated for the resource it is currently queued on,
    only considering all the operations that come before it in the queue. A
    rescheduling only happens if the newly selected resource is
    estimated to be faster than the current one.

    The class has the same attributes as its parent class. See
    `OperationTimeout` for more information.

    """
    def _estimated_processing_time(self, operation, resource):
        """Calculates the estimated processing time of a given operation on a
        given resource.

        It uses the performance of the resource that is calculated / measured
        at runtime, and the pre-determined processing time of the operation on
        the resource.

        Args:
            operation (Operation): An operation.
            resource (Resource): A resource.

        Returns:
            float: the estimated processing time of the given operation on the
            given resource.

        """
        est_proc_time = operation.processing_time(resource)
        res_perf = self.runtime_resource_performance(resource)
        est_proc_time += int(est_proc_time * (1 - res_perf))
        return est_proc_time

    def _get_alternative_resource(self, operation, potential_resources):
        """For a list of potential other resources, get an alternative resource
        the operation can go queue to instead of waiting any longer.

        Args:
            operation (Operation): An operation.
            potential_resources (dict): A dictionary with potential `Resource`
                objects as keys, and the processing time for the operation as
                value.

        Returns:
            Resource: a new resource the operation can be queued on without a
            deadlock.

        Raises:
            NoAlternativeException: if there are no more alternatives the
                operation can be queued on.
            AssertionError: if the operation process of the operation could not
                be retrieved.

        """
        # Get the values for the current operation i.e. the OperationProcess
        # object and the resource it is currently scheduled on.
        cur_op_proc = self.operation_process(operation)
        assert cur_op_proc is not None, "The operation process for operation %s could not be retrieved." % operation
        current_resource = cur_op_proc.resource

        # Set the initial value of the estimated finish times to the estimated
        # time of the current operation on the corresponding resource.
        # This is always part of the metric and can therefore be done directly
        # here.
        cur_resource_est_finish_time = self._estimated_processing_time(operation, current_resource)
        resource_sum_est_finish_time = {res: self._estimated_processing_time(operation, res) for res in potential_resources}
        # Marker to exclude following ops on the same res.
        self_found = False
        for op in self._states.queued_operations():
            # get the resource of the operation and check if it is in the list
            # of potential resources. If that is not the case, ignore this round
            op_proc = self.operation_process(op)
            assert op_proc is not None, "The operation process for operation %s could not be retrieved." % op
            res = op_proc.resource
            if res not in resource_sum_est_finish_time:
                continue

            # Exclude all resources where scheduling to it could result in a
            # deadlock.
            # Since deadlock prevention in this communication scheme is not
            # possible, we only allow rescheduling an operation if it has no
            # more successors or if a resource's queue is empty.
            if operation != op and operation.has_successor():
                del resource_sum_est_finish_time[res]
                continue

            # For all remaining resources, add up the estimated processing
            # times of the operations that are queued on the resource.
            try:
                resource_sum_est_finish_time[res] += self._estimated_processing_time(op, res)
            except KeyError:
                pass

            # calculate the metric for the current resource, excluding all
            # queued operations that come after the operation of concern.
            self_found = self_found or (operation == op)
            if not self_found and current_resource == res:
                cur_resource_est_finish_time += self._estimated_processing_time(op, res)

        # Now the estimated idle time of the resource has to be incorporated
        # into the estimated finish time.
        for res, est_finish_time in resource_sum_est_finish_time.items():
            resource_sum_est_finish_time[res] += int(est_finish_time * self._mean_resource_idle_time(res))
        cur_resource_est_finish_time += int(cur_resource_est_finish_time * self._mean_resource_idle_time(current_resource))

        # Finally, we can select the resource with the smallest estimated finish
        # time for the operation. If that resource is faster than the current
        # one, a rescheduling is performed, otherwise, an
        # NoAlternativeException is thrown.
        try:
            fastest_res = min(resource_sum_est_finish_time, key=resource_sum_est_finish_time.get)
        except ValueError:
            # Occurs if the resource_sum_est_finish_time dictionary is empty.
            raise NoAlternativeException("There are no further alternative resources for operation %s." % operation)
        else:
            if resource_sum_est_finish_time[fastest_res] < cur_resource_est_finish_time:
                self._log("A new resource (%s) with an estimated processing time of %s has been found that is faster than the current one: %s" % (fastest_res, resource_sum_est_finish_time[fastest_res], cur_resource_est_finish_time))
                return fastest_res
            else:
                raise NoAlternativeException("No rescheduling: The fastest potential other resource: %s for operation %s has an higher or equal estimated finish time than the current resource." % (fastest_res, operation))
