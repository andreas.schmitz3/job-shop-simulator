# -*- coding: utf-8 -*-
from jobshop.sim.executor import Executor


class NoCommunication(Executor):
    """Implements the behavior of the base `~jobshop.sim.executor.Executor`
    class and, so far, does not add any other functionality.

    This simulates the very simple case in which a schedule / plan is executed
    to the point without reacting to real world influences.

    """
    pass
