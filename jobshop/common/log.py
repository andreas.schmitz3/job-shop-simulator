# -*- coding: utf-8 -*-
import logging
import os.path
from os import makedirs


def init_logger(level=logging.DEBUG, filename=None):
    """Initializes the logging system with the provided log level and the
    specified filename, where the log entries are stored.

    Args:
        level (int): A log level e.g. logging.DEBUG.
        filename (str): A filename including the path where the logfile
            is stored. This is optional. If set to none, no logfile is written.

    """
    logger = logging.getLogger()
    logger.setLevel(level)

    # Configure file logging
    if filename:
        if not filename.endswith('.log'):
            filename += '.log'

        # Handler which logs into a file
        fileFormatter = logging.Formatter(fmt='%(asctime)s %(filename)s line:%(lineno)d  %(levelname)-8s %(message)s')
        # Create folder if it does not exist
        p = os.path.dirname(filename)
        if p and not os.path.exists(p):
            makedirs(p)
        fileHandler = logging.FileHandler(filename=filename)
        fileHandler.setFormatter(fileFormatter)
        logger.addHandler(fileHandler)

    # Configure stdout / console logging
    # Handler which writes logs to console
    conFormatter = logging.Formatter(fmt='%(filename)-14s line:%(lineno)-4d  %(levelname)-8s %(message)s')
    conHandler = logging.StreamHandler()
    conHandler.setFormatter(conFormatter)
    logger.addHandler(conHandler)


def _log_examples():
    logging.debug("Debug test")
    logging.info("Info test")
    logging.warn("Warn test")
    logging.critical("Critical test")
    logging.error("Error test")
