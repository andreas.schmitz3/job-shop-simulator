def chunk_list(items, num_chunks):
    """Splits a list into `num_chunks` chunks.

    Args:
        items (list): A list, or an iterable that supports `len()`.
        num_chunks (int): Then umber of chunks the iterable should be split
            into.

    Returns:
        list: A list of lists.

    """
    chunks = [[] for _ in range(min(num_chunks, len(items)))]
    for i, item in enumerate(items):
        chunks[i % num_chunks].append(item)
    return chunks
