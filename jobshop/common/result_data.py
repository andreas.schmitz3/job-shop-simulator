# -*- coding: utf-8 -*-
"""This module includes classes that represent the results of a simulation run.

See the documentation of the following three classes for further details.

Classes
    - `ResultTuple`
    - `SimulationResult`
    - `SimulationMetadata`

"""
from collections import namedtuple
import functools
import inspect
import statistics
from operator import attrgetter


class AnalyzeError(Exception):
    """Error Indicating that something went wrong with the Analyzer
    """
    pass


def check_simulation_metadata(method):
    """Decorator used to decorate a metric function of the `SimulationResult`
    class.

    The decorator ensures that the `simulation_metadata` attribute of the
    `SimulationResult` is properly set with the `SimulationMetadata` object of
    the corresponding `SimulationEnvironment` that executes the simulation.

    It is used for all performance metrics that need said metadata object.

    In case the attribute is not set properly, it raises an
    `~jobshop.common.errors.AnalyzeError`.

    """
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        if not self.simulation_metadata:
            raise AnalyzeError("Simulation metadata is not yet set, cannot calculate %s " % method.__name__)
        return method(self, *args, **kwargs)
    return wrapper


def round_result(decimals):
    """Decorator used to decorate a metric function of the `SimulationResult`
    class.

    It is used to round the returned result to the provided number of decimals.

    """
    def actual_decorator(method):
        @functools.wraps(method)
        def wrapper(self, *args, **kwargs):
            val = method(self, *args, **kwargs)
            return round(val, decimals)
        return wrapper
    return actual_decorator


def cache_result(method):
    """Decorator used to decorate a metric function of the `SimulationResult`
    class.

    It is used to cache the result of a performance metric s.t. it is only
    calculated once.

    """
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        if hasattr(self, "_" + method.__name__):
            return getattr(self, "_" + method.__name__)
        val = method(self, *args, **kwargs)
        setattr(self, "_" + method.__name__, val)
        return val
    return wrapper


def check_failed(method):
    """Decorator used to decorate a metric function of the `Analyzer` class.

    The decorator is used to check if the set `SimulationResult` is marked as
    failed. In this case an `~jobshop.common.errors.AnalyzeError` is raised.

    Otherwise the wrapped method is executed without further ado.
    """
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        if self.is_failed():
            raise AnalyzeError("Simulation result failed.")
        else:
            return method(self, *args, **kwargs)
    return wrapper


class ResultTuple(namedtuple('ResultTuple', 'op start finish planned_start planned_finish')):
    """A class wrapper for a `namedtuple`.

    Used to indicate when an `~jobshop.sim.model.Operation` object started
    and finished on a resource. The attributes ``planned_start`` and
    ``planned_finish`` can be set to `None` if they were not available.

    .. TODO:: Add planned_resource. needs also schema adaption? -.-

    Example:
        >>> ResultTuple(op=operation, start=0, finish=0, planned_start=None, planned_finish=None)

    """
    pass


class SimulationResult:
    """The `SimulationResult` object is used to store, for each name of a
    `~jobshop.sim.model.Resource` object, a sequence of
    `~jobshop.sim.model.Operation` objects (their names) with start and
    finish time and an optional planned start and finish time.

    Additionally it allows the `~jobshop.sim.executor.Executor` objects to
    store meta data collected during the run of the simulation
    (`metadata`).

    It also carries a reference to the `SimulationMetadata` object that is
    maintained by the `~jobshop.sim.environment.SimulationEnvironment` which
    carries out the simulation (`simulation_metadata`).

    Finally, the class has methods for querying performance metrics, which are
    calculated based on the result of the simulation run.
    Since the results are cached, they should only be executed when the
    simulation finished and no new data is added to the schedule.

    Attributes:
        metadata (dict): A dictionary that contains the meta data of
            the simulation run. Can be used e.g. by the executor to store
            additional about the simulation run.
        simulation_metadata (SimulationMetadata): The metadata object maintained
            by the simulation environment and shared between all executor and
            simulation result objects.

    """
    def __init__(self):
        self._schedule = {}
        self.metadata = {}
        self.simulation_metadata = None

    @property
    def schedule(self):
        """dict: The dictionary that stores the list of `ResultTuple` objects
        for each `~jobshop.sim.model.Resource` object. **(read-only)**
        """
        return self._schedule

    def resources(self):
        """Get all resources for which operations were scheduled.

        The returned resources can be used to get the list of `ResultTuple`'s
        that were scheduled on the resource. Use the `resource_entries()`
        method for that.

        .. NOTE::
            Does not return a list, but a generator that yields the resources.

        Yields:
            Resource: The resources for which operations were scheduled.

        """
        for res in self._schedule:
            yield res

    def resource_entries(self, resource):
        """Returns the operations scheduled for a resource.

        Args:
            resource (Resource): A resource for which the scheduled operations
                should be returned.

        Returns:
            list: A list of `ResultTuple` objects that were scheduled to the
            provided resource.

        Raises:
            KeyError: if the provided resource cannot be found in the schedule.

        """
        return self._schedule[resource]

    def is_failed(self):
        """An check that is performed on the result schedule to guarantee that
        the results that are generated by the executors are valid.

        Returns:
            boolean: True if the simulation run for this simulation result
            failed, False otherwise.

        """
        if len(self._schedule) == 0:
            return True
        try:
            jobs = {}
            for res in self.resources():
                last_finish = 0
                for res_tuple in self.resource_entries(res):
                    if res_tuple.planned_start:
                        assert res_tuple.start >= res_tuple.planned_start

                    assert res in res_tuple.op.resources

                    try:
                        res_tuples = jobs[res_tuple.op.job]
                    except KeyError:
                        res_tuples = []
                        jobs[res_tuple.op.job] = res_tuples
                    res_tuples.append(res_tuple)

                    assert last_finish <= res_tuple.start
                    last_finish = res_tuple.finish

            for job, res_tuples in jobs.items():
                res_tuples.sort(key=attrgetter('op.sequence_number'))
                ops = [rt.op for rt in res_tuples]
                for op in job.operations:
                    assert op in ops
                for i in range(len(res_tuples) - 1):
                    assert res_tuples[i].finish <= res_tuples[i + 1].start

        except AssertionError:
            return True
        else:
            return False

    def append(self, resource, result_tuple):
        """Adds an operation to a resource in form of a `ResultTuple` object.

        Args:
            resource (Resource): The name of a resource jobs can be scheduled on
            result_tuple (ResultTuple): Operation, start and end time of the
                simulated operation.

        Raises:
            ValueError: if the start time of the `result_tuple` is less than
                the finish time of the previous added `ResultTuple`, or if the
                finish time of the `result_tuple` is less than or equal to the
                start time of it.

        Example:
            >>> sr = SimulationResult()
            >>> rt = ResultTuple(op=operation, start=0, finish=0)
            >>> sr.append("421337", rt)

        """
        # Check if the provided result_tuple has valid values.
        if result_tuple.finish <= result_tuple.start:
            raise ValueError("The provided finish time (%d) is less than or equal to the provided start time (%d)." % (result_tuple.finish, result_tuple.start))

        if resource not in self._schedule:
            self._schedule[resource] = []

        # Guarantees the proper order on a resource
        try:
            last_entry = self._schedule[resource][-1]
        except IndexError:
            pass
        else:
            if result_tuple.start < last_entry.finish:
                raise ValueError("The provided start time (%d) is less than the finish time (%s) of the previous entry." % (result_tuple.start, last_entry.finish))

        self._schedule[resource].append(result_tuple)

    def metrics(self):
        """Gets all performance metrics in a combined dictionary. This is a
        convenience method s.t. not every metric method needs to be called
        separately.

        For a metric to be part of the dictionary, its method has to start
        with **metric_** e.g. **metric_objective_time**.

        Returns:
            dict: A dictionary with all metrics as key and its value as value.

        Raises:
            AnalyzeError: if at least one of the performance metrics raises an
                `~jobshop.common.errors.AnalyzeError`.

                This can be the case if the `SimulationResult` returns `True`
                when calling `is_failed()` or if the `simulation_metadata`
                attribute is not set yet.

        """
        metrics = {}
        for name, func in inspect.getmembers(self, inspect.ismethod):
            if name.startswith("metric_"):
                metrics[name] = func()
        return metrics

    @check_failed
    @cache_result
    def metric_objective_time(self):
        """The time in which the last operation was finished in the simulation.

        Returns:
            int: The simulation time the last operation was finished.

        Raises:
            AnalyzeError: if the attribute the simulation result turns out to be
                failed.

        """
        # Get the max value from every machines last operation's finish time
        return max([self.schedule[m][-1].finish for m in self.schedule])

    @check_failed
    @check_simulation_metadata
    @round_result(2)
    @cache_result
    def metric_mean_job_flow_time(self):
        """The mean time a job needs from the moment it was entered into the
        simulation until it finished.

        Returns:
            float: the mean job flow time.

        Raises:
            AnalyzeError: if the attributes `simulation_metadata` is not set or
                the simulation result turns out to be failed.

        """
        job_flow_times = self._job_flow_times()
        return statistics.mean(job_flow_times.values())

    @check_failed
    @check_simulation_metadata
    @round_result(2)
    @cache_result
    def metric_stdev_job_flow_time(self):
        """The standard deviation of the job flow time. See
        `metric_mean_job_flow_time()` for more information.

        Returns:
            float: the standard deviation of the job flow time.

        Raises:
            AnalyzeError: if the attributes `simulation_metadata` is not set or
                the simulation result turns out to be failed.

        """
        mean_flow_time = self.metric_mean_job_flow_time()
        job_flow_times = self._job_flow_times()
        return statistics.stdev(job_flow_times.values(), mean_flow_time)

    def _job_flow_times(self):
        """Helper function that returns a dictionary with all jobs of the
        simulation as key and the duration it took the job from start to finish
        as value.
        """
        # Find the time the last operation of every job was finished.
        job_finish = {}
        for _, res_tuples in self.schedule.items():
            for res_tuple in res_tuples:
                try:
                    job = res_tuple.op.job
                    job_finish[job]
                except KeyError:
                    job_finish[job] = {}
                    job_finish[job] = res_tuple.finish
                else:
                    if job_finish[job] < res_tuple.finish:
                        job_finish[job] = res_tuple.finish

        job_flow_time = {}
        for job in job_finish:
            job_flow_time[job] = (job_finish[job] - self.simulation_metadata.job_entry_time(job))
        return job_flow_time

    @check_failed
    @round_result(2)
    @cache_result
    def metric_mean_operation_flow_time(self):
        """The mean time an operation needs from the moment it's dependencies
        were resolved to the point it is done.

        Returns:
            float: the mean operation flow time.

        Raises:
            AnalyzeError: if the attributes `simulation_metadata` is not set or
                the simulation result turns out to be failed.

        """
        operation_flow_times = self._operation_flow_times()
        return statistics.mean(operation_flow_times.values())

    @check_failed
    @round_result(2)
    @cache_result
    def metric_stdev_operation_flow_time(self):
        """The standard deviation of the operation flow time. See
        `metric_mean_operation_flow_time()` for more information.

        Returns:
            float: the standard deviation of the operation flow time.

        Raises:
            AnalyzeError: if the attributes `simulation_metadata` is not set or
                the simulation result turns out to be failed.

        """
        mean_operation_flow_time = self.metric_mean_operation_flow_time()
        operation_flow_times = self._operation_flow_times()
        return statistics.stdev(operation_flow_times.values(), mean_operation_flow_time)

    def _operation_flow_times(self):
        """Helper function that returns a dictionary with all operations of the
        simulation as key and the duration it took it from start to finish as
        value.
        """
        op_flow_times = {}
        for _, res_tuples in self.schedule.items():
            for res_tuple in res_tuples:
                op_flow_times[res_tuple.op] = res_tuple.finish - self.operation_unblock_time(res_tuple.op)
        return op_flow_times

    @check_failed
    @round_result(2)
    @cache_result
    def metric_mean_operation_wait_time(self):
        """The average time an operation has to wait for a resource after it's dependencies are resolved.

        Returns:
            float: the average operation wait time.

        Raises:
            AnalyzeError: if the attribute the simulation result turns out to be
                failed.

        """
        operation_wait_times = self._operation_wait_times()
        return statistics.mean(operation_wait_times.values())

    @check_failed
    @round_result(2)
    @cache_result
    def metric_stdev_operation_wait_time(self):
        """The standard deviation of the operation wait time. See
        `metric_mean_operation_wait_time()` for more information.

        Returns:
            float: the standard deviation of the operation wait time.

        Raises:
            AnalyzeError: if the attribute the simulation result turns out to be
                failed.

        """
        mean_operation_wait_time = self.metric_mean_operation_wait_time()
        operation_wait_times = self._operation_wait_times()
        return statistics.stdev(operation_wait_times.values(), mean_operation_wait_time)

    def _operation_wait_times(self):
        """Helper function that returns a dictionary with all operations of the
        simulation as key and the duration it had to wait for the resource once
        it dependencies were resolved as value.
        """
        op_flow_times = {}
        for _, res_tuples in self.schedule.items():
            for res_tuple in res_tuples:
                op_flow_times[res_tuple.op] = res_tuple.start - self.operation_unblock_time(res_tuple.op)
        return op_flow_times

    @check_failed
    @round_result(2)
    @cache_result
    def metric_mean_machine_idle_time_percentage(self):
        """Calculates the average percentage a resource is idling.

        Returns:
            float: The average resource workload in percentage [0-1].

        Raises:
            AnalyzeError: if the attribute the simulation result turns out to be
                failed.

        """
        # Get the last finish time of each resource and the cumulated sum of
        # all scheduled operations. From this the resource workload in
        # percentage is calculated.
        workload = {}
        for res, rtuples in self.schedule.items():
            timeSum = 0
            last = 0
            for rtuple in rtuples:
                timeSum += (rtuple.finish - rtuple.start)
                last = rtuple.finish
            workload[res] = timeSum / last
        # Calculate the average resource workload.
        return 1 - statistics.mean(workload.values())

    @check_failed
    @round_result(2)
    @cache_result
    def metric_mean_total_machine_idle_time(self):
        """Returns the mean of the duration that each resource stood idle
        because it had to wait for the next operation.

        Returns:
            float: the mean resource idle time.

        Raises:
            AnalyzeError: if the attribute the simulation result turns out to be
                failed.

        """
        idle_time = {}

        for res in self.resources():
            idle_time[res] = 0
            curr = 0
            entries = self.resource_entries(res)
            for op in entries:
                idle = op.start - curr
                idle_time[res] += idle
                curr = op.finish

        return sum(idle_time.values()) / len(idle_time)

    @check_failed
    @round_result(2)
    @cache_result
    def metric_average_queue_length(self):
        """Returns the average number of jobs in the resource queues.

        Returns:
            float: the average queue length of all resources.

        Raises:
            AnalyzeError: if the attribute the simulation result turns out to be
                failed.

        """
        return statistics.mean(
            self.mean_resource_queue_lengths.values()
        )

    @property
    def operation_unblock_times(self):
        """dict: A dictionary storing `~jobshop.sim.model.Operation` objects as
        key and the time (int) they got unblocked (i.e. the dependencies gets
        resolved and it could potentially be processed) as value
        **(read-only)**.
        """
        try:
            return self.metadata['operation_unblock_times']
        except KeyError:
            unblock_time_dict = {}
            self.metadata['operation_unblock_times'] = unblock_time_dict
            return unblock_time_dict

    def operation_unblock_time(self, operation):
        """The time the dependencies of the specified operation were resolved.

        Args:
            operation (Operation): An operation.

        Returns:
            int: simulation time the operation's dependencies were resolved and
                it get unblocked.

        Raises:
            KeyError: If no simulation time was noted for the specified
                operation, or no operation was entered at all.

        """
        return self.metadata['operation_unblock_times'][operation.name]

    def add_operation_unblock_time(self, operation, time):
        """Note the time a operation's dependencies got resolved.

        Args:
            operation (operation): An operation.
            time (int): The simulation time the operation got unblocked.

        """
        try:
            unblock_time_dict = self.metadata['operation_unblock_times']
        except:
            unblock_time_dict = {}
            self.metadata['operation_unblock_times'] = unblock_time_dict
        unblock_time_dict[operation.name] = time

    @property
    def mean_resource_queue_lengths(self):
        """dict: The average queue length of all resources (values) for a given
        list of simulation time unit the data was entered (key).
        """
        try:
            return self.metadata['mean_queue_lengths']
        except KeyError:
            mean_queue_lengths = {}
            self.metadata['mean_queue_lengths'] = mean_queue_lengths
            return mean_queue_lengths

    def add_mean_resource_queue_length(self, time, mean_len):
        """Note the average queue length of all resources for the given
        simulation time.

        Args:
            time (int): The current simulation time.
            mean_len (float): A job that entered the simulation.

        """
        try:
            mean_queue_lengths = self.metadata['mean_queue_lengths']
        except:
            mean_queue_lengths = {}
            self.metadata['mean_queue_lengths'] = mean_queue_lengths
        mean_queue_lengths[time] = mean_len


class SimulationMetadata:
    """A class used to collect meta data.

    This is used instead of storing the meta data directly in the
    `SimulationEnvironment` to allow for an easy transcoding of the data which
    allows an easy exchange via the network.

    A the moment only the time a job was entered to the simulation is stored.
    """
    def __init__(self):
        self._job_entry_times = {}

    @property
    def job_entry_times(self):
        """dict: A dictionary storing the different `~jobshop.sim.model.Job`
        objects as key and the time (int) they were added to the simulation
        via the job distributor as value **(read-only)**.
        """
        return self._job_entry_times

    def job_entry_time(self, job):
        """The time the specified job entered the simulation.

        Args:
            job (Job): A job.

        Returns:
            int: simulation time the job entered the simulation.

        Raises:
            KeyError: If no simulation time was noted for the specified job.

        """
        return self._job_entry_times[job]

    def add_job_entry_time(self, job, time):
        """Note the time a job entered the simulation.

        Args:
            job (Job): A job that entered the simulation.
            time (int): The simulation time the job entered the simulation.

        """
        self._job_entry_times[job] = time
