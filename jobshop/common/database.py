# -*- coding: utf-8 -*-
"""The single class `CommonDatabase` of this module acts as a layer between the
SQLite library that stores all the job-shop and simulation related data.

For further description see the documentation of the class itself.

"""
import sys
import sqlite3  # DB-API for Sqlite3
import os
import shutil
from collections import OrderedDict
import json
import pkg_resources
import threading
import tempfile
import secrets

from jobshop import __version__
from jobshop.sim.model import Job, Operation, Resource
from jobshop.common.security import pwd_context


class CommonDatabase:
    """Common Database that establish a sqlite3 connection to a specified
    sqlite database.

    Automatically creates the database and loads the schema, if the database
    is not available.

    Allows for querying, inserting and deleting job and simulation related data.

    Args:
        database_file (str): A filename to a database file (including path).
            ":memory:" is not supported due to different threads sharing a
            single database.

            If the file does not exist yet, it is created and the schema is
            loaded into the database.

    Example:
        >>> cd = CommonDatabase("/tmp/sim-db.sqlite3")

    """
    _dummy_user = "dummy"
    _pool_conns = {}

    def __init__(self, database_file=None):
        if not database_file:
            self._tempfolder = tempfile.mkdtemp()
            database_file = os.path.join(self._tempfolder, 'temp-db.sqlite3')
        self._database_file = database_file

        # Create dir if it does not yet exist.
        dname = os.path.dirname(self.database_file)
        if dname and not os.path.exists(dname):
            os.makedirs(dname)

        # check if the file already exist. if that is not the case, the
        # database is created automatically by the sqlite module and the
        # schema is loaded afterwards.
        if not os.path.isfile(self.database_file):
            self._load_schema()
            # now that the script is loaded, we can create a dummy user
            # that is needed for the `user_credentials()` method to work
            # properly.
            dummy_secret = secrets.token_hex(64)
            hashed_secret = pwd_context.hash(dummy_secret)
            self.create_user(
                CommonDatabase._dummy_user,
                hashed_secret
            )
            # Furthermore we add a default user account, to reduce the
            # bootstrapping time for the user.
            default_user = "fjssp"
            default_password = "fjssp$2017&RWTH"
            default_hashed_password = pwd_context.hash(default_password)
            self.create_user(
                default_user,
                default_hashed_password
            )

    def __del__(self):
        if hasattr(self, "_tempfolder"):
            shutil.rmtree(self._tempfolder)

    def _load_schema(self):
        """Loads the schema into the connected SQLite database.
        """
        con = sqlite3.connect(self.database_file)
        schema = pkg_resources.resource_filename('jobshop', "share/schema/simulations.sql")
        with open(schema) as fp:
            c = con.cursor()
            c.executescript(fp.read())
            c.close()

    def _get_connection(self):
        """Get a connection for the current thread.

        This is separated by database object. E.g. if more than one database
        object exists, there is one connection per thread per database object.
        """
        thread_id = threading.current_thread().ident
        try:
            con = self._pool_conns[(thread_id, self)]
        except KeyError:
            con = sqlite3.connect(self.database_file)
            con.row_factory = sqlite3.Row
            # Activate foreign key support for the connection.
            con.execute("PRAGMA foreign_keys = 1")
            self._pool_conns[(thread_id, self)] = con
        return con

    def _select_query(self, query, query_parameters=None):
        """A simple wrapper that handles SQLite select queries.

        Args:
            query (str): The database query with "?" where the parameters will
                be filled in.
            query_parameters (list): A list of string convertible elements.

        Returns:
            list: A list of `sqlite3.Row` objects that contain the retrieved
            data.

        """
        c = self._get_connection().cursor()
        if query_parameters:
            data = c.execute(query, query_parameters).fetchall()
        else:
            data = c.execute(query).fetchall()

        c.close()

        return data

    @property
    def database_file(self):
        """str: The file where the database is stored. Used to create a sqlite
        connection from. **(read-only)**
        """
        return self._database_file

    def user_credentials(self, username):
        """Queries the database for the hashed passwort of the user.

        The database is queried for the provided username and a dummy account.
        Only one hashed_password is returned. This tries to make user
        enumeration timing attacks harder.

        Args:
            username (str): The username for which the hashed password is
                requested.

        Returns:
            str: The hashed password of the queried user, or a dummy password
            if the user does not exist in the database. This tries to make
            timing attacks to enumerate user accounts harder.

        Raises:
            RuntimeError: if retrieving the user's hashed password failed.

        """
        try:
            c = self._get_connection().cursor()
            data = c.execute('SELECT password FROM user WHERE name=? or name=? ORDER BY id DESC LIMIT 1', [username, CommonDatabase._dummy_user]).fetchone()
            c.close()
            return data['password']
        except (sqlite3.OperationalError, TypeError) as e:
            raise RuntimeError("Retrieving the user's password from the database failed. Error: %s" % e)

    def create_user(self, username, password):
        """Creates a new user in the database using the provided hashed password
        and the username.

        If the username already exists, an exception is thrown.

        Args:
            username (str): The login name of the account to be created.
            password (str): The hashed password of the account.

        Returns:
            int: The user id of the newly created account.

        Raises:
            RuntimeError: if there is already an existing entry in the database
                for the specified username.

        """
        try:
            con = self._get_connection()
            cursor = con.cursor()
            params = {
                "name": username,
                "password": password
            }
            cursor.execute("INSERT INTO user (name, password) VALUES (:name, :password)", params)
            user_id = cursor.lastrowid

            con.commit()
            cursor.close()
            return user_id
        except Exception as e:
            con.rollback()
            raise RuntimeError("Creating a new user account failed. Error: %s" % e)

    def update_user_password(self, username, password):
        """Updates the password of an user account in the database.

        Args:
            username (str): The login name of the account to be updated.
            password (str): The new hashed password of the account.

        Raises:
            RuntimeError: if an database error occurred the user could not be
                found.

        """
        try:
            con = self._get_connection()
            cursor = con.cursor()
            params = {
                "name": username,
                "password": password
            }
            cursor.execute("UPDATE user SET password=:password WHERE name=:name ", params)
            if cursor.rowcount == 0:
                raise RuntimeError("There is no account '%s'." % username)

            con.commit()
            cursor.close()
        except Exception as e:
            con.rollback()
            raise RuntimeError("Updating the password of an user account failed. Error: %s" % e)

    def delete_user(self, username):
        """Deletes a new user in the database

        If the username already exists, an exception is thrown.

        Args:
            username (str): The login name of the account to be deleted.

        Raises:
            RuntimeError: if deleting the user was not possible.

        """
        try:
            con = self._get_connection()
            cursor = con.cursor()
            cursor.execute("DELETE FROM user WHERE name=?", [username])
            if cursor.rowcount == 0:
                raise RuntimeError("There is no account '%s'." % username)

            con.commit()
            cursor.close()
        except Exception as e:
            con.rollback()
            raise RuntimeError("Deletion of user account failed. Error: %s" % (e))

    def all_users(self):
        """Retrieves a list of all registered user accounts.

        Returns:
            list: A list of all registered user accounts, except the dummy user.

        Raises:
            RuntimeError: if a database error occurred.

        """
        try:
            con = self._get_connection()
            cursor = con.cursor()

            cursor.execute("SELECT name FROM user")
            users = []
            for row in cursor.fetchall():
                if row['name'] == CommonDatabase._dummy_user:
                    continue
                users.append(row['name'])

            cursor.close()
            return users
        except Exception as e:
            raise RuntimeError("Checking the database for a username failed. Error: %s" % e)

    def user_exists(self, username):
        """Checks if a user for the given username already exists.

        Args:
            username (str): The login name of the account to be created.

        Returns:
            bool: `True` if an account for the specified username already exists
            in the database, `False` otherwise.

        Raises:
            RuntimeError: if a database error occurred.

        """
        try:
            con = self._get_connection()
            cursor = con.cursor()

            cursor.execute("SELECT * FROM user WHERE name = ?", [username])
            if len(cursor.fetchall()) > 0:
                exists = True
            else:
                exists = False

            cursor.close()
            return exists
        except Exception as e:
            raise RuntimeError("Checking the database for a username failed. Error: %s" % e)

    def all_job_names(self, shop_id):
        """Returns all different job names from the database. The names can be
        used to query the `jobs_resources_for_names()` function for
        `~jobshop.sim.model.Job` and `~jobshop.sim.model.Resource` objects.

        Returns:
            list: a list of unique job names (str) that can be used to query
            `~jobshop.sim.model.Job` and `~jobshop.sim.model.Resource` objects
            from the `jobs_resources_for_names()` method.

        Raises:
            RuntimeError: if querying for all names was not successful.

        Examples:
            >>> jd = CommonDatabase(":memory:")
            >>> jd.all_job_names(1)
            [
                '1', '2', '3'
            ]

        """
        try:
            rows = self._select_query('SELECT name FROM job WHERE shop_id = ? ORDER BY id', [shop_id])
        except sqlite3.OperationalError as e:
            raise RuntimeError("Not possible to retrieve all job names from the database. Error: %s" % e)
        data = []
        for row in rows:
            data.append(row['name'])
        return data

    def jobs_resources_for_names(self, shop_id, names):
        """Creates `~jobshop.sim.model.Job` and
        `~jobshop.sim.model.Operation` objects for the given list of job names
        and returns a list ob `~jobshop.sim.model.Job` objects and a list of
        resources that can be used to process the different
        `~jobshop.sim.model.Operation` objects of the `~jobshop.sim.model.Job`
        objects.

        Args:
            names (list): A list of job names that occur in the database file
                specified by the ``dbfile`` argument of the class constructor.

        Returns:
            A tuple consisting of a list of `~jobshop.sim.model.Job` objects
            and a combined list of `~jobshop.sim.model.Resource` objects that
            the different `~jobshop.sim.model.Operation` objects can be
            processed on.

        Raises:
            RuntimeError: if querying for the names was not successful.

        Examples:
            >>> jd = CommonDatabase(":memory:")
            >>> jd.jobs_resources_for_names(shop_id=0, names=[2, 1])
            (
                [
                    Job("1", ""),
                    Job("2", "")
                ],
                [
                     Resource("1", "Gravieren"),
                     Resource("2", "Hauptlager"),
                     Resource("3", "Sandstrahlerei"),
                     Resource("4", "Einlagern"),
                     Resource("5", "Laserschweißen"),
                     Resource("6", "Fertigung - Dickschichtheizungen")
                ]
            )

        """
        if not names:
            return [], []

        try:
            # Since SQLite has a limit to the maximum number of variables in a
            # query, we split the request up in chunks of 100 variables.
            # This seems not to be a performance problem, but is kind of a hack.
            num_names = len(names)
            start_index = 0
            query_max_var_num = 100
            rows = []
            while num_names > 0:
                part_len = min(num_names, query_max_var_num)
                num_names -= part_len
                placeholder = "?," * (part_len - 1)
                placeholder = "(" + placeholder + "?)"
                params = [shop_id] + names[start_index:start_index + part_len]
                rows += self._select_query('SELECT job_name, job_description, sequence_number, operation_description, resource_name, resource_description, performance_mean, performance_stddev, processing_time FROM joined_jobs WHERE shop_id = ? and job_name in %s ' % placeholder, params)
                start_index += part_len
        except sqlite3.OperationalError:
            raise RuntimeError("The data for the provided list of job names could not be retrieved.")

        jobs = OrderedDict()
        resources = OrderedDict()
        operations = OrderedDict()
        for row in rows:
            # Extract the resources
            res_name = row['resource_name']
            try:
                res = resources[res_name]
            except KeyError:
                perf_mean = row['performance_mean']
                if perf_mean is not None:
                    perf_mean = float(perf_mean)

                perf_stddev = row['performance_stddev']
                if perf_stddev is not None:
                    perf_stddev = float(perf_stddev)

                res = Resource(
                    name=res_name,
                    description=row['resource_description'],
                    performance_mean=perf_mean,
                    performance_stddev=perf_stddev
                )
                resources[res_name] = res

            # Extract and create the jobs
            job_name = row['job_name']
            try:
                job = jobs[job_name]
            except KeyError:
                job = Job(
                    name=job_name
                )
                jobs[job_name] = job

            # Extract and create the operations
            seq_num = int(row['sequence_number'])
            op_name = (job_name, seq_num)
            try:
                op = operations[op_name]
            except KeyError:
                op = Operation(
                    sequence_number=seq_num,
                    job=job,
                    description=row['operation_description']
                )
                job.operations.append(op)
                operations[op_name] = op

            # Add the processing times to the operations
            proctime = int(row['processing_time'])
            op.add_resource(res, proctime)
        assert len(jobs.values()) == len(names)
        return list(jobs.values()), list(resources.values())

    def all_shops(self, ascending=True):
        """Queries the database for all stored shops and returns the list
        of all shop ids.

        Args:
            ascending (boolean): Indicating whether the returned list should be
                sorted ascending (True) or descending (False).

        Returns:
            list: The list of ids of all shops in the database.

        Raises:
            RuntimeError: if retrieving the list of shops from the database
                failed.

        """
        order = "ASC"
        if ascending is False:
            order = "DESC"

        try:
            rows = self._select_query('SELECT id, name, description, seed FROM shop ORDER BY id %s' % order)
            data = []
            for row in rows:
                data.append({
                    'shop_id': row['id'],
                    'name': row['name'],
                    'description': row['description'],
                    'seed': row['seed']
                })

            return data
        except sqlite3.OperationalError:
            raise RuntimeError("The list of all available shops could not be retrieved.")

    def shop_for_id(self, shop_id):
        """Queries the database for a shop with the specified id.

        Args:
            shop_id (int): The database internal id of a shop.

        Returns:
            dict: A dictionary containing the data for the shop.

        Raises:
            RuntimeError: if retrieving the shop data from the database failed.

        """
        try:
            rows = self._select_query('SELECT id, name, description, seed FROM shop WHERE id = %s' % shop_id)
            assert len(rows) == 1

            data = {}
            for row in rows:
                data['shop_id'] = row['id']
                data['name'] = row['name']
                data['description'] = row['description']
                data['seed'] = row['seed']
            return data
        except (sqlite3.OperationalError, AssertionError):
            raise RuntimeError("The data for the specified job could not be retrieved.")

    def shop_exists(self, shop_id):
        """Checks if a shop can be found for the given shop_id.

        Args:
            shop_id (int): The database internal id of a shop.

        Returns:
            bool: `True` if a shop for the specified id exists in the database,
            `False` otherwise.

        Raises:
            RuntimeError: if a database error occurred.

        """
        try:
            con = self._get_connection()
            cursor = con.cursor()

            cursor.execute("SELECT * FROM shop WHERE id = ?", [shop_id])
            if len(cursor.fetchall()) > 0:
                exists = True
            else:
                exists = False

            cursor.close()
            return exists
        except Exception as e:
            raise RuntimeError("Checking the database for a shop failed. Error: %s" % e)

    def create_shop(self, jobs, resources, name="", description="", seed=None, remark=""):
        """Creates a new shop in the database based on the provided list of
        jobs and and resources.

        Args:
            jobs (list): A list of `~jobshop.sim.model.Job` objects.
            resources (list): A list of `~jobshop.sim.model.Resource` objects.
            name (str): The name of the new shop.
            description (str): A description for the new shop.
            seed (str, None): Used to store a seed that was used to create the
                shop data. (Only if the data was generated).
            remark (str): An internal remark for the shop that is not given out
                in any way and only stays as a remark in the database.

        Returns:
            int: the shop id of the newly created shop.

        Raises:
            RuntimeError: if the new shop could not be created. The internal
                state of the database is rolled back if this happens. So, if
                an exception is raised, it can be assumed that the internal
                state of the database before, and after the call of this
                function is the same.

        """
        try:
            cursor = self._get_connection().cursor()
            # Create new shop in database.
            cursor.execute("INSERT INTO shop (name, description, seed, remark) VALUES (?, ?, ?, ?)", (name, description, seed, remark))
            shop_id = cursor.lastrowid

            # Insert new resources.
            res_id_map = {}
            for res in resources:
                new_res = (
                    shop_id,
                    res.name,
                    res.description,
                    res.performance_mean,
                    res.performance_stddev
                )
                cursor.execute("INSERT INTO resource (shop_id, name,  description, performance_mean, performance_stddev) VALUES (?, ?, ?, ?, ?)", new_res)
                res_id_map[res.name] = cursor.lastrowid

            # Insert new jobs
            job_name_map = {}
            for job in jobs:
                cursor.execute("INSERT INTO job (shop_id, name, description) VALUES (?, ?, ?)", (shop_id, job.name, job.description))

                job_name_map[job.name] = cursor.lastrowid

                for op in job.operations:
                    cursor.execute("INSERT INTO operation (job_id, sequence_number) VALUES (?, ?)", (job_name_map[job.name], op.sequence_number))
                    op_id = cursor.lastrowid
                    for res in op.resources:
                        proctime = op.processing_time(res)
                        cursor.execute("INSERT INTO processing_time (operation_id, resource_id, time) VALUES (?, ?, ?)", (op_id, res_id_map[res.name], proctime))

            self._get_connection().commit()
            cursor.close()
            return shop_id
        except Exception:
            self._get_connection().rollback()
            raise RuntimeError("Writing the shop into the database failed.")

    def delete_shop(self, shop_id):
        """Deletes a shop, its jobs, resources, operations and all the
        simulations performed on it from the database.

        Args:
            shop_id (int): A database internal id used to identify a shop.

        Raises:
            RuntimeError: if an exception occurred during the deletion of the
                shop. The internal state of the database is rolled back
                if this happens. So, if an exception is raised, it can be
                assumed that the internal state of the database before, and
                after the call of this function is the same.

        """
        try:
            cursor = self._get_connection().cursor()

            cursor.execute("DELETE FROM shop WHERE id = ?", [shop_id])

            self._get_connection().commit()
            cursor.close()
        except sqlite3.OperationalError:
            raise RuntimeError("Deleting simulation with id %s went wrong." % shop_id)

    def insert_simulation(self, simulation):
        """Saves the results of a simulation run in the database.

        This is a convenience method for executing `prepare_new_simulation()``
        and  `fill_simulation()`.

        Args:
            simulation (SimulationEnvironment): A simulation environment after
                its `run()` method.

        Raises:
            RuntimeError: if an exception occurred during the insertion of the
                simulation. The internal state of the database is rolled back
                if this happens. So, if an exception is raised, it can be
                assumed that the internal state of the database before, and
                after the call of this function is the same.

        Returns:
            int: The internal id of the recently created simulation.

        """
        simulation_id = self.prepare_new_simulation(simulation)
        self.fill_simulation(simulation_id, simulation)
        return simulation_id

    def prepare_new_simulation(self, simulation):
        """Creates a new simulation in the database using the provided
        properties of the simulation.

        Args:
            simulation (SimulationEnvironment): A simulation environment after
                its `run()` method.

        Raises:
            RuntimeError: if an exception occurred during the insertion of the
                simulation. The internal state of the database is rolled back
                if this happens. So, if an exception is raised, it can be
                assumed that the internal state of the database before, and
                after the call of this function is the same.

        Returns:
            int: The internal id of the newly created simulation. The id can be
            used to add the simulation results later on.

        """
        try:
            cursor = self._get_connection().cursor()

            # Insert simulation run into database
            params = {
                "name": simulation.name,
                "shop_id": simulation.shop_id,
                "seed": str(simulation.seed),
                "status": "Running",
                "job_selector_strategy": simulation.job_selector,
                "job_selector_parameters": json.dumps(simulation.job_selector_parameters),
                "job_time_slice_strategy": simulation.job_time_slice,
                "job_time_slice_parameters": json.dumps(simulation.job_time_slice_parameters),
                "job_distribution_strategy": simulation.job_distribution,
                "job_distribution_parameters": json.dumps(simulation.job_distribution_parameters),
                "python_version": str(sys.version_info),
                "application_version": __version__
            }
            cursor.execute("INSERT INTO simulation (name, shop_id, seed, status, job_selector_strategy, job_selector_parameters, job_time_slice_strategy, job_time_slice_parameters, job_distribution_strategy, job_distribution_parameters, python_version, application_version) VALUES (:name, :shop_id, :seed, :status, :job_selector_strategy, :job_selector_parameters, :job_time_slice_strategy, :job_time_slice_parameters, :job_distribution_strategy, :job_distribution_parameters, :python_version, :application_version)", params)
            simulation_id = cursor.lastrowid

            self._get_connection().commit()
            cursor.close()
            return simulation_id
        except Exception as e:
            self._get_connection().rollback()
            raise RuntimeError("Writing the simulation into the database failed with error: %s " % e)

    def fill_simulation(self, simulation_id, simulation):
        """Saves the results of a simulation run for the provided simulation
        id to the database.

        Args:
            simulation_id (int): The id of a simulation, created via
                `prepare_new_simulation()`.
            simulation (SimulationEnvironment): A simulation environment after
                its `run()` method.

        Raises:
            RuntimeError: if an exception occurred during the insertion of the
                simulation results. The internal state of the database is
                rolled back if this happens. So, if an exception is raised, it
                can be assumed that the internal state of the database before,
                and after the call of this function is the same.

        """
        try:
            cursor = self._get_connection().cursor()
            # Insert the job_entry times of the simulation run into the
            # database.
            cursor.execute("INSERT INTO simulation_metadata (simulation_id, type) VALUES (?, ?)", (simulation_id, "job_entry_times"))
            meta_id = cursor.lastrowid
            job_entry_times = []
            for job, time in simulation.metadata.job_entry_times.items():
                job_entry_times.append({
                    "simulation_metadata_id": meta_id,
                    "job_name": job.name,
                    "shop_id": simulation.shop_id,
                    "entry_time": time
                })
            cursor.executemany("INSERT INTO simulation_metadata_entry (simulation_metadata_id, key, value) VALUES (:simulation_metadata_id, (SELECT id FROM job WHERE shop_id = :shop_id and name = :job_name), :entry_time)", job_entry_times)
            del job_entry_times

            # Insert simulation results from the different executors to the
            # database.
            for simtup in simulation.simulations:
                simentry = {
                    "simulation_id": simulation_id,
                    "scheduler_name": simtup.scheduler.name(),
                    "executor_name": simtup.executor.name(),
                    "scheduler_parameters": json.dumps(simtup.scheduler_parameters),
                    "executor_parameters": json.dumps(simtup.executor_parameters)
                }
                cursor.execute("INSERT INTO simulation_result (simulation_id, scheduler_name, executor_name, scheduler_parameters, executor_parameters) VALUES (:simulation_id, :scheduler_name, :executor_name, :scheduler_parameters, :executor_parameters)", simentry)
                simulation_result_id = cursor.lastrowid

                # Insert result entries of the simulation result into database.
                executor = simtup.executor
                for resource, restuples in executor.result.schedule.items():
                    for restuple in restuples:
                        result_entry = {
                            "simulation_result_id": simulation_result_id,
                            "shop_id": simulation.shop_id,
                            "resource_name": resource.name,
                            "job_name": restuple.op.job.name,
                            "sequence_number": restuple.op.sequence_number,
                            "start": restuple.start,
                            "finish": restuple.finish,
                            "planned_start": restuple.planned_start,
                            "planned_finish": restuple.planned_finish
                        }
                        cursor.execute("INSERT INTO result_entry (simulation_result_id, operation_id, resource_id, start, finish, planned_start, planned_finish) VALUES (:simulation_result_id, (SELECT id FROM operation WHERE job_id = (SELECT id FROM job WHERE shop_id = :shop_id and name = :job_name) and sequence_number = :sequence_number), (SELECT id FROM resource WHERE shop_id = :shop_id and name = :resource_name), :start, :finish, :planned_start, :planned_finish)", result_entry)

                # Insert the meta data of the simulation result into the
                # database.
                for d_type, d_dict in executor.result.metadata.items():
                    cursor.execute("INSERT INTO result_metadata (simulation_result_id, type) VALUES (?, ?)", (simulation_result_id, d_type))
                    meta_id = cursor.lastrowid
                    metadata_entries = []
                    for key, value in d_dict.items():
                        metadata_entries.append({
                            "result_metadata_id": meta_id,
                            "key": key,
                            "value": value
                        })
                    cursor.executemany("INSERT INTO result_metadata_entry (result_metadata_id, key, value) VALUES (:result_metadata_id, :key, :value)", metadata_entries)
                    del metadata_entries

                # Insert the performance metrics into the database.
                metrics = []
                for metric, value in executor.result.metrics().items():
                    valType = None
                    if type(value) == int:
                        valType = "int"
                    elif type(value) == float:
                        valType = "float"
                    metrics.append((
                        simulation_result_id,
                        metric,
                        valType,
                        value
                    ))
                cursor.executemany("INSERT INTO result_performance (simulation_result_id, key, type, value) VALUES (?, ?, ?, ?)", metrics)

            self._get_connection().commit()
            cursor.close()
            return simulation_id
        except Exception as e:
            self._get_connection().rollback()
            raise RuntimeError("Writing the simulation results into the database failed. Error: %s" % e)

    def set_simulation_status(self, simulation_id, status):
        """Sets the status of an simulation to Running, Finished, or Failed.

        Args:
            simulation_id (int): The id of a simulation, created via
                `prepare_new_simulation()`.
            status (str): The status the simulation should be set to. Valid
                values are: ``Running``, ``Finished`` and ``Failed``.

        Raises:
            RuntimeError: if updating the status of the simulation in the
                database failed.

        """
        try:
            cursor = self._get_connection().cursor()
            cursor.execute("UPDATE simulation SET status = ? WHERE id = ?", (status, simulation_id))
            self._get_connection().commit()
            cursor.close()
            return simulation_id
        except Exception:
            self._get_connection().rollback()
            raise RuntimeError("Updating the status of the simulation failed.")

    def all_simulations(self, ascending=True):
        """Queries the database for all stored simulations and returns the list
        of all simulation ids.

        Args:
            ascending (boolean): Indicating whether the returned list should be
                sorted ascending (True) or descending (False).

        Returns:
            list: The list of ids of all simulations in the database.

        Raises:
            RuntimeError: if an error with the database occurred and the list
                of all simulations could not be retrieved.

        """
        order = "ASC"
        if ascending is False:
            order = "DESC"

        try:
            rows = self._select_query('SELECT id, name, status, shop_id, creation_time FROM simulation ORDER BY id %s' % order)
            data = []

            for row in rows:
                data.append({
                    'simulation_id': row['id'],
                    'name': row['name'],
                    'status': row['status'],
                    'shop_id': row['shop_id'],
                    'creation_time': row['creation_time']
                })

            return data
        except sqlite3.OperationalError:
            raise RuntimeError("The list of all simulations could not be retrieved.")

    def simulation_for_id(self, simulation_id):
        """Returns the simulation data stored for in the database for a given
        id.

        Args:
            simulation_id (int): A database internal id of a simulation.

        Returns:
            dict: A dictionary containing the different elements that are stored
            in the database as key and their value as value.

        Raises:
            RuntimeError: if querying the database for the simulation id failed.

        """
        try:
            rows = self._select_query('SELECT name, shop_id, seed, job_selector_strategy, job_selector_parameters, job_time_slice_strategy, job_time_slice_parameters, job_distribution_strategy, job_distribution_parameters, creation_time FROM simulation WHERE id = ?', [simulation_id])
            assert len(rows) == 1
            data = {}
            for row in rows:
                data = dict(row)
                data['job_selector_parameters'] = json.loads(data['job_selector_parameters'])
                data['job_time_slice_parameters'] = json.loads(data['job_time_slice_parameters'])
                data['job_distribution_parameters'] = json.loads(data['job_distribution_parameters'])

            return data
        except (sqlite3.OperationalError, AssertionError):
            raise RuntimeError("No simulation for id %s found." % simulation_id)

    def delete_simulation(self, simulation_id):
        """Deletes a simulation with the specified id.

        Args:
            simulation_id (int): A database internal id used to identify one
                simulation.

        Raises:
            RuntimeError: if deleting the simulation with the specified id went
                wrong.

        """
        try:
            cursor = self._get_connection().cursor()

            cursor.execute("DELETE FROM simulation WHERE id = ?", [simulation_id])

            self._get_connection().commit()
            cursor.close()
        except sqlite3.OperationalError:
            raise RuntimeError("Deleting simulation with id %s went wrong." % simulation_id)

    def simulation_results(self, simulation_id):
        """The list of all simulation results of an specified simulation.
        This is a list of schedulers and executors that were simulated.

        Args:
            simulation_id (str): An id of a simulation in the database.

                Can for example be retrieved via `all_simulation_ids()`.

        Returns:
            list: A list of simulation results for the given ``simulation_id``.

        Raises:
            RuntimeError: if an error with the database occurred and the
                simulation results could not be retrieved.

        """
        try:
            rows = self._select_query('SELECT id, scheduler_name, executor_name, scheduler_parameters, executor_parameters FROM simulation_result WHERE simulation_id = ? ORDER BY id', [simulation_id])

            data = []
            for row in rows:
                sched_params = None
                exec_params = None
                if row['scheduler_parameters']:
                    sched_params = json.loads(row['scheduler_parameters'])
                if row['executor_parameters']:
                    exec_params = json.loads(row['executor_parameters'])

                data.append({
                    'simulation_result_id': row['id'],
                    'scheduler_name': row['scheduler_name'],
                    'executor_name': row['executor_name'],
                    'scheduler_parameters': sched_params,
                    'executor_parameters': exec_params
                })
            return data
        except sqlite3.OperationalError:
            raise RuntimeError("The list of simulation results for the specified simulation_id: %s could not be retrieved." % simulation_id)

    def simulation_result_performance_for_id(self, result_id):
        """Gets all the performance metrics for a given simulation result id

        Args:
            result_id (int): A database internal id for a simulation result.

        Returns:
            dict: A dictionary with the name of the performance metric as key
            and the value of the metric as value.

        Raises:
            RuntimeError: if querying the database for the performance metrics
                failed.

        """
        try:
            rows = self._select_query('SELECT key, type, value FROM result_performance WHERE simulation_result_id = ?', [result_id])

            metrics = {}
            for row in rows:
                value = row['value']
                if row['type'] == "int":
                    value = int(value)
                elif row['type' == "float"]:
                    value = float(value)
                metrics[row['key']] = value
            return metrics
        except sqlite3.OperationalError as e:
            raise RuntimeError("The simulation result metrics for id %s could not be retrieved. Error: %s" % (result_id, e))

    def simulation_result_for_id(self, result_id):
        """Returns the simulation result data that is stored in the database
        for the given id.

        This does not return a simulation result object. As of now this is not
        needed and would make the routine much more complicated since we had
        to retrieve the different model objects of the simulation result. This
        would require a mixture of collecting already existing model objects
        and creating new ones. This may change in the future.

        Args:
            result_id (int): The database row id, the simulation result is
                stored at.

        Returns:
            dict: A dictionary representing the simulation result.

                This can directly be encoded as JSON.

        Raises:
            RuntimeError: if an error occurred retrieving the data for the id.

        """
        try:
            rows = self._select_query('SELECT job_name, sequence_number, r.name as resource_name, operation_description, start, finish, planned_start, planned_finish FROM result_entry as re JOIN (SELECT j.name as job_name, op.id as operation_id, op.sequence_number as sequence_number, op.description as operation_description FROM operation as op JOIN job as j ON op.job_id = j.id) as qj ON qj.operation_id = re.operation_id JOIN resource as r ON re.resource_id = r.id WHERE simulation_result_id = ?', [result_id])

            data = {}
            data['metadata'] = {}
            data['schedule'] = {}
            schedule = data['schedule']
            for row in rows:
                res = row['resource_name']
                try:
                    resList = schedule[res]
                except KeyError:
                    resList = []
                    schedule[res] = resList
                resList.append({
                    "job": row['job_name'],
                    "description": row['operation_description'],
                    "sequence_number": row['sequence_number'],
                    "start": row['start'],
                    "finish": row['finish'],
                    "planned_start": row['planned_start'],
                    "planned_finish": row['planned_finish']
                })
            return data
        except sqlite3.OperationalError as e:
            raise RuntimeError("The simulation result entry for id %s could not be retrieved. Error: %s" % (result_id, e))

    def performance_metrics_for_simulation_ids(self, simulation_ids):
        """Returns the performance metrics for all simulation results of all
        given simulation ids.

        The returned data is hierarchically sorted, starting with the names
        of the performance metrics at the root, followed by the different
        strategies that were performed, and lastly a collection of values to the
        metric and strategy.

        Args:
            simulation_ids (list): A list of internal simulation ids for which a
                aggregated list of metric values should be returned.

        Returns:
            dict: A dictionary representing the performance metric data in a
            hierarchical structure.

        Raises:
            RuntimeError: if an error occurred retrieving the data for the ids.

        """
        try:
            placeholder = "?," * (len(simulation_ids) - 1)
            placeholder = "(" + placeholder + "?)"
            rows = self._select_query(
                'SELECT name, shop_id, key as performance_metric, value, scheduler_name, scheduler_parameters, executor_name, executor_parameters, seed FROM result_performance as resperf JOIN (SELECT * FROM simulation_result as sres JOIN  (SELECT id, name, seed, shop_id FROM simulation WHERE id IN %s ) as sim ON sres.simulation_id = sim.id) as tmp ON resperf.simulation_result_id = tmp.id' % placeholder,
                simulation_ids
            )

            parsed_data = {}
            for row in rows:
                try:
                    metric = parsed_data[row['performance_metric']]
                except KeyError:
                    metric = {}
                    parsed_data[row['performance_metric']] = metric

                key = (row['scheduler_name'], row['scheduler_parameters'], row['executor_name'], row['executor_parameters'])
                try:
                    storage = metric[key]
                except KeyError:
                    storage = []
                    metric[key] = storage
                storage.append((row['shop_id'], row['value'], row['seed'], row['name']))

            return parsed_data
        except sqlite3.OperationalError as e:
            raise RuntimeError("The performance metrics for the given simulation ids %s could not be retrieved. Error: %s" % (simulation_ids, e))
