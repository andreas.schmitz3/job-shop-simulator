BEGIN TRANSACTION;
CREATE TABLE shop (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    description TEXT,
    seed TEXT,
    remark TEXT  -- only used internally
);
CREATE TABLE job (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    shop_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    description TEXT,
    remark TEXT,  -- only used internally
    UNIQUE(shop_id, name),
    FOREIGN KEY(shop_id) REFERENCES shop(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE resource (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    shop_id INTEGER NOT NULL,
    description TEXT,
    performance_mean REAL,
    performance_stddev REAL,
    remark TEXT,  -- only used internally
    UNIQUE(shop_id, name),
    FOREIGN KEY(shop_id) REFERENCES shop(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE operation (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    job_id INTEGER NOT NULL,
    sequence_number INTEGER NOT NULL,
    description TEXT,
    UNIQUE(job_id, sequence_number),
    FOREIGN KEY(job_id) REFERENCES job(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE processing_time (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    operation_id INTEGER NOT NULL,
    resource_id INTEGER NOT NULL,
    time INTEGER NOT NULL,
    UNIQUE(operation_id, resource_id),
    FOREIGN KEY(operation_id) REFERENCES operation(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(resource_id) REFERENCES resource(id) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE simulation (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    shop_id INTEGER NOT NULL,
    seed TEXT NOT NULL,
    job_selector_strategy INTEGER NOT NULL,
    job_selector_parameters TEXT NOT NULL,
    job_time_slice_strategy INTEGER NOT NULL,
    job_time_slice_parameters TEXT NOT NULL,
    job_distribution_strategy INTEGER NOT NULL,
    job_distribution_parameters TEXT NOT NULL,
    status TEXT NOT NULL,
    python_version TEXT NOT NULL,
    application_version TEXT NOT NULL,
    creation_time INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY(shop_id) REFERENCES shop(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE simulation_result (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    simulation_id INTEGER NOT NULL,
    scheduler_name TEXT NOT NULL,
    executor_name TEXT NOT NULL,
    scheduler_parameters TEXT,
    executor_parameters TEXT,
    FOREIGN KEY(simulation_id) REFERENCES simulation(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE result_metadata (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    simulation_result_id INTEGER NOT NULL,
    type TEXT NOT NULL,
    FOREIGN KEY(simulation_result_id) REFERENCES simulation_result(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE result_metadata_entry (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    result_metadata_id INTEGER NOT NULL,
    key TEXT,
    value TEXT,
    FOREIGN KEY (result_metadata_id) REFERENCES result_metadata(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE result_entry (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    simulation_result_id INTEGER NOT NULL,
    operation_id INTEGER NOT NULL,
    resource_id INTEGER NOT NULL,
    start INTEGER NOT NULL,
    finish INTEGER NOT NULL,
    planned_start INTEGER,
    planned_finish INTEGER,
    FOREIGN KEY(simulation_result_id) REFERENCES simulation_result(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(operation_id) REFERENCES operation(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(resource_id) REFERENCES resource(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE result_performance (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    simulation_result_id INTEGER NOT NULL,
    key TEXT NOT NULL,
    type TEXT,
    value TEXT NOT NULL,
    UNIQUE(simulation_result_id, key),
    FOREIGN KEY(simulation_result_id) REFERENCES simulation_result(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE simulation_metadata (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    simulation_id INTEGER NOT NULL,
    type TEXT NOT NULL,
    FOREIGN KEY(simulation_id) REFERENCES simulation(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE simulation_metadata_entry (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    simulation_metadata_id INTEGER NOT NULL,
    key TEXT,
    value TEXT,
    FOREIGN KEY (simulation_metadata_id) REFERENCES simulation_metadata(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL
);
CREATE VIEW joined_jobs as
    SELECT
        j.shop_id as shop_id,
        j.name as job_name,
        j.description as job_description,
        op.sequence_number as sequence_number,
        op.description as operation_description,
        pt.resource_name as resource_name,
        pt.resource_description as resource_description,
        pt.performance_mean as performance_mean,
        pt.performance_stddev as performance_stddev,
        pt.time as processing_time
    FROM operation as op
    JOIN
        job as j
    ON op.job_id = j.id
    JOIN
           (SELECT
            r.name as resource_name,
            r.description as resource_description,
            r.performance_mean as performance_mean,
            r.performance_stddev as performance_stddev,
            pt.operation_id,
            pt.time
        FROM processing_time as pt
        JOIN
            resource as r
        ON pt.resource_id = r.id
        ) as pt
    ON pt.operation_id = op.id
    ORDER BY op.id;
COMMIT;
