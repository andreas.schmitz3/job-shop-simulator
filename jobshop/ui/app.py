# -*- coding: utf-8 -*-
"""This module contains only one function to create the
`~tornado.web.Application` object that takes care of the web application.

"""
from tornado.web import URLSpec, StaticFileHandler, Application
import os.path

from jobshop.ui.handler import IndexHandler, SimulationsHandler, SimulationsIDHandler, LoginHandler, LogoutHandler, ChartHandler, ShopsHandler, ShopHandler
from jobshop.ui.websocket import WSAPIHandler
from jobshop.ui.web_security import cookie_password


def make_app(simserver, cookie_location='.cookiesecret', debug=False):
    """Creates the Application which handles the web requests.

    Args:
        simserver (SimulationServer): A SimulationServer object which is
            handed to the request handlers.

    Returns:
        tornado.web.Application: A tornado application.
    """
    settings = {
        "static_path": os.path.join(os.path.dirname(__file__), "static"),
        "cookie_secret": cookie_password(cookie_location),
        "xsrf_cookies": True,
        "debug": debug,
        "template_path": os.path.join(os.path.dirname(__file__), "templates"),
        "login_url": "/login"
    }

    # Application:
    # global configuration, including routing
    # URLSpec objects (or tuples). Order matters
    # The capture regex groups are used as input parameters for the matching
    # HTTP methods
    return Application([
        URLSpec(
            r"/",
            IndexHandler,
            dict(simserver=simserver),
            name="home"),
        URLSpec(
            r"/login",
            LoginHandler,
            dict(simserver=simserver),
        ),
        URLSpec(
            r"/logout",
            LogoutHandler,
            name="logout"
        ),
        URLSpec(
            r"/simulations",
            SimulationsHandler,
            dict(simserver=simserver),
            name="simulations"),
        URLSpec(
            r"/simulations/(\d+$)",
            SimulationsIDHandler,
            dict(simserver=simserver)),
        URLSpec(
            r"/shops",
            ShopsHandler,
            dict(simserver=simserver),
            name="shops"),
        URLSpec(
            r"/shop/(\d*$)",
            ShopHandler,
            dict(simserver=simserver)),
        URLSpec(
            r"/charts/(\d+$)",
            ChartHandler,
            dict(simserver=simserver)),
        URLSpec(
            r"/ws",
            WSAPIHandler,
            dict(simserver=simserver)),
        URLSpec(r"/static/(.*)", StaticFileHandler, {"path": "static"})
    ], **settings)
