# -*- coding: utf-8 -*-
"""This module only contains basic handler for the different pages of the UI.
"""

import tornado.websocket
import tornado.ioloop
import tornado.web
import logging
from jobshop.common.security import pwd_context


# ---------------------------------------------------------------------------
# BASIC HANDLER
# ---------------------------------------------------------------------------
class BasicHandler(tornado.web.RequestHandler):

    def on_finish(self):
        logging.debug("Request handler %s finish called." % self)

    def get_current_user(self):
        # REMARK: If this is edited, it has to be done for the web socket
        # handler as well.
        cuser = self.get_secure_cookie("user")
        if not cuser:
            return None
        return tornado.escape.json_decode(cuser)

    def initialize(self, simserver, config={}):
        self.simserver = simserver
        self.pagePrefix = "Job-Shop Simulator"
        self.config = config

    def render_error_page(self, title, error):
        self.render("error.html", title=title, error=error)


# ---------------------------------------------------------------------------
# LOGIN / LOGOUT HANDLER
# ---------------------------------------------------------------------------
class LoginHandler(tornado.web.RequestHandler):

    def initialize(self, simserver):
        self.simserver = simserver

    def get(self):
        self.render("login.html", title="Login")

    def post(self):
        username = self.get_argument('username')
        password = self.get_argument('password')
        authorized = self._check_permissions(username, password)

        if authorized:
            self.set_secure_cookie("user", tornado.escape.json_encode(username))
            # redirect to / if parameter missing
            self.redirect(self.get_argument("next", "/"))
        else:
            err_msg = tornado.escape.url_escape("Login Incorrect", plus=False)
            self.redirect("/login?error=" + err_msg)

    def _check_permissions(self, user, password):
        try:
            db_hash = self.simserver.user_credentials(user)
        except RuntimeError:  # This should not happen, but just in case.
            return False
        else:
            if pwd_context.verify(password, db_hash):
                return True
            return False


class LogoutHandler(tornado.web.RequestHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect(self.get_argument("next", "/"))


# ---------------------------------------------------------------------------
# WEB HANDLER
# ---------------------------------------------------------------------------
class IndexHandler(BasicHandler):

    @tornado.web.authenticated
    def get(self):
        self.render("index.html", title=self.pagePrefix)


class SimulationsHandler(BasicHandler):

    @tornado.web.authenticated
    def get(self):
        self.render("simulations.html", title=self.pagePrefix + " - Simulations")


class SimulationsIDHandler(BasicHandler):

    @tornado.web.authenticated
    def get(self, sim_id):
        try:
            data = self.simserver.simulation_for_id(sim_id)
        except RuntimeError:
            self.render_error_page("Database Error", "Retrieving data from the database failed.")
        else:
            self.render("simulation.html", title="Simulation: %s" % data['name'], simulation_id=sim_id)


class ShopsHandler(BasicHandler):

    @tornado.web.authenticated
    def get(self):
        self.render("shops.html", title="Shops")


class ShopHandler(BasicHandler):

    @tornado.web.authenticated
    def get(self, shop_id):
        try:
            data = self.simserver.shop_for_id(shop_id)
        except RuntimeError:
            self.render_error_page("Database Error", "Retrieving data from the database failed.")
        else:
            self.render("shop.html", title="Shop: %s" % data['name'], shop_id=shop_id)


class ChartHandler(BasicHandler):

    @tornado.web.authenticated
    def get(self, result_id):
        self.render("chart.html", title="Simulation Result %s - Gantt Chart" % result_id, result_id=result_id)
