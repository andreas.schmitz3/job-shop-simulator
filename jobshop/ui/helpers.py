# -*- coding: utf-8 -*-
"""
"""
import json


class TypeEncoder(json.JSONEncoder):
    """Encodes objects of class `type` e.g. int or float by replacing
    it with its name.
    """
    def default(self, obj):
        if isinstance(obj, type):
            return obj.__name__
        return json.JSONEncoder.default(self, obj)
