# Assets

The asset files in this folder (Javascript and CSS) are the minified and compiled results of the assets in the sibling folder **assets**.

All Javascript and CSS changes should happen to those assets.
See the README.md file in the **assets** folder for more information

