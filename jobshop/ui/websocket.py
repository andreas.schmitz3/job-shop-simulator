# -*- coding: utf-8 -*-
"""This module takes care of the websocket interface to the frontend. This
includes message parsing and validation as well as sending messages to the
connected frontends.

"""

import tornado.websocket
import tornado.ioloop
import tornado.web
from tornado import gen
from tornado.concurrent import run_on_executor
import logging
from concurrent.futures import ThreadPoolExecutor
import json
from datetime import datetime

from jobshop.ui.protocol import ProtocolVersion
from jobshop.ui.helpers import TypeEncoder
from jobshop.sim.executor import ExecutorFactory
from jobshop.sim.scheduler import SchedulerFactory
from jobshop.sim.job_management import JobSelector, JobDistributor


class ValidationError(Exception):
    """Error indicating that the validation of a message or other component
    failed.
    """
    pass


class WSAPIHandler(tornado.websocket.WebSocketHandler):
    """WebSocket handler that takes care of the whole communication between the
    front- and backend.

    """
    #: thread pool used to execute new simulations. Although this does not
    #: really scale that well due to the GIL, it at least does not block the
    #: whole application.
    #:
    #: .. NOTE::
    #:     If more performance is needed, consider using ProcessPoolExecutor
    #:     instead of ThreadPoolExecutor (see here:
    #:     http://stackoverflow.com/a/25208213) or use IPC with ZMQ. This
    #:     could be done by starting a couple of processes at the start of the
    #:     server and communicate with them via ZMQ.
    # The name "executor" is used by the run_on_executor decorator by default.
    executor = ThreadPoolExecutor()
    # List of available handles for an incoming message from the front-end.
    available_handlers = {
        "get": {
            "simulation_result": "_handler_simulation_result",
            "simulation_capabilities": "_handler_simulation_capabilities",
            "simulations": "_handler_simulations",
            "simulation": "_handler_simulation",
            "shops": "_handler_shops",
            "shop": "_handler_shop"
        },
        "delete": {
            "simulations": "_handler_delete_simulations",
            "shops": "_handler_delete_shops"
        },
        "add": {
            "simulations": "_handler_add_simulations",
            "shops": "_handler_add_shops"
        }
    }

    def initialize(self, simserver):
        self.simserver = simserver

    def get_current_user(self):
        # REMARK: If this is edited, it has to be done for the basic handler
        # handler as well.
        cuser = self.get_secure_cookie("user")
        if not cuser:
            return None
        return tornado.escape.json_decode(cuser)

    def get(self):
        # Check if user is logged in (authorized.)
        if not self.current_user:
            self.set_status(401)  # Unauthorized
            self.finish()
            return
        super().get()

    # REMARK: By default foreign origins are not allowed.
    def open(self):
        logging.debug('New websocket connection. Request: %s' % self.request)

    def on_close(self):
        logging.debug('websocket connection closed')

    @gen.coroutine
    def on_message(self, message):
        logging.debug('websocket message received %s' % message)
        try:
            data = json.loads(message)
            self._validate_message(data)
        except ValidationError as e:
            yield self._send_error("Received message was not valid. Error: %s" % e)
        except json.decoder.JSONDecodeError as e:
            yield self._send_error("The send JSON is malformed.")
        else:
            yield self._message_handler(data)

    def _validate_message(self, message):
        """Validates a received message. Calls the specific handler validator
        functions depending on the message.

        Args:
            message (dict): A dictionary (loaded from JSON) that was received
                from the front-end.

        Raises:
            ValidationError: if the provided message is not valid.

        """
        try:
            # Check proper envelope and general composition.
            assert message['version'] == ProtocolVersion.String, "The protocol version number of the message does not match."
            assert message['action'] in WSAPIHandler.available_handlers, "Specified action not supported."
            assert message['target'] in WSAPIHandler.available_handlers[message['action']], "Specified target for action not supported."
            assert isinstance(message['data'], dict), "The message is malformed."
            # Get the validator message for the handler
            handler_name = WSAPIHandler.available_handlers[message['action']][message['target']]
            handler_validator = getattr(self, handler_name + "_validator")
            assert callable(handler_validator), "The message could not be validated."
            handler_validator(message['data'])
        except (AssertionError, ValueError, KeyError, AttributeError) as e:
            raise ValidationError(e)

    @gen.coroutine
    def _message_handler(self, msg):
        """Passes a given message through to the appropriate message handler.
        Sens an error message to the front-end if the message could not be
        handled.
        """
        try:
            actionHandler = WSAPIHandler.available_handlers[msg['action']]
            targetName = actionHandler[msg['target']]
        except KeyError:
            yield self._send_error("The provided combination of target and action is not supported.")
        else:
            try:
                target = getattr(self, targetName)
            except AttributeError:
                yield self._send_error("The provided combination of target and action is not supported.")
            else:
                yield target(msg['data'])

    @gen.coroutine
    def _send_answer(self, action, target, data, encoder=json.JSONEncoder):
        """Sends an answer to the websocket for the specified action, target
        and data. This function encapsulates these information in a unified
        package with a version number.

        Args:
            action (str): The action of the message. e.g. get, add, ...
            target (str): The target of the action. e.g. simulation
            data: JSON serializable value e.g. dict, int, str, ...
        """
        answer = {
            "action": action,
            "target": target,
            "data": data,
            "version": ProtocolVersion.String
        }
        try:
            yield self.write_message(json.dumps(answer, cls=encoder))
        except tornado.websocket.WebSocketClosedError:
            pass

    @gen.coroutine
    def _send_error(self, msg, encoder=json.JSONEncoder):
        """Sends an answer to the websocket containing an error message.
        """
        logging.warn(msg)
        answer = {
            "action": "info",
            "target": "error",
            "message": msg,
            "version": ProtocolVersion.String
        }
        try:
            yield self.write_message(json.dumps(answer, cls=encoder))
        except tornado.websocket.WebSocketClosedError:
            pass

    @gen.coroutine
    def _handler_simulation_capabilities(self, data):
        """Retrieves all the capabilities of application i.e. which schedulers,
        executors and parameters are available.
        """
        cap = {}
        cap['executors'] = {}
        for exec_name in ExecutorFactory.available_executors():
            cap['executors'][exec_name] = {
                'name': exec_name,
                'parameters': ExecutorFactory.executor_arguments(exec_name)
            }
        cap['schedulers'] = {}
        for scheduler_name in SchedulerFactory.available_schedulers():
            cap['schedulers'][scheduler_name] = {
                'name': scheduler_name,
                'parameters': SchedulerFactory.get_scheduler(scheduler_name).arguments()
            }
        cap['job_distribution'] = JobDistributor.distribution_arguments()
        cap['job_time_slice'] = JobDistributor.time_slice_arguments()
        cap['job_selector'] = JobSelector.arguments()
        try:
            yield self._send_answer('add', 'simulation_capabilities', cap, encoder=TypeEncoder)
        except tornado.websocket.WebSocketClosedError:
            pass

    def _handler_simulation_capabilities_validator(self, data):
        """
        """
        pass

    @gen.coroutine
    def _handler_simulation_result(self, data):
        """Retrieves the simulation result for a given result id.
        """
        try:
            result = self.simserver.simulation_result_for_id(data['result_id'])
        except RuntimeError as e:
            yield self._send_error(str(e))
        else:
            answer = {
                "result_id": data["result_id"],  # Mirror back
                "result": result
            }
            yield self._send_answer('add', 'simulation_result', answer)

    def _handler_simulation_result_validator(self, data):
        """
        """
        assert isinstance(data['result_id'], int), "The id of the simulation result to be retrieved is missing."

    @gen.coroutine
    def _handler_simulation(self, data):
        """Retrieves the simulation data for a given simulation id.
        """
        try:
            sim = self.simserver.simulation_for_id(data['simulation_id'])
            shop = self.simserver.shop_for_id(sim['shop_id'])
            sim_results = self.simserver.simulation_results(data['simulation_id'])
            for simres in sim_results:
                simresid = simres['simulation_result_id']
                simres['metrics'] = self.simserver.simulation_result_performance_for_id(simresid)
        except RuntimeError as e:
            yield self._send_error(str(e))
        else:
            answer = {
                "simulation_id": data['simulation_id'],  # Mirror back
                "name": sim['name'],
                "seed": sim['seed'],
                "shop_id": sim['shop_id'],
                "shop_name": shop['name'],
                "job_selector_strategy": sim['job_selector_strategy'],
                "job_selector_parameters": sim['job_selector_parameters'],
                "job_time_slice_strategy": sim['job_time_slice_strategy'],
                "job_time_slice_parameters": sim['job_time_slice_parameters'],
                "job_distribution_strategy": sim['job_distribution_strategy'],
                "job_distribution_parameters": sim['job_distribution_parameters'],
                "creation_time": sim['creation_time'],
                "simulation_results": sim_results,
            }
            yield self._send_answer('add', 'simulation', answer)

    def _handler_simulation_validator(self, data):
        """
        """
        assert isinstance(data['simulation_id'], int), "The id of the simulation to be retrieved is missing."

    @gen.coroutine
    def _handler_simulations(self, data):
        """Retrieves a list of all simulations and returns it to the websocket.
        """
        try:
            result = self.simserver.all_simulations(ascending=True)
        except RuntimeError as e:
            yield self._send_error(str(e))
        else:
            yield self._send_answer('add', 'simulations', result)

    def _handler_simulations_validator(self, data):
        """
        """
        pass

    @run_on_executor
    def _async_run_new_simulation(self, simulation_id, simulation):
        """Runs the execution of a new simulation on a separate thread.
        """
        self.simserver.start_simulation(simulation_id, simulation)

    @gen.coroutine
    def _handler_add_simulations(self, data):
        """Handler to start a new simulation.
        """
        try:
            # Prepare the simulation, give it an id and answer the frontend
            # that everything worked as intended...
            sim_id, simenv = self.simserver.prepare_new_simulation(data)
            answer = {
                'simulation_id': sim_id,
                'name': simenv.name,
                'shop_id': simenv.shop_id,
                'status': 'Running',
                'creation_time': str(datetime.utcnow())
            }
            yield self._send_answer('add', 'simulations', [answer])

            # ... Afterwards, the execution of the simulation is trigged.
            # Once it has finished, an status update is send to the client.
            yield self._async_run_new_simulation(sim_id, simenv)
            status = "Finished"
        except (RuntimeError, AssertionError) as e:
            status = "Failed"
            yield self._send_error(str(e))

        # Sets the status of the simulation. This is in a separate
        # try except block since it is also necessary to update the status in
        # case of a possible previous exception.
        try:
            self.simserver.set_simulation_status(sim_id, status)
        except RuntimeError as e:
            yield self._send_error(str(e))

        answer = {
            'simulation_id': sim_id,
            'status': status
        }
        yield self._send_answer('update', 'simulations', [answer])

    def _assert_argument(self, parameter, value, arguments_dict, component_name):
        """Helper function that checks if a given parameter and value are
        consistent with the provided argument list.
        """
        assert parameter in arguments_dict, "The parameter %s cannot be found in the list of arguments of the %s." % (parameter, component_name)
        param_type = arguments_dict[parameter].get('type')
        param_enum = arguments_dict[parameter].get('enum')
        if param_type:
            if param_type == float:
                float(value)
            else:
                assert isinstance(value, param_type), "The %s parameter of the %s is not valid" % (parameter, component_name)
        if param_enum:
            assert value in param_enum, "The value %s for paramter %s of the %s is not in the list of supported values: %s" % (value, parameter, component_name, ", ".join(param_enum))

    def _assert_component(self, data, dict_key, arguments, component_name):
        """Helper function to validate the job_selector, job_distribution and
        time_slice components.
        """
        assert isinstance(data[dict_key], dict), "The send data did not contain information about the %s" % component_name
        strategy = data[dict_key]['strategy']
        assert isinstance(strategy, int), "The provided %s strategy value is invalid." % component_name
        strategy_dict = arguments[strategy]
        assert isinstance(strategy_dict, dict), "The provided %s strategy is invalid." % component_name
        if 'parameters' in strategy_dict:
            args = strategy_dict['parameters']
            assert isinstance(args, dict), "There are no parameters available for the provided %s strategy." % component_name
            for param, value in data[dict_key].items():
                if param != "strategy":
                    self._assert_argument(param, value, args, component_name)

    def _handler_add_simulations_validator(self, data):
        """Validates the data that starts a new simulation.
        """
        assert isinstance(data['shop_id'], int), "The send data did not contain a valid shop_id."
        assert isinstance(data['seed'], int) or data['seed'] is None, "The send data did not contain a valid seed."
        assert isinstance(data['name'], str), "The send data did not contain a valid name."
        assert isinstance(data['simulations'], list), "The send data did not contain a list of strategies to be simulated."
        assert len(data['simulations']) > 0, "At least one strategy to be simulated has to be added."

        # Validation of the job selector, job time slice and distribution
        # parameters.
        self._assert_component(
            data=data,
            dict_key='job_selector',
            arguments=JobSelector.arguments(),
            component_name="job selector"
        )
        self._assert_component(
            data=data,
            dict_key='job_time_slice',
            arguments=JobDistributor.time_slice_arguments(),
            component_name="job time distributor"
        )
        self._assert_component(
            data=data,
            dict_key='job_distribution',
            arguments=JobDistributor.distribution_arguments(),
            component_name="job chunk distributor"
        )

        # Validate simulation strategies
        for sim in data['simulations']:
            assert isinstance(sim, dict), "The parameter list of an added strategy is malformed."
            scheduler_name = sim['scheduler']
            assert scheduler_name in SchedulerFactory.available_schedulers(), "The provided scheduler is not in the list of available schedulers."

            scheduler_parameters = sim.get('scheduler_parameters')
            if scheduler_parameters:
                assert isinstance(scheduler_parameters, dict), "The provided scheduler parameter entry is not a dictionary."
                scheduler_args = SchedulerFactory.get_scheduler(scheduler_name).arguments()
                for param, value in scheduler_parameters.items():
                    self._assert_argument(param, value, scheduler_args, scheduler_name + " scheduler")

            executor_name = sim['executor']
            assert executor_name in ExecutorFactory.available_executors(), "The provided Executor is not in the list of available executors."
            executor_parameters = sim.get('executor_parameters')
            if executor_parameters:
                assert isinstance(executor_parameters, dict), "The provided executor parameter entry is not a dictionary."
                executor_args = ExecutorFactory.executor_arguments(executor_name)
                for param, value in executor_parameters.items():
                    self._assert_argument(param, value, executor_args, executor_name + " executor")

    @gen.coroutine
    def _handler_delete_simulations(self, data):
        """
        """
        try:
            self.simserver.delete_simulation(data['simulation_id'])
        except RuntimeError as e:
            yield self._send_error(str(e))
        else:
            answer = {
                'simulation_id': data['simulation_id']
            }
            yield self._send_answer('remove', 'simulations', answer)

    def _handler_delete_simulations_validator(self, data):
        """
        """
        assert isinstance(data['simulation_id'], int), "The id of the simulation to be deleted is missing."

    @gen.coroutine
    def _handler_shops(self, data):
        """
        """
        try:
            result = self.simserver.all_shops(ascending=True)
        except RuntimeError as e:
            yield self._send_error(str(e))
        else:
            yield self._send_answer('add', 'shops', result)

    def _handler_shops_validator(self, data):
        """
        """
        pass

    @gen.coroutine
    def _handler_add_shops(self, data):
        """
        """
        try:
            shop_id = self.simserver.new_shop(data)
            answer = self.simserver.shop_for_id(shop_id)
        except RuntimeError as e:
            raise
            yield self._send_error(str(e))
        else:
            yield self._send_answer('add', 'shops', [answer])

    def _handler_add_shops_validator(self, data):
        """
        """
        data['seed']
        assert isinstance(data['resources'], int), "Number of resources to be generated is missing."
        assert isinstance(data['jobs'], int), "Number of jobs to be generated is missing."
        assert isinstance(data['min_resources'], int), "Minimal flexibility of each operation is missing."
        assert isinstance(data['max_resources'], int), "Maximal flexibility of each operation is missing."
        assert isinstance(data['description'], str), "Description is missing"
        assert isinstance(data['name'], str), "Name is missing"

    @gen.coroutine
    def _handler_delete_shops(self, data):
        """
        """
        try:
            self.simserver.delete_shop(data['shop_id'])
        except RuntimeError as e:
            yield self._send_error(str(e))
        else:
            answer = {
                'shop_id': data['shop_id']
            }
            yield self._send_answer('remove', 'shops', answer)

    def _handler_delete_shops_validator(self, data):
        """
        """
        assert isinstance(data['shop_id'], int), "The id of the shop to be deleted is missing."

    @gen.coroutine
    def _handler_shop(self, data):
        """
        """
        try:
            result = self.simserver.shop_for_id(data['shop_id'])
        except RuntimeError as e:
            yield self._send_error(str(e))
        else:
            yield self._send_answer('add', 'shop', result)

    def _handler_shop_validator(self, data):
        """
        """
        assert isinstance(data['shop_id'], int), "The id of the shop to be retrieved is missing."
