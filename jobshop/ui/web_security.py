# -*- coding: utf-8 -*-
"""
"""

import ssl
import os.path
import random
import string


def cookie_password(keyfile):
    """Returns a secret with which the cookies are protected.

    Args:
        keyfile(string): Filename where the cookie secret should be loaded
            from or stored at if a new one is created (Default: .cookiesecret).

    Returns:
        str: A randomly generated secret to protect the cookies.

    """
    def new_secret(keylen=42):
        seq = string.digits + string.ascii_letters + string.punctuation
        return ''.join(random.SystemRandom().choice(seq) for _ in range(keylen)).strip()

    # Try to open the file and read the secret.
    # if more than one line is present, it is regarded as invalid file
    # and gets replaced. It also checks for a minimal secret length.
    secret = None
    if os.path.isfile(keyfile):
        with open(keyfile, 'r') as file:
            content = file.readlines()
            if len(content) == 1 and len(content[0].strip()) >= 42:
                secret = content[0].strip()

    if not secret:
        # (over)write the file if the secret could not be extracted
        with open(keyfile, 'w+') as file:
            secret = new_secret()
            file.write(secret)

    return secret


def hardened_ssl_context(certfile, keyfile):
    """Creates a TLS1.2 security context with PFS support.

    Args:
        certfile (str): A file path to a certificate file.
        keyfile (str): A file path to the key of the certificate.

    Returns:
        A SSL Security Context used to start the web server.

    """
    ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_ctx.load_cert_chain(
        certfile=certfile,
        keyfile=keyfile
    )

    # This only works under python 3 and newer python 2 versions.
    # To run this with an old python 2 verison, comment the following
    # lines out (not recommended).
    # Configuration based on: https://wiki.mozilla.org/Security/Server_Side_TLS#Modern_compatibility
    ssl_ctx.options |= ssl.OP_NO_SSLv2
    ssl_ctx.options |= ssl.OP_NO_SSLv3
    ssl_ctx.options |= ssl.OP_NO_TLSv1
    ssl_ctx.options |= ssl.OP_NO_TLSv1_1
    ssl_ctx.set_ciphers("ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256")

    return ssl_ctx
