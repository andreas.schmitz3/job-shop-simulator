app.page.login = {};

app.page.login.init = function() {
    app.debug.log('login - init started');

    var pars = app.module.helper.getURLParameter(window.location.search);
    if (pars.error) {
        app.error.write(pars.error);
    }

    app.debug.log('login - init ended');
};
