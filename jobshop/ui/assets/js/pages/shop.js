/**
 *
 */
app.page.shop = {};
app.page.shop.id = {};  // To invalidate compares against undefined.

/**
 *
 */
app.page.shop.init = function() {
    app.debug.log('Shop - init started');

    app.websocket.create(
        app.page.shop.onmessage,
        app.page.shop.onopen
    );

    app.debug.log('Shop - init ended');
};


/**
 *
 */
app.page.shop.onopen = function(ev) {
    var id = $('.shop_id').data('id');
    var parsedId = parseInt(id);
    if (isNaN(parsedId)) {
        // TODO: error
        return;
    }

    app.page.shop.id = parsedId;
    var data = {
        "shop_id": parsedId
    };
    app.websocket.sendMessage("get", "shop", data);
};


/**
 *
 */
app.page.shop.onmessage = function(ev) {

    var json = JSON.parse(ev.data);
    app.debug.log("Received data:", json);

    if (json.target === "error" && json.action === "info") {
        app.error.write(json.message);
        return;
    }

    var callback = app.page.shop.handleCommand(json);
    callback(json.data);
};


/**
 *
 */
app.page.shop.handleCommand = function(cmd) {
    var strategy = {
        shop : {
            add:    app.page.shop.handleAddShop,
        },
    };
    if (strategy[cmd.target] !== undefined && strategy[cmd.target][cmd.action] !== undefined) {
        return strategy[cmd.target][cmd.action];
    }
    return function(){};
};


/**
 *
 */
app.page.shop.handleAddShop = function(data) {
    if (data.shop_id !== app.page.shop.id) {
        // TODO: Show error message?!
        return;
    }

    function deleteShop(event) {
        event.preventDefault();
        // TODO: Spawn layer which asks for confirmation
        app.page.shop.deleteShop(event.data.shop_id);
    }

    var main = $('main');
    main.find('h1')
        .attr('title', data.shop_id);

    if (data.description !== undefined && data.description !== null) {
        var desc = $("<p>").text(data.description);
        main.append(desc);
    }
    if (data.seed !== undefined && data.seed !== null) {
        var seed = $('<span>')
            .attr('class', 'seed')
            .text(data.seed);
        var seedWrapper = $('<p>')
            .text('Seed: ');
        seedWrapper.append(seed);
        main.append(seedWrapper);
    }

    var delLink = $('<a>')
        .attr('href', '#')
        .text('Delete Shop')
        .on('click', {shop_id: data.shop_id}, deleteShop);
    main.append(delLink);

    // TODO: Add more, detailed information about the jobs and resources.
};

/**
 *
 */
app.page.shop.deleteShop = function(id) {

    var parsedInt = parseInt(id);
    if (isNaN(parsedInt)) {
        // show error
        return;
    }

    var data = {
        "shop_id": parsedInt
    };
    app.websocket.sendMessage("delete", "shops", data);
    window.location.href = "/shops";
};
