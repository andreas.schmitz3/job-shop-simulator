/**
 *
 */
app.page.shops = {};

/**
 *
 */
app.page.shops.init = function() {
    app.debug.log('Shops - init started');

    app.websocket.create(
        app.page.shops.onmessage,
        app.page.shops.onopen
    );
    app.page.shops.bindEvents();

    app.debug.log('Shops - init ended');
};


/**
 *
 */
app.page.shops.onopen = function(ev) {
    app.websocket.sendMessage("get", "shops");
};


/**
 *
 */
app.page.shops.onmessage = function(ev) {

    var json = JSON.parse(ev.data);
    app.debug.log("Received data:", json);

    if (json.target === "error" && json.action === "info") {
        app.error.write(json.message);
        return;
    }

    var callback = app.page.shops.handleCommand(json);
    callback(json.data);
};


/**
 *
 */
app.page.shops.deleteShop = function(id) {

    var parsedInt = parseInt(id);
    if (isNaN(parsedInt)) {
        // show error
        return;
    }

    var data = {
        "shop_id": parsedInt
    };
    app.websocket.sendMessage("delete", "shops", data);
};


/**
 *
 */
 // TODO: USE JSON Schema
app.page.shops.newShop = function(form) {
    app.debug.log("Creating new shop...");
    var serform = form.serializeInputs();

    // TODO: validate with JSON schema

    var data = {
        name: serform.shopName,
        description: serform.description,
        jobs: parseInt(serform.jobs),
        resources: parseInt(serform.resources),
        seed: parseInt(serform.seed),
        min_resources: parseInt(serform.minResources),
        max_resources: parseInt(serform.maxResources)
    };

    app.debug.log("Data to be send: ", data);
    app.websocket.sendMessage("add", "shops", data);
};


/**
 *
 */
app.page.shops.handleCommand = function(cmd) {
    var strategy = {
        shops : {
            add:    app.page.shops.handleAddShop,
            remove: app.page.shops.handleRemoveShop,
        },
    };
    if (strategy[cmd.target] !== undefined && strategy[cmd.target][cmd.action] !== undefined) {
        return strategy[cmd.target][cmd.action];
    }
    return function(){};
};


/**
 *
 */
app.page.shops.handleAddShop = function(data) {

    function deleteShop(event) {
        event.preventDefault();
        // TODO: Spawn layer which asks for confirmation
        var shopWrap = $(event.target).parents('tr');
        shopWrap.addClass('markedDeleted');
        app.page.shops.deleteShop(event.data.shop_id);
    }

    for (var i = 0; i < data.length; i++) {
        var shop = data[i];

        var tr = $('<tr>');
        tr.attr('id', 'shop' + shop.shop_id);
        var tdName = $('<td>')
            .attr('class', 'name')
            .attr('title', shop.shop_id);
        if (shop.name.length > 0) {
            tdName.text(shop.name);
        } else {
            tdName.text(shop.shop_id);
        }
        var tdNumJobs = $('<td>')
            .attr('class', 'numJobs')
            .text("#Jobs");

        var tdNumResources = $('<td>')
            .attr('class', 'numResources')
            .text("#Resources");


        var tdAction = $('<td>')
            .attr('class', 'action');
        var viewShop = $('<a>')
            .attr('href', '/shop/' + shop.shop_id)
            .attr('class', 'viewShop')
            .text('Details');
        var separator = $('<span>')
            .attr('class', 'separator');
        var delShop = $('<a>')
            .attr('href', '#')
            .attr('class', 'deleteShop')
            .text('Delete')
            .on('click', {shop_id: shop.shop_id}, deleteShop);

        tdAction.append(viewShop).append(separator).append(delShop);

        tr.append(tdName).append(tdNumJobs).append(tdNumResources).append(tdAction);
        $('#shops tbody').append(tr);
    }
};


/**
 *
 */
app.page.shops.handleRemoveShop = function(data){
    $('#shops tbody').find('#shop' + data.shop_id).remove();
};


/**
 *
 */
app.page.shops.bindEvents = function() {
    $("#newShop").on("submit", function(e) {
        e.preventDefault();
        app.page.shops.newShop($(this));
        return false;
    });
    $("#newShop").on("keypress", function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            app.page.shops.newShop($(this));
            return false;
        }
        return true;
    });
};
