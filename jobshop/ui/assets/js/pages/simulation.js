app.page.simulation = {};



/**
 * Initializes the page.
 */
app.page.simulation.init = function() {
    app.debug.log('Simulation - init started');

     app.websocket.create(
        app.page.simulation.onmessage,
        app.page.simulation.onopen
    );

    app.debug.log('Simulation - init ended');
};


/**
 * Executed when a new message is received over the web socket.
 */
app.page.simulation.onmessage = function(ev) {
    var json = JSON.parse(ev.data);
    app.debug.log("Received data:", json);

    if (json.target === "simulation" && json.action === "add") {
        app.page.simulation.handleAddSimulation(json.data);
    } else if (json.target === "error" && json.action === "info") {
        app.error.write(json.message);
    }

};


/**
 * Executed after the web socket opened successfully.
 * Requests data over the web socket.
 */
app.page.simulation.onopen = function(ev) {
    var id = $('.simulation_id').data('id');
    var parsedId = parseInt(id);
    if (isNaN(parsedId)) {
        // TODO: error
        return;
    }

    var data = {
        "simulation_id": parsedId
    };
    app.websocket.sendMessage("get", "simulation", data);
};


/**
 *
 */
app.page.simulation.handleAddSimulation = function(data) {
    var title = "";
    if (data.name !== undefined && data.name.length !== 0) {
        title = data.name + "(" + data.simulation_id + ")";
    } else {
        title = data.simulation_id;
    }
    $('h1').text("Simulation: " + title);

    // Add meta data.
    function add_metadata(text, value) {
        if (value === undefined || value.length === 0) return;
        var tr = $('<tr>');
        var k = $('<th>').attr('scope', 'row').text(text);
        var v = $('<td>').text(value);
        tr.append(k).append(v);
        $('#metadata tbody').append(tr);
    }
    add_metadata("Shop (ID)", data.shop_name + " (" + data.shop_id + ")");
    add_metadata("Creation Date", data.creation_time);
    add_metadata("Seed", data.seed);

    // Add different performance metrics to the table.
    var sim = $('table#simulation tbody');
    for (var i = 0; i < data.simulation_results.length; i++) {
        var simres = data.simulation_results[i];

        var tr = $('<tr>');
        app.page.simulation.addNamesAndLink(tr, simres);
        app.page.simulation.addPerformanceMetrics(tr, simres);

        sim.append(tr);
    }

    app.page.simulation.initPerformanceMetricsControl(data);
};



/**
 *
 */
app.page.simulation.initPerformanceMetricsControl = function(data) {
    $(document).on('click', "#simulation th[class^='metric_'],#simulation th[class*=' metric_']", function(event){
        var target = $(event.target);
        var column = $('#simulation .' + target.attr('class'));
        column.hide();

        var tr = $('<tr>');
        var td = $('<td>')
            .attr('class', target.attr('class'))
            .text(target.text());
        tr.append(td);

        var table = $('table#disabledMetrics');
        var tbody = table.find('tbody');
        tbody.append(tr);
        table.show();

        tr.on('click', function(){
            tr.remove();
            column.show();
            if (tbody.find('tr').length === 0) {
                table.hide();
            }
        });
    });

};


/**
 *
 */
app.page.simulation.addNamesAndLink = function(tr, simres) {
    var tdScheduler = $("<td>")
        .attr('class', 'scheduler')
        .text(simres.scheduler_name);

    var tdExecutor = $("<td>")
        .attr('class', 'executor')
        .text(simres.executor_name);

    var tdLink = $('<td>')
        .attr('class', 'link');
    var chartLink = $('<a>')
        .attr('class', 'chartLink')
        .attr('href', '/charts/' + simres.simulation_result_id)
        .text('View Chart');

    tdLink.append(chartLink);

    tr.append(tdScheduler).append(tdExecutor).append(tdLink);
};


/**
 *
 */
app.page.simulation.addPerformanceMetrics = function(tr, simres) {
    var keys = Object.keys(simres.metrics);
    for (var i = 0; i < keys.length; i++) {
        var metric = keys[i];
        var value = simres.metrics[metric];

        var td = $('<td>')
            .attr('class', metric)
            .text(value);

        tr.append(td);
        app.page.simulation.addPerformanceMetricHeader(metric);
    }
};


app.page.simulation.addPerformanceMetricHeader = function(metric) {
    if (typeof metric !== "string" || metric.length === 0) {
        return;
    }

    // check if the column header was already created.
    var headRow = $('table#simulation thead tr');
    if ( headRow.find('.' + metric).length !== 0) {
        return;
    }

    var textMetric = metric.replace("metric_", "");
    textMetric = app.module.helper.sentenceCase(textMetric, "_", " ");
    var th = $('<th>')
        .attr('class', metric)
        .attr('scope', 'col')
        .text(textMetric);
    headRow.append(th);
};
