/**
 *
 */
app.page.simulations = {};

/**
 *
 */
app.page.simulations.init = function() {
    app.debug.log('Simulations - init started');

    app.websocket.create(
        app.page.simulations.onmessage,
        app.page.simulations.onopen
    );
    app.page.simulations.bindEvents();

    app.debug.log('Simulations - init ended');
};



app.page.simulations.onopen = function(ev) {
    app.websocket.sendMessage("get", "simulations");
    app.websocket.sendMessage("get", "simulation_capabilities");
    app.websocket.sendMessage("get", "shops");
};


/**
 *
 */
app.page.simulations.onmessage = function(ev) {

    var json = JSON.parse(ev.data);
    app.debug.log("Received data:", json);

    if (json.target === "error" && json.action === "info") {
        app.error.write(json.message);
        return;
    }

    var callback = app.page.simulations.handleCommand(json);
    callback(json.data);
};


/**
 *
 */
app.page.simulations.deleteSimulation = function(id) {

    var parsedInt = parseInt(id);
    if (isNaN(parsedInt)) {
        // show error
        return;
    }

    var data = {
        "simulation_id": parsedInt
    };
    app.websocket.sendMessage("delete", "simulations", data);
};


/**
 *
 */
app.page.simulations.getFormData = function(form) {
    var seed = $('#seed').val();
    var data = {
        name: $('#simname').val(),
        seed: seed.length > 0 ? parseInt(seed) : null,
        shop_id: parseInt($('#shop').val()),
        simulations: []
    };

    // extract selected strategies.
    $('#strategies tbody tr').each(function() {
        var scheduler = $(this).find('td.scheduler');
        var executor = $(this).find('td.executor');
        var sim = {};
        sim.scheduler = scheduler.data('value');
        sim.executor = executor.data('value');
        sim.scheduler_parameters = scheduler.data('parameters');
        sim.executor_parameters = executor.data('parameters');
        data.simulations.push(sim);
    });

    // extract the selected job distribution and job selection.
    $('#job_selector, #job_time_slice, #job_distribution').each(function() {
        var _this = $(this);
        var name = _this.attr('name');
        var params = _this
            .siblings('.parameterList')
            .find('input, select, textarea')
            .serializeInputs();
        params.strategy = parseInt(_this.val());
        data[name] = params;
    });
    return data;
};


/**
 *
 */
app.page.simulations.newSimulation = function(form) {
    app.debug.log("Creating new simulation...");

    var data = app.page.simulations.getFormData(form);
    app.debug.log("Data to be send: ", data);

    app.websocket.sendMessage("add", "simulations", data);
};


/**
 *
 */
app.page.simulations.handleCommand = function(cmd) {
    var strategy = {
        simulations: {
            add: app.page.simulations.handleAddSimulation,
            remove: app.page.simulations.handleRemoveSimulation,
            update: app.page.simulations.handleUpdateSimulation
        },
        simulation_capabilities: {
            add: app.page.simulations.handleAddSimulationCapabilities
        },
        shops: {
            add: app.page.simulations.handleAddShops,
            remove: app.page.simulations.handleRemoveShops,
        }
    };
    if (strategy[cmd.target] !== undefined && strategy[cmd.target][cmd.action] !== undefined) {
        return strategy[cmd.target][cmd.action];
    }
    return function() {};
};


/**
 * Executed whenever new shops are added to the system.
 * Adds the shop id as an option to the shop select element.
 *
 * @param {object} data - The data send by the backend.
 */
app.page.simulations.handleAddShops = function(data) {
    var shopSelect = $('.newSimWrapper #shop');
    for (var i = 0; i < data.length; i++) {
        var shop = data[i];
        var opt = $('<option>')
            .attr('class', 'shop' + shop.shop_id)
            .attr('value', shop.shop_id);
        if (shop.name.length > 0) {
            opt.text(shop.name + "(" + shop.shop_id + ")");
        } else {
            opt.text("(" + shop.shop_id + ")");
        }
        shopSelect.append(opt);
    }
};


/**
 * Executed whenever a shop ist removed from the system.
 * Removes it from the select as well.
 *
 * @param {object} data - The data send by the backend.
 */
app.page.simulations.handleRemoveShops = function(data) {
    var shopSelect = $('.newSimWrapper #shop');
    for (var i = 0; i < data.length; i++) {
        var shop = data[i];
        shopSelect.find('option.shop' + shop.shop_id).remove();
    }
};


/**
 * For every part in the provided data - meaning schedulers, executors,
 * job_selector options etc. - the proper select tag is selected and filled with
 * the provided options. Furthermore if there are optional arguments provided
 * for the different options of those parts, they are added to the DOM once
 * the an select changes.
 */
app.page.simulations.handleAddSimulationCapabilities = function(data) {
    // Get a list of strings with the names of  all the different select /
    // parts that were send by the back-end. This keys are used to find the
    // proper select.
    var parts = Object.keys(data);
    for (var i in parts) {
        // select the data of the part (i.e. its parameters etc.) and get
        // the DOM element (select), where the different options are added to.
        var partID = parts[i];
        var partData = data[partID];
        var select = $('.newSimWrapper select#' + partID);
        // Iterate over all parameters and add an option for it to the DOM.
        // If an alternative name, via the partData[i].name property is
        // provided, it is used as display text. Otherwise, the value that is
        // send back to the back-end in case of an submit, is used as text.
        for (var partVal in partData) {
            var partName = (partData[partVal].name !== undefined) ? partData[partVal].name : partVal;
            var option = $('<option>')
                .attr('value', partVal)
                .text(partName);
            select.append(option);
        }
        // If another option is selected, the received data is used to create
        // proper input fields for the option.
        select.on('change', { partData: partData }, app.page.simulations.selectChangeHandler);
    }
};


/**
 * Executed when a select with dynamic contents changes. This is e.g. the case
 * for executor and scheduler selects.
 *
 * event.data contains the data passed to.
 *
 * @param {object} event - The change event passed by the event dispatcher.
 */
app.page.simulations.selectChangeHandler = function(event) {
    // Delete previous input fields.
    var paramWrapper = $(this).siblings('.parameterList').empty();
    // check data
    var partData = event.data.partData;
    if (partData === undefined) return;
    // Create input elements depending on the selected option.
    var name = $(this).val();
    var partOps = event.data.partData[name];
    if (partOps !== undefined) {
        for (var arg in partOps.parameters) {
            var elem = app.page.simulations.createArgumentField(arg, partOps.parameters[arg]);
            paramWrapper.append(elem);
        }
    }
};


/**
 * Creates and returns the DOM elements for the specified argument name and
 * type.
 *
 * @param {string} name - the name of the argument
 * @param {object} obj - The definition of the argument e.g. type, enum, ...
 * @returns {object} - A DOM object that can be appended to the argument list.
 */
app.page.simulations.createArgumentField = function(name, obj) {
    var readableName;
    // obj.name is an optional readable name provided by the backend.
    if (obj.name !== undefined && typeof obj.name === "string") {
        readableName = obj.name;
    } else {
        readableName = app.module.helper.sentenceCase(name, '_', ' ');
    }
    var label = $('<label>')
        .text(readableName);
    if (obj.enum !== undefined) {
        var select = $('<select>')
            .addClass(name)
            .addClass('defaultSelected')
            .attr('name', name);
        $('<option>').attr('disabled', '').appendTo(select);
        $('<option>')
            .attr('value', '')
            .attr('class', 'default')
            .text('Please choose')
            .appendTo(select);

        for (var i in obj.enum) {
            var opt = $('<option>')
                .attr('value', obj.enum[i])
                .text(obj.enum[i]);
            select.append(opt);
        }
        label.append(select);
    } else if (["int", "float", "list"].indexOf(obj.type) > -1) {
        var input = $('<input>')
            .attr('type', 'text')
            .attr('name', name)
            .data('type', obj.type);
        if (obj.type === "int") {
            input.attr('pattern', '[0-9]*');
        } else if (obj.type === "float") {
            input.attr('step', "0.01");
        }
        if (obj.type === "int" || obj.type === "float") {
            input.attr('type', 'number');
        }
        if (obj.default !== undefined) {
            input.val(obj.default);
        }
        label.append(input);
    } else if (obj.type === 'bool') {
        var cbox = $('<input>')
            .attr('type', 'checkbox')
            .attr('name', name);
        if (obj.default === true) {
            cbox.attr('checked', '');
        }
        label.append(cbox);
    }
    return label;
};


/**
 * Adds a new entry in the list of simulations.
 * This is triggered based on a message from the back-end.
 */
app.page.simulations.handleAddSimulation = function(data) {
    // delete previous error messages.
    app.error.clear();

    function deleteSim(event) {
        event.preventDefault();
        var simWrap = $(event.target).parents('tr');
        simWrap.addClass('markedDeleted');
        app.page.simulations.deleteSimulation(event.data.simulation_id);
    }

    function smartDate(creation_time) {
        // NOTE: Convert datetime to ISO 8601 Extended Format
        var date = new Date(creation_time.split(" ").join("T") + "Z");
        var today = new Date();
        today.setMinutes(0);
        today.setHours(0);
        today.setSeconds(0);
        today.setMilliseconds(0);
        var yesterday = new Date(today);
        yesterday.setDate(yesterday.getDate() - 1);
        if (date < today && date > yesterday)
            return "Yesterday, " + date.toLocaleTimeString();
        else if (date < today)
            return date.toLocaleString();
        else
            return date.toLocaleTimeString();
    }


    for (var i = 0; i < data.length; i++) {
        var sim = data[i];

        var tr = $('<tr>');
        tr.attr('id', 'sim' + sim.simulation_id);
        var tdName = $('<td>')
            .attr('class', 'name')
            .attr('title', sim.simulation_id);
        if (sim.name.length > 0) {
            tdName.text(sim.name);
        } else {
            tdName.text(sim.simulation_id);
        }
        var tdDate = $('<td>')
            .attr('class', 'date')
            .text(smartDate(sim.creation_time));
        var tdStatus = $('<td>')
            .attr('class', 'status')
            .text(sim.status);

        var tdAction = $('<td>')
            .attr('class', 'action');
        var viewResults = $('<a>')
            .attr('href', '/simulations/' + sim.simulation_id)
            .attr('class', 'viewResults')
            .text('View Results');
        var separator = $('<span>')
            .attr('class', 'separator');
        var delSim = $('<a>')
            .attr('href', '#')
            .attr('class', 'deleteSimulation')
            .text('Delete')
            .on('click', { simulation_id: sim.simulation_id }, deleteSim);

        tdAction.append(viewResults).append(separator).append(delSim);

        tr.append(tdDate).append(tdName).append(tdStatus).append(tdAction);
        $('#simulations tbody').prepend(tr);
    }
};


/**
 * Removes a simulation from the table.
 * This is triggered based on a message from the back-end.
 */
app.page.simulations.handleRemoveSimulation = function(data) {
    $('#simulations tbody').find('#sim' + data.simulation_id).remove();
};


/**
 * Updates the status of the simulation.
 */
app.page.simulations.handleUpdateSimulation = function(data) {
    for (var i = 0; i < data.length; i++) {
        var sim = data[i];
        var simEntry = $('#simulations tbody').find('#sim' + sim.simulation_id);
        if (simEntry.length) {
            simEntry.find('td.status').text(sim.status);
        }
    }
};


/**
 * Handler executed whenever the "Add Strategy" button is clicked.
 */
app.page.simulations.addStrategyHandler = function(event) {
    var fset = $('fieldset.strategies');
    var schedVal = $('select#schedulers').val();
    var schedParams = fset.find('.column.schedulers .parameterList')
        .find('select, input')
        .serializeInputs();

    var execVal = $('select#executors').val();
    var execParams = fset.find('.column.executors .parameterList')
        .find('select, input')
        .serializeInputs();

    // TODO: Check / validate parameters
    // remove previous error classes.
    $('select#executors, select#schedulers').removeClass('error');
    if (schedVal === "" || execVal === "") {
        if (schedVal === "") $('select#schedulers').addClass('error');
        if (execVal === "") $('select#executors').addClass('error');

        app.error.write("You need to select proper executor and scheduler values to add a new strategy.");
        app.debug.log("The selected strategy values are empty.");
        return;
    }

    // Check for duplicates and return ifh the strategy to be scheduled was
    // already added.
    var isDuplicate = false;
    $('#strategies tbody tr').each(function() {
        var currSched = $(this).find('td.scheduler');
        var currExec = $(this).find('td.executor');
        if (currSched.data('value') === schedVal &&
            currExec.data('value') === execVal &&
            app.module.helper.objectEquals(currSched.data('parameters'), schedParams) &&
            app.module.helper.objectEquals(currExec.data('parameters'), execParams)) {
            isDuplicate = true;
            return;
        }
    });
    if (isDuplicate) {
        app.error.write("The strategy is already scheduled.");
        app.debug.log("The strategy was already added, exiting.");
        return;
    }

    function addListEntries(list, parameters) {
        var paramNames = Object.getOwnPropertyNames(parameters);
        for (var i = 0; i < paramNames.length; i++) {
            var k = $('<span>')
                .attr('class', 'key')
                .text(paramNames[i]);
            var v = $('<span>')
                .attr('class', 'value')
                .text(parameters[paramNames[i]]);
            $('<li>')
                .append(k)
                .append('<span>: </span>')
                .append(v)
                .appendTo(list);
        }
    }

    var newStrat = $('<tr>');
    var schedTD = $('<td>')
        .attr('class', 'scheduler')
        .data('value', schedVal)
        .data('parameters', schedParams)
        .text($('select#schedulers option:selected').text());
    var schedParamList = $('<ul>')
        .appendTo(schedTD);
    addListEntries(schedParamList, schedParams);
    var execTD = $('<td>')
        .attr('class', 'executor')
        .data('value', execVal)
        .data('parameters', execParams)
        .text($('select#executors option:selected').text());
    var execParamList = $('<ul>')
        .appendTo(execTD);
    addListEntries(execParamList, execParams);
    var actionTD = $('<td>')
        .attr('class', 'action');

    $('<a>')
        .attr('class', 'removeStrategy')
        .attr('href', '#')
        .text('Remove')
        .appendTo(actionTD);
    newStrat.append(schedTD).append(execTD).append(actionTD);
    $('#strategies tbody').append(newStrat);
};


/**
 * Binds the different events that are used throughout the function.
 */
app.page.simulations.bindEvents = function() {
    // Simulations specific
    $('button#toggleNewSim').on('click', function(e) {
        $('.newSimWrapper').slideToggle();
    });

    $('button#addStrategy')
        .on('click', app.page.simulations.addStrategyHandler);

    $(document).on('click', '.newSimWrapper .removeStrategy', function(event) {
        event.preventDefault();
        $(event.target).parents('tr').remove();
    });

    $("#newSimulation").on("submit", function(e) {
        e.preventDefault();
        app.page.simulations.newSimulation($(this));
        return false;
    });
    $("#newSimulation").on("keypress", function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            app.page.simulations.newSimulation($(this));
            return false;
        }
        return true;
    });

    // set default selected class if necessary
    $(document).on('change', '#newSimulation select', function() {
        var _this = $(this);
        if (_this.val() === undefined || _this.val().length === 0) {
            _this.addClass("defaultSelected");
        } else {
            _this.removeClass("defaultSelected");
        }
    });
};
