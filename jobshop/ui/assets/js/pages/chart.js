app.page.chart = {};

// Configuration
app.page.chart.config = {};
app.page.chart.config.width = 1024;
app.page.chart.config.height = 640;

/**
 *
 */
app.page.chart.init = function() {
    app.debug.log('Chart - init started');

     app.websocket.create(
        app.page.chart.onmessage,
        app.page.chart.onopen
    );

    app.debug.log('Chart - init ended');
};


/**
 *
 * @param {object} data - A javascript object representing a simulation result
 *      scheduled.
 */
app.page.chart.create = function(data) {
    // create domains for the data and get the main container
    var jobs = app.page.chart._jobSet(data);
    var context = app.page.chart._getContext();
    var x = app.page.chart._xScale(data);
    var y = app.page.chart._yScale(data);
    var color = app.page.chart._colorScale(jobs);

    // Add the operations and resources.
    var resources = context.selectAll('.resource')
        .data(d3.keys(data))
      .enter().append('g')
        .attr('class', 'resource')
        .attr('transform', function(d) {
            return "translate(0,"+y(d)+")";
        });

    var operations = resources.selectAll('.operation')
        .data(function(res){ return data[res]; })
      .enter().append('g')
        .attr('class', 'operation');

    operations.append('rect')
        .attr('height', y.bandwidth())
        .attr('width', function(d){ return x(d.finish - d.start); })
        .attr('x', function(d){ return x(d.start); })
        .style('fill', function(d){ return color(d.job); })
      .append("title")
        .text(function(d) {
            var title = d.job + "-" + d.sequence_number;
            title += " (" + d.start + ", " + d.finish + ")";
            if (d.description !== undefined) {
                title += ": " + d.description;
            }
            return title;
        });

    // Initialize legend and hover events.
    // TODO: Reactivate after adding planned_resource capabilities. See function
    // description for more info.
    // app.page.chart._plannedOperation(operations, color, x, y);
    app.page.chart._legend(jobs, operations, color);
};


/**
 *
 */
app.page.chart.clear = function() {
    $('.svgWrapper svg').empty();
    $('.legendWrapper').empty();
};


/* ------------------------------------------------------------------------- *\
 * PRIVATE FUNCTIONS
\* ------------------------------------------------------------------------- */
/**
 *
 */
app.page.chart.onopen = function(ev) {

    var id = $('.simulation_result_id').data('id');
    var parsedId = parseInt(id);
    if (isNaN(parsedId)) {
        // TODO: error
        return;
    }

    var data = {
        "result_id": parsedId
    };
    app.websocket.sendMessage("get", "simulation_result", data);
};


/**
 *
 */
app.page.chart.onmessage = function(ev) {

    var json = JSON.parse(ev.data);
    app.debug.log("Received data:", json);

    if (json.target === "simulation_result" && json.action === "add") {
        var id = $('.simulation_result_id').data('id');
        if (id === json.data.result_id) {
            app.page.chart.clear();
            app.page.chart.create(json.data.result.schedule);
        }

    } else if (json.target === "error" && json.action === "info") {
        app.error.write(json.message);
    }

};


/**
 *
 * @param {object} data - A javascript object based on JSON
 */
app.page.chart._xScale = function(data) {
    var x = d3.scaleLinear().range([0, app.page.chart.config.width]);
    var keys = d3.keys(data);
    var lastFinishTime = d3.max(keys, function(resource){
        return d3.max(data[resource], function(op){
            return op.finish;
        });
    });
    x.domain([0, lastFinishTime]);
    return x;
};


/**
 *
 * @param {object} data - A javascript object based on JSON
 */
app.page.chart._yScale = function(data) {
    var y = d3.scaleBand()
        .range([0, app.page.chart.config.height])
        .padding(0.1);

    // Set the domains of the axis
    // Y: The resources
    // X: Range between 0 and the time the last operation finished.
    var keys = d3.keys(data);
    y.domain(keys.map(function(d){ return d; }));
    return y;
};


/**
 *
 */
app.page.chart._colorScale = function(jobs) {
    var c = d3.scaleOrdinal()
        .domain(jobs.values().map(function(d){ return d; }))
        .range(d3.schemeCategory20c);
    return c;
};


/**
 *
 */
app.page.chart._jobSet = function(data) {
    var jobs = d3.set();
    var dataMap = d3.map(data);
    dataMap.each(function(schedule, resource) {
        var schedMap = d3.map(schedule);
        schedMap.each(function(op) {
            jobs.add(op.job);
        });
    });
    return jobs;
};


/**
 *
 * @param {object} data - A javascript object based on JSON
 * @return {d3.selection}
 */
app.page.chart._getContext = function() {
    // http://stackoverflow.com/questions/17626555/responsive-d3-chart
    var svg = d3.select("#gantt svg")
        .attr("width", '100%')
        .attr("height", '100%')
        .attr('viewBox','0 0 ' + app.page.chart.config.width + ' ' + app.page.chart.config.height)
        .attr('preserveAspectRatio','xMinYMin');
    var context = svg.append('g')
        .attr('class', 'context');
    return context;
};


/**
 *
 */
app.page.chart._blur = function(sel, transitionGroup, speed, opacity) {
    if (speed === undefined) {
        speed = 400;
    }
    if (opacity === undefined) {
        opacity = 0.3;
    }
    sel.transition(transitionGroup)
        .duration(speed)
        .ease(d3.easeExp)
        .style("opacity", opacity);
};


/**
 *
 */
app.page.chart._easeIn = function(sel, transitionGroup, speed) {
    if (speed === undefined) {
        speed = 400;
    }
    sel.transition(transitionGroup)
        .duration(speed)
        .ease(d3.easeExp)
        .style("opacity", 1);
};


/**
 *
 */
app.page.chart._easeOut = function(sel, transitionGroup, speed) {
    if (speed === undefined) {
        speed = 400;
    }
    sel.transition(transitionGroup)
        .duration(speed)
        .ease(d3.easeExp)
        .style('opacity', 0);
};


/**
 * @param {object} name - desc
 */
app.page.chart._legend = function(jobs, operations, color) {
    var legend = d3.select("#gantt .legendWrapper").append("table")
        .attr('class', 'legend');

    var job = legend.selectAll('tr')
        .data(jobs.values())
      .enter().append('tr')
        .attr('class', 'job');

    job.append('td')
        .attr('class', 'icon')
        .style('background-color', function(d){ return color(d); });
    job.append('td')
        .attr('class', 'text')
        .text(function(d){ return d; });


    job.on('click', function(job){
        // Toggle selected class
        var that = d3.select(this);
        that.classed('selected', !that.classed('selected'));

        // Create the list of selected jobs and the inverse
        var selectedJobList = d3.selectAll('#gantt .legend .job.selected').data();
        var selectedJobs = operations.filter(function(d){
            return selectedJobList.indexOf(d.job) !== -1;
        });
        var otherJobs = operations.filter(function(d) {
            return selectedJobList.indexOf(d.job) === -1;
        });

        if (!selectedJobs.empty()) {
            otherJobs.call(app.page.chart._easeOut, "filteredJobs")
                .classed('hidden', true);
            selectedJobs.call(app.page.chart._easeIn, "filteredJobs")
                .classed('hidden', false);
        } else {
            operations.call(app.page.chart._easeIn, "filteredJobs")
                .classed('hidden', false);
        }

    });
};


/**
 * TODO: also add touch events (e.g. toggle with touch?!)
 * TODO: This is wrong for results where the operation was first scheduled to a
 * different resource. This needs adaptions in the model, database and a lot of
 * other places. Therefore we, at this moment, simply deactivate the whole
 * function.
 * @param {object} name - desc
 * @param {object} name - desc
 * @param {object} name - desc
 * @param {object} name - desc
 */
app.page.chart._plannedOperation = function(operations, color, x, y) {
    // insert a dummy operation for a potential planned operation.
    var svg = d3.select("#gantt svg");
    var plannedOp = svg.insert("g", ":first-child")
        .attr("class", "planned_operation")
        .style("opacity", 0.0);
    var plannedOpRect = plannedOp.append("rect")
        .attr('height', y.bandwidth());
    // If the mouse enters an operation, it is checked if the operation had a
    // planned start and finish time. If that's the case, am operation is
    // displayed, that shows when the operation was planned.
    // By making the original operation slightly translucent, it is even
    // possible to see the planned operation if it equals the original
    // operation.
    operations.on('mouseenter', function(op) {
        if (op.planned_start === null || op.planned_finish === null || op.planned_start === undefined || op.planned_finish === undefined) {
            return;
        }
        var that = d3.select(this);
        // check if the the operation wasn't set to hidden by an easeOut call.
        if (that.classed('hidden')) {
            return;
        }

        // Set hovering class to match the mouseleave function s.t. it is only
        // triggered for operations that reach this point.
        that.classed('hovering', true);

        // Get the resources of the hovering operation to know where to place
        // the planned operation.
        var resource = d3.select(this.parentNode);
        plannedOpRect
            .style('fill', color(op.job))
            .attr('width', x(op.planned_finish - op.planned_start))
            .attr('x', x(op.planned_start));
        plannedOp
            .attr("transform", "translate(0," + y(resource.datum()) + ")");

        // Show the planned operation and blur all other, currently visible
        // operations.
        plannedOp.call(app.page.chart._easeIn, "plannedOperation");
        var visibleOps = operations.filter(function(){
            return !d3.select(this).classed('hidden');
        });
        if (visibleOps.empty()) {
            operations.call(app.page.chart._blur, "plannedOperation");
        } else {
            visibleOps.call(app.page.chart._blur, "plannedOperation");
        }

    });
    // When the mouse leaves a rect, reset the opacity of the planned operation
    // rect to 0.0 and the opacity of the original operation to 1.0.
    operations.on("mouseleave", function(op) {
        if (d3.select(this).classed('hovering')) {
            d3.select(this).classed('hovering', false);
            plannedOp.call(app.page.chart._easeOut, "plannedOperation");
            var selectedJobList = d3.selectAll('#gantt .legend .job.selected').data();
            var selectedJobs = operations.filter(function(d){
                return selectedJobList.indexOf(d.job) !== -1;
            });
            if (selectedJobs.empty()) {
                operations.call(app.page.chart._easeIn, "plannedOperation");
            } else {
                selectedJobs.call(app.page.chart._easeIn, "plannedOperation");
            }
        }
    });
};
