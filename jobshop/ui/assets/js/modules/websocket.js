app.websocket = {};

app.websocket.socket = undefined;


/**
 * Creates a single, globally accessible websocket that is used for all
 * communication to the back-end.
 * The first callback is executed if a new message is send to the socket,
 * the second callback is executed when the web socket was opened successfully.
 */
app.websocket.create = function(onMessageCb, onOpenCb) {
    if (!window.WebSocket) {
        app.error.write("Your browser does not support WebSockets. You therefore cannot use this site. Please try again with another (modern) browser.");
        return;
    }

    // Shows the websocket status symbol
    $('#statusBadge').css('display', 'inline');
    // build url based on the used protocol and the host.
    var url = location.host + '/ws';
    if (location.protocol === "http:") {
        url = "ws://" + url;
    } else if (location.protocol === "https:") {
        url = "wss://" + url;
    }
    var ws = new WebSocket(url);
    app.debug.log("Setup web socket");
    ws.onopen = function(ev) {
        app.debug.log("Web socket opened");
        $('#statusBadge').attr('class', 'online');

        if (typeof onOpenCb === "function") {
            onOpenCb(ev);
        }
    };
    ws.onmessage = onMessageCb;
    ws.onclose = function(ev){
        app.debug.log("web socket was closed. Event: ", ev);
        app.error.write("The server went offline, please reload the page.");
        $('#statusBadge').attr('class', 'offline');
        if (!ev.wasClean) {
            app.debug.error("web socket was closed abnormally.", ev.reason, ev.code);
        }
    };
    ws.onerror = function(ev) {
        app.debug.error("web socket error occureds: ", ev);
        $('#statusBadge').attr('class', 'error');
    };

    app.websocket.socket = ws;
};


/**
 * Sends a message with the provided action, target and data values through
 * the global web socket to the back-end.
 */
app.websocket.sendMessage = function(action, target, data) {
    if (app.websocket.socket === undefined) {
        // Show error?
        return;
    }

    var message = {
        "action": action,
        "target": target,
        "version": "1.0"
    };

    if (data !== undefined) {
        message.data = data;
    } else {
        message.data = {};
    }

    app.websocket.socket.send(JSON.stringify(message));
};
