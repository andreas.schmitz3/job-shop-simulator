// TODO: rename to app.helper
app.module.helper = {};


/**
 * Used to extract the parameters from a URL into an object.
 * A parameter can then be accessed as follows:
 *
 * Link: https://example.com?para=value
 * vars.para returns "value"
 */
app.module.helper.getURLParameter = function(url) {
    if (url === undefined || url.length === 0) {
        return [];
    }
    var vars = [];
    var hash;

    var pos = url.indexOf('?');
    if (pos === -1) {
        return [];
    }

    var hashes = url.slice(pos + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = decodeURIComponent(hash[1]);
    }
    return vars;
};


/**
 * Convenience function to return the value of a cookie for the specified name.
 */
app.module.helper.getCookie = function(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
};


/**
 * Extends jQuery for useful helper functions.
 */
app.module.helper.extendJQuery = function() {
    // Adapted from the original serializeArray function of jQuery.
    // The difference is that checkboxes are always included and their value
    // is either true or false.
    var rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	    rsubmittable = /^(?:input|select|textarea|keygen|checkbox)/i,
        rcheckableType = /^(?:radio)$/i,
        rCRLF = /\r?\n/g;
    jQuery.fn.extend({
        serializeInputsArray: function() {
            return this.map(function(){
                var elements = jQuery.prop(this, "elements");
                return elements ? jQuery.makeArray(elements) : this;
            })
            .filter(function(){
                var type = this.type;
                return this.name &&
                    !jQuery(this).is(":disabled") &&
                    rsubmittable.test(this.nodeName) &&
                    !rsubmitterTypes.test(type) &&
                    (this.checked || !rcheckableType.test(type));
            })
            .map(function(i, elem){
                var _this = $(this);
                var val = _this.val();
                var type = _this.attr('type');
                // contains potential type information about e.g. int, float.
                var dataType = _this.data('type');

                if (type === "checkbox") {
                    val = _this.is(':checked');
                }

                if (dataType === "int") {
                    val = parseInt(val);
                } else if (dataType === "float") {
                    val = parseFloat(val);
                } else if (dataType === "list") {
                    val = val.split(",");
                }

                if (val === null) {
                    return null;
                }

                if (jQuery.isArray(val)) {
                    console.log("YOOO", val);
                    return jQuery.map(val, function(val) {
                        return { name: elem.name, value: val.replace(rCRLF, "\r\n")};
                    });
                }
                if (typeof(val) === "string") {
                    val = val.replace(rCRLF, "\r\n");
                }
                return { name: elem.name, value: val};
            }).get();
        },
        serializeInputs: function() {
            var fields = this.serializeInputsArray();
            var data = {};
            for (var i = 0; i < fields.length; i++) {
                data[fields[i].name] = fields[i].value;
            }
            if (data.next) delete data.next;
            return data;
        }
    });
};


/**
 * Splits a string according to the provided splitChar, capitalized the first
 * character of each part and joins the parts with the provided joinChar.
 * The resulting string is then returned.
 */
app.module.helper.sentenceCase = function(str, splitChar, joinChar) {
    if (typeof str !== "string") {
        return;
    }

    function capitalizeFirstChar(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    var parts = str.split(splitChar);
    for (var i = 0; i < parts.length; i++) {
        parts[i] = capitalizeFirstChar(parts[i]);
    }

    return parts.join(joinChar);
};


/**
 * Checks if two objects equal in their properties.
 *
 * Source: http://adripofjavascript.com/blog/drips/object-equality-in-javascript.html
 *
 * @param {object} a - The object compared to b.
 * @param {object} b - The object compared to a.
 * @returns {boolean} - True if the objects are equal, otherwise false.
 */
app.module.helper.objectEquals = function(a, b) {
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);
    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length != bProps.length) {
        return false;
    }
    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];
        // If values of same property are not equal,
        // objects are not equivalent
        if (a[propName] !== b[propName]) {
            return false;
        }
    }
    // If we made it this far, objects
    // are considered equivalent
    return true;
};


/**
 * Initializes the functions in the helper module that need to be executed
 * to be used properly
 */
app.module.helper.init = function() {
    app.module.helper.extendJQuery();
};
