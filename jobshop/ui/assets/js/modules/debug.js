/**
 * Used to print out debug messages.
 *
 * @author Andreas Schmitz
 */

app.debug.banner = "FJSSP";
/*------------------------------------*\
    $DEBUG
\*------------------------------------*/
/*
 * Used to print messages to the console.
 * Can be disabled by setting app.debug.enabled to false
 * (recommended for production)
 * @public
 */
if (app.debug.enabled) {
    app.debug.log = function() {
        var mainArguments = Array.prototype.slice.call(arguments);
        var prefix = ['[+] [' + app.debug.banner + ']: '];
        console.log.apply(this, prefix.concat(mainArguments));
    };
    app.debug.error = function() {
        var mainArguments = Array.prototype.slice.call(arguments);
        var prefix = ['[+] [' + app.debug.banner + ']: '];
        console.error.apply(this, prefix.concat(mainArguments));
    };
} else {
    app.debug.log = function() {};
    app.debug.error = function() {};
}
