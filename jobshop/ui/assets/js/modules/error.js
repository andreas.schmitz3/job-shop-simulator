/**
 *
 * @author Andreas Schmitz
 *
 **/
app.error = {};

app.error.wrapper = undefined;

/**
 * Used to write a single line of error message into the main error DOM.
 */
app.error.write = function(message){
    if (app.error.wrapper === undefined){
        app.error.helper.init();
    }
    app.debug.log("Displaying user error messages...");
    app.error.wrapper.show();
    var error = $('<p>');
    error.attr('class', 'errorMsg');
    error.text(message);
    app.error.wrapper.append(error);
    app.debug.log("error message: " + message);
    app.error.wrapper.show();
    return error;
};


/**
 * Removes all existing error Messages
 */
app.error.clear = function() {
    if (app.error.wrapper === undefined){
        app.error.helper.init();
    }
    app.error.wrapper.find('.errorMsg').remove();
    app.error.wrapper.hide();
};


/**
 * Binds the close button to an actual event
 */
app.error.bindCloseBtn = function () {
    var close = app.error.wrapper.find('.close');
    close.on("click", function (event) {
        event.preventDefault();
        app.error.wrapper.hide('fast');
        app.error.clear();
    });
};

app.error.helper = {};

/**
 * Set the global error DOM variable and binds the close button
 */
app.error.helper.init = function() {
    app.debug.log("Initializing error wrapper...");
    app.error.wrapper = $('#errorWrapper');
    app.error.bindCloseBtn();
};

