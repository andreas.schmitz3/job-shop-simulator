/* jshint ignore:start */
import 'external/jquery-3.1.1.min';
import 'external/d3.min';
/* jshint ignore:end */

(function(window, document, $){
    "use strict";
    var app = {};
    app.page = {};
    app.module = {};
    app.debug = {};
    app.debug.enabled = true;
    import 'modules/debug';
    import 'modules/error';
    import 'modules/helper';
    import 'modules/websocket';
    import 'pages/login';
    import 'pages/home';
    import 'pages/simulations';
    import 'pages/simulation';
    import 'pages/shop';
    import 'pages/shops';
    import 'pages/chart';

    app.run = function() {
        var body = $('body');

        app.module.helper.init();

        if (body.hasClass('login')) {
            app.page.login.init();
        } else if (body.hasClass('home')) {
            app.page.home.init();
        } else if (body.hasClass('simulations')) {
            app.page.simulations.init();
        } else if (body.hasClass('simulation')) {
            app.page.simulation.init();
        } else if (body.hasClass('chart')) {
            app.page.chart.init();
        } else if (body.hasClass('shops')) {
            app.page.shops.init();
        } else if (body.hasClass('shop')) {
            app.page.shop.init();
        }
    };

    $(document).ready(function(argument) {
        app.run();
    });

}(window, document, jQuery));
