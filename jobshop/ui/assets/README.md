# Web Assets

This folder contains the uncompiled assets of the control server.
If changes to the web page of the control server are necessary, they should occur here.

To execute the build process some tools are necessary. Due to good alternatives, we use nodejs to check the files and perform the build process.

The result of the build process is that the files in the **static** folder are updated.

## Build process

### Prerequirements

  * **SASS:** A SASS compiler, e.g SASSC: https://github.com/sass/sassc
  * **Node.js:** Available via package managers e.g. `brew install nodejs` (macOS). For other platforms see here: [NodeJS Download](https://nodejs.org/en/download/package-manager/). Alternatively, [NVM](https://github.com/creationix/nvm) can be used. Simply enter `nvm use`, and the correct node version is selected.

### Installation

To install the necessary build tools, simply enter `npm install`.


### Creating a new build

To create a new build of the Javascript and CSS file, simpy execute `npm run build`. If everything goes fine, the files in the **static** folder should be updated.
