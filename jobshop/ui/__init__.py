# -*- coding: utf-8 -*-
"""Modules used to start a web server that is acts as an user interface to start
simulations for the Job-Shop Scheduling problem.

This is only a side-project and therefore not as well documented as the
`~jobshop.sim` and `~jobshop.ui` sub-packages.
"""
from jobshop.ui.simulation_server import SimulationServer
from jobshop.ui.app import make_app
from jobshop.ui.web_security import hardened_ssl_context
