# -*- coding: utf-8 -*-


class ProtocolVersion:
    """The version the web socket protocol uses.
    """
    #: Major digit of the protocol's version number.
    Major = 1
    #: Minor digit of the protocol's version number.
    Minor = 0
    #: A string representation of the protocols version number.
    String = "%s.%s" % (Major, Minor)
