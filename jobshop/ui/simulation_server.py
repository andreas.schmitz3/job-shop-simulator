# -*- coding: utf-8 -*-
import logging
import pkg_resources
import random

from jobshop.sim.data_generator import create_jobs_and_resources, load_distribution
from jobshop.sim.environment import SimulationEnvironment
from jobshop.common.database import CommonDatabase


class SimulationServer(object):
    """Acts as a middleware for the handlers instead of accessing the database
    layer directly.

    This results in a couple of methods that are similar to the database, but
    it makes the structure of the application clearer and allows for an easy
    exchange of the database if necessary.

    Args:
        database_file (str): The filename of the database used for the
            simulation.

    """
    def __init__(self, database_file):
        self._database_file = database_file
        self._database = CommonDatabase(database_file=self.database_file)

    @property
    def database_file(self):
        """str: The file to the database used to store the job-shop and
        simulation data **(read-only)**.
        """
        return self._database_file

    # -------------------------------------------------------------------------
    # USER RELATED FUNCTIONS
    # -------------------------------------------------------------------------
    def user_credentials(self, username):
        """Retrieves the user's hashed password from the database.

        Returns:
            str: the user's hashed password.

        Raises:
            RuntimeError: if retrieving the user's hashed password failed.

        """
        return self._database.user_credentials(username)

    # -------------------------------------------------------------------------
    # SIMULATION FUNCTIONS - GET, ADD, UPDATE AND DELETE
    # -------------------------------------------------------------------------
    def all_simulations(self, ascending=True):
        """Queries the database for all stored simulations and returns the list
        of all simulation ids.

        Args:
            ascending (boolean): Indicating whether the returned list should be
                sorted ascending (True) or descending (False).

        Returns:
            list: The list of ids of all simulations in the database.

        Raises:
            RuntimeError: if an error with the database occurred and the list
                of all simulations could not be retrieved.

        """
        return self._database.all_simulations(ascending)

    def simulation_for_id(self, simulation_id):
        """Returns a registered simulation object for the given simulation id.

        Args:
            simulation_id (int): An internal id, used by the database to
                reference to a simulation.

        Returns:
            Data stored for a simulation corresponding to the given id.

        Raises:
            RuntimeError: if retrieving the data went wrong.

        """
        return self._database.simulation_for_id(simulation_id)

    def delete_simulation(self, simulation_id):
        """Deletes a simulation with the specified id.

        Args:
            simulation_id (int): A database internal id used to identify one
                simulation.

        Raises:
            RuntimeError: if deleting the simulation with the specified id went
                wrong.

        """
        self._database.delete_simulation(simulation_id)

    def prepare_new_simulation(self, data):
        """Prepares the simulation by generating the simulation environment and
        creating a database entry for the simulation. Both values are returned
        and can be used with `start_simulation()` to start the simulation and
        store it's result in the database.

        Args:
            data(dict): A dictionary object that is used to create a new
                simulation.

        Returns:
            tuple: A tuple consisting of the database internal simulation id as
            first and a `~jobshop.sim.environment.SimulationEnvironment`
            object as second argument.

        """
        data['database'] = self._database
        simulation_env = SimulationEnvironment(**data)
        for sim in data['simulations']:
            simulation_env.add_simulation(
                scheduler_name=sim['scheduler'],
                executor_name=sim['executor'],
                scheduler_parameters=sim.get('scheduler_parameters'),
                executor_parameters=sim.get('executor_parameters')
            )
        simulation_id = self._database.prepare_new_simulation(simulation_env)
        return simulation_id, simulation_env

    def set_simulation_status(self, simulation_id, status):
        """Sets the status of an simulation to Running, Finished, or Failed.

        Args:
            simulation_id (int): The id of a simulation, created via
                `prepare_new_simulation()`.
            status (str): The status the simulation should be set to. Valid
                values are: ``Running``, ``Finished`` and ``Failed``.

        Raises:
            RuntimeError: if updating the status of the simulation in the
                database failed.

        """
        self._database.set_simulation_status(simulation_id, status)

    def start_simulation(self, simulation_id, simulation):
        """Starts to execute a simulation for the data created via the
        `prepare_new_simulation()` method.

        This function can take a long time to finish and therefore should be
        executed on another thread if possible.

        Args:
            simulation_id (int): The id of a simulation, created via
                `prepare_new_simulation()`.
            simulation (SimulationEnvironment): A simulation environment after
                its `run()` method.

        Raises:
            RuntimeError: if the execution of the simulation or storing the
            results in the database failed.

        """
        simulation.run()
        self._database.fill_simulation(simulation_id, simulation)

        for simtup in simulation.simulations:
            simres = simtup.executor.result
            try:
                assert not simres.is_failed(), "The simulation result is not valid. This seems to be an internal error. Please check the logs for further information."
            except AssertionError:
                # Since something went wrong, find out if some operations were
                # missing and list them if that is the case.
                all_ops = set()
                for res in simres.resources():
                    for restup in simres.resource_entries(res):
                        all_ops.add(restup.op)
                if simulation.scheduled_operations != all_ops:
                    missing_ops = list(simulation.scheduled_operations - all_ops)
                    missing_ops = [op.name for op in missing_ops]
                    missing_ops.sort()
                    logging.error("Something went wrong. The number of scheduled operations does not match the number of operations in the result of the %s executor." % simtup.executor.name())
                    logging.error("Number of operations (result / scheduled): %d / %d" % (len(all_ops), len(simulation.scheduled_operations)))
                    logging.error("Missing operations: %s" % missing_ops)
                raise

    def simulation_results(self, simulation_id):
        """Retrieves the data about the scheduler and executor objects that were
        used in the simulation.

        Args:
            simulation_id (int): An internal id, used by the database to
                reference to a simulation.

        Returns:
            dict: The different executor and scheduler pairs that make up the
            simulation.

        Raises:
            RuntimeError: if retrieving the data went wrong.

        """
        return self._database.simulation_results(simulation_id)

    def simulation_result_for_id(self, result_id):
        """Retrieves the simulation result data for a given id.

        Args:
            result_id (int): An internal id, used by the database to
                reference to a simulation result.

        Returns:
            dict: The data of the simulation result.

        Raises:
            RuntimeError: if retrieving the data went wrong.

        """
        return self._database.simulation_result_for_id(result_id)

    def simulation_result_performance_for_id(self, simulation_result_id):
        """Gets all the performance metrics for a given simulation result id.

        Args:
            result_id (int): A database internal id for a simulation result.

        Returns:
            dict: A dictionary with the name of the performance metric as key
            and the value of the metric as value.

        Raises:
            RuntimeError: if querying the database for the performance metrics
                failed.

        """
        return self._database.simulation_result_performance_for_id(simulation_result_id)

    def all_shops(self, ascending=True):
        """Queries the database for all stored shops and returns the list
        of all shop ids.

        Args:
            ascending (boolean): Indicating whether the returned list should be
                sorted ascending (True) or descending (False).

        Returns:
            list: The list of ids of all shops in the database.

        Raises:
            RuntimeError: if retrieving the list of shops from the database
                failed.

        """
        return self._database.all_shops(ascending)

    def shop_for_id(self, shop_id):
        """Queries the database for a shop with the specified id.

        Args:
            shop_id (int): The database internal id of a shop.

        Returns:
            dict: A dictionary containing the data for the shop.

        Raises:
            RuntimeError: if retrieving the shop data from the database failed.

        """
        return self._database.shop_for_id(shop_id)

    def new_shop(self, data):
        """Creates a new shop in the database for the provided data.

        Args:
            data (dict): A dictionary that contains the necessary data to
                create a new shop.

        Raises:
            RuntimeError: if creating and storing the new shop did not succeed

        """
        try:
            res_file = pkg_resources.resource_filename('jobshop', "share/distributions/resource_performance.csv")
            res_perf = load_distribution(res_file)
            proctime_file = pkg_resources.resource_filename('jobshop', "share/distributions/processing_times.csv")
            proctime = load_distribution(proctime_file)
            numops_file = pkg_resources.resource_filename('jobshop', "share/distributions/number_operations.csv")
            numops = load_distribution(numops_file)
        except FileNotFoundError as e:
            raise RuntimeError(e)

        if data['seed']:
            seed = "%s" % data['seed']
        else:
            seed = "%d" % random.getrandbits(64)
        try:
            jobs, resources = create_jobs_and_resources(
                seed=seed,
                num_resources=data['resources'],
                num_jobs=data['jobs'],
                op_min_res=data['min_resources'],
                op_max_res=data['max_resources'],
                resource_performance_distr=res_perf,
                processing_time_distr=proctime,
                num_operations_distr=numops
            )
        except AssertionError as e:
            raise RuntimeError(e)

        try:
            shop_id = self._database.create_shop(jobs, resources, name=data['name'], description=data['description'], seed=seed)
        except RuntimeError:
            raise
        else:
            return shop_id

    def delete_shop(self, shop_id):
        """Deletes a shop, its jobs, resources, operations and all the
        simulations performed on it from the database.

        Args:
            shop_id (int): A database internal id used to identify a shop.

        Raises:
            RuntimeError: if an exception occurred during the deletion of the
                shop. The internal state of the database is rolled back
                if this happens. So, if an exception is raised, it can be
                assumed that the internal state of the database before, and
                after the call of this function is the same.

        """
        self._database.delete_shop(shop_id)
