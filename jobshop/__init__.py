"""The package is made up of three sub-packages

- `~jobshop.sim` - Modules used to carry out simulations and generate data sets.
- `~jobshop.ui` - Modules used to create a web UI that uses the client modules
to create new data or start new simulations.
- `~jobshop.common` - Modules that are used by either of the previous named
  modules.

"""

__author__ = 'Andreas Schmitz'
__versioninfo__ = (1, 0, 0)  # NOTE: Also adapt in setup.py
__version__ = '.'.join(map(str, __versioninfo__))
