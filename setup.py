# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from os import path


# Get the long description from the README file
here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='job-shop-simulator',
    version='1.0.0',  # NOTE: Also adapt in jobshop/__init__.py
    description='Simulator to analyze centralized and decentralized algorithms for solving real-world (flexible) job-shop scheduling problems.',
    long_description=long_description,
    author='Andreas Schmitz',
    author_email='andreas.schmitz3@rwth-aachen.de',
    url='https://git.rwth-aachen.de/andreas.schmitz3/job-shop-simulator',
    license='MIT',
    packages=find_packages(exclude=['tests*', 'doc', 'dependency-backup']),
    install_requires=[
        'simpy~=3.0',
        'tornado~=4.4',
        'orderedset~=2.0',
        'appdirs~=1.4',
        'passlib~=1.7'
    ],
    # Files bundled with bdist_wheel etc.
    # Also make the changes in the MANIFEST.in
    package_data={
        'jobshop': [
            'share/distributions/*.csv',
            'share/schema/*.sql',
        ],
        'jobshop.ui': [
            'static/app.css',
            'static/app.js',
            'templates/*html'
        ]
    },
    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
    extras_require={
        'dev': [
            'sphinx~=1.5',
            'sphinx_rtd_theme>=0.1.9',
            'wheel>=0.29.0'
        ],
        'test': [
            'coverage>=4.3.4',
            'nose>=1.3.7'
        ],
    },
    scripts=[
        'scripts/new-simulation',
        'scripts/generate-shop',
        'scripts/import-shop',
        'scripts/job-shop-simulator',
        'scripts/manage-simulator',
    ],
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 5 - Production/Stable',

        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Information Analysis',

        # Environment
        'Environment :: Console',
        'Environment :: Web Environment',

        # Supported operating systems
        'Operating System :: Unix',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX :: Linux',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        'Natural Language :: English',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: JavaScript',
    ],
    keywords='operations-research flexible-job-shop-scheduling visualization simulation machine-scheduling',
)
