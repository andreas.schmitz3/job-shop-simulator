#!/bin/bash
source .venv/bin/activate
nosetests -v --with-coverage --cover-package=jobshop.sim --cover-package=jobshop.common
