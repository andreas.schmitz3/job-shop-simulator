Scripts
=======

The following scripts are shipped alongside the project.

  - ``job-shop-simulator`` starts a web-server that allows to create new shops, start simulations and visualize simulation results. Use the ``--help`` option for more information.
  - ``manage-simulator`` is used to create, delete and update user accounts for the ``job-shop-simulator`` web ui. See ``--help`` for more information.
  - ``new-simulation`` allows to create and run new simulation runs of different strategies. It is completely independent of the ``job-shop-simulator``, although both can use the same sqlite database as data source. The different strategies run by the script are configured using a config file. See ``--help`` for more information.
  - ``generate-shop`` generates new test instances (shops) that can be used by the ``job-shop-simulator`` or ``new-simulation`` tool to run simulations on. See ``--help`` for more information.
  - ``import-shop`` allows to import standard `OR-Lib <http://people.brunel.ac.uk/~mastjjb/jeb/info.html>`_  job-shop, or OR-Lib compatible test instances, for example: http://edoc.sub.uni-hamburg.de/hsu/volltexte/2012/2982/. See ``--help`` for more information.  A couple of test instances are already shipped with this project, they reside in the share folder. Example: ``import-shop jobshop/share/instances/0_BehnkeGeiger/Behnke1.fjs``.
