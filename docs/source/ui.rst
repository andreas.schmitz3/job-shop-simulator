``jobshop.ui`` --- Web UI Modules
=====================================

.. automodule:: jobshop.ui
    :members:


Modules
-------
.. toctree::
    ui/app
    ui/simulation_server
    ui/websocket
    ui/web_security
    ui/handler
    ui/helpers
    ui/protocol
