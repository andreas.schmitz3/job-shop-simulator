
``jobshop.sim.executor`` --- Executor
===========================================

.. automodule:: jobshop.sim.executor
    :members:
    :show-inheritance:


.. toctree::
    :hidden:

    executor/base_class
    executor/executor_factory
    executor/no_communication
    executor/machine_timeout
    executor/operation_timeout
    executor/operation_communication
    executor/operator_communication
