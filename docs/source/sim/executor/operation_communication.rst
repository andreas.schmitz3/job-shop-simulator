
OperationCommunication Strategy
===============================

.. automodule:: jobshop.sim.executor.operation_communication
    :members:
    :undoc-members:
    :private-members:
