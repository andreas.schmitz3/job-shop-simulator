
OperatorCommunication Strategy
==============================

.. automodule:: jobshop.sim.executor.operator_communication
    :members:
    :undoc-members:
    :private-members:
