
Executor Base Class
===================

.. automodule:: jobshop.sim.executor.base_class
    :members:
    :undoc-members:
    :private-members:
