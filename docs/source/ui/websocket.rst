``jobshop.ui.websocket`` --- Websocket Interface
================================================

.. automodule:: jobshop.ui.websocket
    :members:
    :show-inheritance:
