``jobshop.ui.simulation_server`` --- Simulation Server Middleware
=================================================================

.. automodule:: jobshop.ui.simulation_server
    :members:
    :show-inheritance:
