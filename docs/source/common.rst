``jobshop.common`` --- Common Modules
=====================================

.. automodule:: jobshop.common
    :members:


Modules
-------
.. toctree::
    common/database
    common/result_data
    common/log
