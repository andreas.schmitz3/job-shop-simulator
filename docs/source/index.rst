Flexible Job-Shop Scheduling Simulation
=======================================

This is the documentation of the ``job-shop-simulator`` project. The repository of this project can be found here: `Job Shop Simulator, RWTH Gitlab <https://git.rwth-aachen.de/andreas.schmitz3/job-shop-simulator>`_.

Also see the `Job Shop Evaluator, RWTH Gitlab <https://git.rwth-aachen.de/andreas.schmitz3/job-shop-evaluator>`_ project for an evaluation tool that makes use of the simulator. The ``job-shop-evaluator`` project is separated from this project due to licensing reasons.

The project is divided into three sub-packages which are listed below.

Part of the project are a couple of scripts / applications which make use of the modules of this project.

.. toctree::
    :titlesonly:

    sim
    common
    ui
    scripts

The `jobshop` package contains a ``share`` folder which contains the database schema, that is automatically loaded once a new database object is created and a database file isn't available at the specified location. Furthermore, the share folder contains a list of FJSSP test instances and a distribution folder which contains mean and standard deviation values that are used to create new test instances.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

