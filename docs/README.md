# Job-Shop Simulator Documentation

This folder contains the documentation of the ``job-shop-simulator`` project. The documentation was created, using the Sphinx documentation generator tool.

Sphinx allows to generate different output formats, which are stored in the ``build`` folder.

A HTML version of the documentation should already be build and available in the ``build/html`` folder. Simply open the ``index.html`` file to access the documentation.

## Creating further outputs.

To run the Sphinx tool, follow the **Installation from Source** instructions of the ``job-shop-simulator`` project, using the **(editable mode including development and test dependencies)** flags to install the dependencies.

Afterwards, a new output can be generated using the ``make`` command. Note that ``make`` has to be installed on your system. On Linux systems ``make`` can be installed by installing the ``build-essential`` package.

Simply execute ``make`` to get a list of available outputs.
