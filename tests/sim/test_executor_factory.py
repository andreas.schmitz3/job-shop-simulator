# -*- coding: utf-8 -*-


from jobshop.sim.executor import ExecutorFactory, Executor, NoCommunication, MachineTimeout, OperationTimeout, OperationCommunication, OperatorCommunication
from jobshop.sim.model import StatePool
from jobshop.sim.deviation import Deviation
from jobshop.common.result_data import SimulationMetadata

import random
import simpy
import unittest


class TestExecutorFactory(unittest.TestCase):
    """Tests the `ExecutorFactory` class of the `jobshop.sim.executor`
    module
    """
    def setUp(self):
        self.env = simpy.Environment()
        self.metdata = SimulationMetadata()
        self.dev = Deviation(random.Random(42))
        self.executor = ExecutorFactory.create_executor(
            "NoCommunication",
            env=self.env,
            deviation=self.dev,
            simulation_metadata=self.metdata
        )

    def test_available_executors(self):
        executors = ExecutorFactory.available_executors()
        cmp_list = [
            'NoCommunication',
            'MachineTimeout',
            'OperationTimeout',
            'OperationCommunication',
            'OperatorCommunication',
        ]
        self.assertCountEqual(executors, cmp_list)

    def test_name(self):
        executors = ExecutorFactory.available_executors()
        for executor in executors:
            exec_obj = ExecutorFactory.create_executor(
                executor,
                env=self.env,
                deviation=self.dev,
                simulation_metadata=self.metdata
            )
            print(exec_obj.name())
            self.assertEqual(exec_obj.name(), exec_obj.__class__.__name__)

    def test_create_executor_for_string(self):
        assert Executor.__subclasscheck__(self.executor.__class__)

    def test_inheritance(self):
        self.assertIsInstance(self.executor._states, StatePool)

    def test_arguments(self):
        self.assertEqual(self.dev, self.executor._deviation)
        self.assertEqual(self.env, self.executor._env)

    def test_list_arguments(self):
        arguments = {
            'NoCommunication': NoCommunication.arguments(),
            'MachineTimeout': MachineTimeout.arguments(),
            'OperationTimeout': OperationTimeout.arguments(),
            'OperationCommunication': OperationCommunication.arguments(),
            'OperatorCommunication': OperatorCommunication.arguments(),
        }
        for name in ExecutorFactory.available_executors():
            self.assertEqual(arguments[name], ExecutorFactory.executor_arguments(name))
        self.assertEqual(len(arguments), len(ExecutorFactory.available_executors()))

