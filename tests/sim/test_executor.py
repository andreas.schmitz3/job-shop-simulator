# -*- coding: utf-8 -*-

from jobshop.sim.data_generator import create_jobs_and_resources


from jobshop.sim.executor import ExecutorFactory, MachineTimeout, OperationTimeout
from jobshop.sim.scheduler import FirstInFirstOut
from jobshop.sim.deviation import Deviation
from jobshop.common.result_data import SimulationMetadata

import random
import simpy
import unittest
import statistics


# TODO: check if number of operations is the same: e.g. operations went missing from schedule to result, Check all tests and complete them.
class CommonEnvironment(unittest.TestCase):
    """Used by the different executors as a common testing environment.
    """
    def setUp(self):
        self.metdata = SimulationMetadata()
        self.jobs, self.resources = create_jobs_and_resources(
            seed=5235,
            num_resources=5,
            num_jobs=6,
            min_ops=4,
            max_ops=10,
            op_min_res=1,
            op_max_res=4
        )
        f = FirstInFirstOut()
        self.ops = []
        for job in self.jobs:
            self.ops += job.operations
            self.metdata.add_job_entry_time(job, 0)
        self.schedule = f.create_schedule(self.ops)
        self.env = simpy.Environment()
        self.dev = Deviation(random.Random(42))
        for res in self.resources:
            self.dev.add_resource(res, 0)

    def checkHasAllOps(self, simresult):
        simres_ops = set()
        all_ops = set(self.ops)
        op_count = 0
        for res in simresult.resources():
            op_count += len(simresult.resource_entries(res))
            for restup in simresult.resource_entries(res):
                simres_ops.add(restup.op)
        missing_ops = list(all_ops - simres_ops)
        missing_ops = [op.name for op in missing_ops]
        missing_ops.sort()
        self.assertEqual(op_count, len(self.ops))
        self.assertEqual(missing_ops, [])


class Test_ExecutorBasics(CommonEnvironment):
    """
    """
    def test_basic_execution(self):
        for exec_name in ExecutorFactory.available_executors():
            executor = ExecutorFactory.create_executor(
                exec_name,
                self.env,
                deviation=self.dev,
                simulation_metadata=self.metdata
            )
            executor.add_schedule(self.schedule)
            self.env.run()
            self.checkHasAllOps(executor.result)
        # TODO: Implement

    def test_mean_resource_performance(self):
        executor = ExecutorFactory.create_executor(
            "NoCommunication",
            self.env,
            deviation=self.dev,
            simulation_metadata=self.metdata
        )
        executor.add_schedule(self.schedule)
        # Check if 1.0 is returned if no value was added yet.
        self.assertAlmostEqual(executor.runtime_resource_performance("1"), 1.0)

        # Add some random values and check the calculated mean.
        comp = []
        seed = random.getrandbits(64)
        my_rand = random.Random(seed)
        for i in range(100):
            rand_int = my_rand.random()
            executor.add_runtime_resource_performance("1", rand_int)
            comp.append(rand_int)
        self.assertAlmostEqual(executor.runtime_resource_performance("1"), statistics.mean(comp))
        print("The random number was seeded with: %d. This can be usedul to reproduce the error." % seed)
        print(executor._resource_performance)


class Test_MachineTimeout(CommonEnvironment):
    """
    """
    def test_watchdog_default(self):
        executor = MachineTimeout(self.env, self.dev, simulation_metadata=self.metdata)
        executor.add_schedule(self.schedule)
        self.env.run()
        # TODO: Implement

    def test_watchdog_no_autorestart(self):
        executor = MachineTimeout(self.env, self.dev, simulation_metadata=self.metdata)
        executor.add_schedule(self.schedule)
        self.env.run()
        # TODO: Implement

    def test_watchdog_changing_timeout(self):
        executor = MachineTimeout(self.env, self.dev, simulation_metadata=self.metdata)
        executor.add_schedule(self.schedule)
        self.env.run()
        # TODO: Implement


class Test_OperationTimeout(CommonEnvironment):

    def setUp(self):
        super().setUp()
        self.executor = OperationTimeout(
            self.env,
            self.dev,
            simulation_metadata=self.metdata
        )
        self.executor.add_schedule(self.schedule)

    def test_resource_idle_time(self):
        self.assertAlmostEqual(self.executor._mean_resource_idle_time("1"), 0.0)

        self.executor._resource_starts_idling("1", 0)
        self.executor._resource_stops_idling("1", 5)
        self.executor._resource_starts_idling("1", 15)
        self.executor._resource_stops_idling("1", 20)
        self.executor._resource_starts_idling("1", 20)
        self.executor._resource_stops_idling("1", 20)
        self.executor._resource_starts_idling("1", 25)
        self.executor._resource_stops_idling("1", 30)
        self.executor._resource_starts_idling("1", 45)
        self.executor._resource_stops_idling("1", 50)
        self.assertAlmostEqual(self.executor._mean_resource_idle_time("1"), 0.4)
        print(self.executor._mean_resource_idle_time("1"))


if __name__ == '__main__':
    unittest.main()
