# -*- coding: utf-8 -*-
from jobshop.sim.fjssp_text_parser import FJSSPTextParser
from jobshop.sim.model import Operation, Resource, Job

import pkg_resources
from collections import OrderedDict
import unittest
import os.path


class Test_FJSSPTextParser(unittest.TestCase):

    def setUp(self):
        self.test_dir = pkg_resources.resource_filename('jobshop', "share/instances")

    def test_parser(self):
        # Create objects from the test data
        # Flexibility: (machine, processing time)
        test_data = OrderedDict()
        test_data["1"] = [
            [(1, 43)],  # Operation 1 of Job 1
            [(1, 87), (2, 95)],
        ]
        test_data["2"] = [
            [(1, 63), (2, 53)],
            [(2, 73)],
        ]
        test_data["3"] = [
            [(1, 125), (2, 135)],
            [(1, 43), (2, 61)],
        ]

        test_jobs = []
        for jobname, ops in test_data.items():
            job = Job(jobname)
            test_jobs.append(job)
            for i, op_caps in enumerate(ops):
                op = Operation(i + 1)
                job.add_operation(op)
                for res, cost in op_caps:
                    op.add_resource(Resource(res), cost)

        # Use the parser to load test data from a file.
        test_file = os.path.join(self.test_dir, '6_Fattahi', 'Fattahi3.fjs')
        jobs, resources = FJSSPTextParser.parseInput(test_file)

        # Test equality
        for job, tjob in zip(jobs, test_jobs):
            print("Parsed: %s, Test: %s" % (repr(job), repr(tjob)))
            for ops, tops in zip(job.operations, tjob.operations):
                print("Parsed: %s, Test: %s" % (repr(ops), repr(tops)))
                self.assertEqual(repr(ops), repr(tops))


if __name__ == '__main__':
    unittest.main()
