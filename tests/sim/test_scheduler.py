# -*- coding: utf-8 -*-

from jobshop.sim.data_generator import create_jobs_and_resources
from jobshop.sim.scheduler import Scheduler, FirstInFirstOut, ShortestProcessingTime, Ziaee, Random
from jobshop.sim.scheduler import SchedulerFactory

import random
import unittest
from nose.tools import raises
from orderedset import OrderedSet
from operator import attrgetter


class Test_SchedulerFactory(unittest.TestCase):
    """Tests the `SchedulerFactory` class of the `jobshop.sim.scheduler`
    module
    """
    def test_available_schedulers(self):
        scheds = SchedulerFactory.available_schedulers()
        schedulers = [
            'FirstInFirstOut',
            'ShortestProcessingTime',
            'Random',
            'Ziaee'
        ]
        self.assertCountEqual(scheds, schedulers)

    def test_scheduler_names(self):
        schedulers = SchedulerFactory.available_schedulers()
        for sched in schedulers:
            s = SchedulerFactory.get_scheduler(sched)
            self.assertEqual(s.name(), s.__class__.__name__)

    def test_all_scheduler(self):
        schedulers = SchedulerFactory.available_schedulers()
        for sched in schedulers:
            s = SchedulerFactory.get_scheduler(sched)
            assert Scheduler.__subclasscheck__(s.__class__)

    def test_get_scheduler_for_string(self):
        s = SchedulerFactory.get_scheduler("FirstInFirstOut")
        assert Scheduler.__subclasscheck__(s.__class__)

    @raises(ValueError)
    def test_get_scheduler_fail(self):
        SchedulerFactory.get_scheduler("NotAScheduler")


class CommonEnvironment(unittest.TestCase):
    """
    """
    def setUp(self):
        self.jobs, self.resources = create_jobs_and_resources(
            seed=5235,
            num_resources=5,
            num_jobs=6,
            min_ops=4,
            max_ops=10,
            op_min_res=1,
            op_max_res=4
        )
        self.operations = []
        for job in self.jobs:
            self.operations += job.operations

    def print_schedule(self, schedule):
        """Prints a given schedule to stdout. Useful for debugging.
        """
        test_jobs = {}
        for res, schedtup in schedule.schedule.items():
            for i, tupl, in enumerate(schedtup):
                print("res %s, op %s, start %s, finish %s" % (res, tupl.op, tupl.start, tupl.finish))
                try:
                    j_list = test_jobs[tupl.op.job]
                except KeyError:
                    j_list = {}
                    test_jobs[tupl.op.job] = j_list
                j_list[tupl.op] = ((res, tupl.start, tupl.finish))

        for job, j_list in test_jobs.items():
            for op in job.operations:
                if op not in j_list:
                    continue
                print("op %s, res %s, start %s, finish %s" % (op, *j_list[op]))

    def check_schedule(self, schedule, test_schedule, check_simres=False, is_partial=False):
        """Compares the computed schedule ``schedule`` with the test set
        ``test_schedule``.
        """
        op_set = set()
        op_list = []
        for res, s_tuples in schedule.schedule.items():
            res_queue = test_schedule[res]
            self.assertEqual(len(res_queue), len(s_tuples))
            for i, tupl in enumerate(s_tuples):
                op_set.add(tupl.op)
                op_list.append(tupl.op)
                self.assertEqual(tupl.op, res_queue[i][0])
                self.assertEqual(tupl.start, res_queue[i][1])
                self.assertEqual(tupl.finish, res_queue[i][2])
        self.assertEqual(len(test_schedule), len(schedule.schedule))
        self.assertEqual(len(op_set), len(op_list))

        if check_simres:
            self.assertTrue(len(schedule.schedule) >= 0)

            jobs = {}
            for res in schedule.schedule:
                last_finish = 0
                for s_tuple in schedule.schedule[res]:
                    self.assertIn(res, s_tuple.op.resources)

                    try:
                        s_tuples = jobs[s_tuple.op.job]
                    except KeyError:
                        s_tuples = []
                        jobs[s_tuple.op.job] = s_tuples
                    s_tuples.append(s_tuple)

                    self.assertLessEqual(last_finish, s_tuple.start)
                    last_finish = s_tuple.finish

            for job, s_tuples in jobs.items():
                s_tuples.sort(key=attrgetter('op.sequence_number'))
                ops = [rt.op for rt in s_tuples]
                if not is_partial:
                    for op in job.operations:
                        self.assertIn(op, ops)
                for i in range(len(s_tuples) - 1):
                    self.assertLessEqual(s_tuples[i].finish, s_tuples[i + 1].start)


class Test_FirstInFirstOut(CommonEnvironment):
    """Tests the `FirstInFirstOut` class of the `jobshop.sim.scheduler` module"""
    def test_fifo_schedule(self):
        res_schedule = {
            self.resources[0]: [
                (self.jobs[0].operations[3], None, None),
                (self.jobs[1].operations[0], None, None),
                (self.jobs[1].operations[4], None, None),
                (self.jobs[1].operations[5], None, None),
                (self.jobs[1].operations[7], None, None),
                (self.jobs[3].operations[3], None, None),
                (self.jobs[4].operations[1], None, None),
                (self.jobs[4].operations[3], None, None),
                (self.jobs[4].operations[4], None, None),
                (self.jobs[5].operations[4], None, None)
            ],
            self.resources[1]: [
                (self.jobs[0].operations[4], None, None),
                (self.jobs[1].operations[2], None, None),
                (self.jobs[2].operations[3], None, None),
                (self.jobs[2].operations[4], None, None),
                (self.jobs[5].operations[1], None, None),
                (self.jobs[5].operations[7], None, None)
            ],
            self.resources[2]: [
                (self.jobs[0].operations[2], None, None),
                (self.jobs[1].operations[6], None, None),
                (self.jobs[2].operations[0], None, None),
                (self.jobs[3].operations[1], None, None),
                (self.jobs[4].operations[2], None, None)
            ],
            self.resources[3]: [
                (self.jobs[0].operations[0], None, None),
                (self.jobs[1].operations[3], None, None),
                (self.jobs[2].operations[1], None, None),
                (self.jobs[2].operations[2], None, None),
                (self.jobs[4].operations[0], None, None),
                (self.jobs[5].operations[2], None, None)
            ],
            self.resources[4]: [
                (self.jobs[0].operations[1], None, None),
                (self.jobs[1].operations[1], None, None),
                (self.jobs[1].operations[8], None, None),
                (self.jobs[3].operations[0], None, None),
                (self.jobs[3].operations[2], None, None),
                (self.jobs[5].operations[0], None, None),
                (self.jobs[5].operations[3], None, None),
                (self.jobs[5].operations[5], None, None),
                (self.jobs[5].operations[6], None, None)
            ]
        }

        sched = FirstInFirstOut.create_schedule(operations=self.operations)
        self.check_schedule(sched, res_schedule)

    def test_fifo_sub_schedule(self):
        res_schedule = {
            self.resources[0]: [
                (self.jobs[1].operations[0], None, None),
                (self.jobs[1].operations[7], None, None),
                (self.jobs[4].operations[4], None, None),
            ],
            self.resources[3]: [
                (self.jobs[5].operations[2], None, None),
                (self.jobs[2].operations[1], None, None),
            ],
            self.resources[4]: [
                (self.jobs[1].operations[1], None, None),
            ],
        }

        # Pick a couple of operations to simulate that only a couple of
        # operations are left to be rescheduled.
        ops = OrderedSet()
        rand = random.Random(85674)
        for _ in range(6):
            ops.add(rand.choice(self.operations))

        # Create the schedule and test it.
        sched = FirstInFirstOut.create_schedule(operations=ops)
        self.check_schedule(sched, res_schedule)


class Test_Random(CommonEnvironment):
    """Tests the `FirstInFirstOut` class of the `jobshop.sim.scheduler` module"""
    def test_random_schedule(self):
        res_schedule = {
            self.resources[0]: [
                (self.jobs[1].operations[0], None, None),
                (self.jobs[1].operations[4], None, None),
                (self.jobs[1].operations[7], None, None),
                (self.jobs[5].operations[1], None, None),
                (self.jobs[5].operations[4], None, None),
                (self.jobs[0].operations[3], None, None),
                (self.jobs[4].operations[3], None, None),
                (self.jobs[3].operations[1], None, None),
            ],
            self.resources[1]: [
                (self.jobs[1].operations[8], None, None),
                (self.jobs[5].operations[7], None, None),
                (self.jobs[0].operations[4], None, None),
                (self.jobs[3].operations[3], None, None),
            ],
            self.resources[2]: [
                (self.jobs[1].operations[6], None, None),
                (self.jobs[5].operations[2], None, None),
                (self.jobs[2].operations[0], None, None),
                (self.jobs[2].operations[2], None, None),
                (self.jobs[0].operations[2], None, None),
                (self.jobs[4].operations[0], None, None),
                (self.jobs[4].operations[2], None, None),
                (self.jobs[4].operations[4], None, None),
                (self.jobs[3].operations[2], None, None),
            ],
            self.resources[3]: [
                (self.jobs[1].operations[3], None, None),
                (self.jobs[1].operations[5], None, None),
                (self.jobs[2].operations[1], None, None),
                (self.jobs[2].operations[4], None, None),
                (self.jobs[0].operations[0], None, None),
            ],
            self.resources[4]: [
                (self.jobs[1].operations[1], None, None),
                (self.jobs[1].operations[2], None, None),
                (self.jobs[5].operations[0], None, None),
                (self.jobs[5].operations[3], None, None),
                (self.jobs[5].operations[5], None, None),
                (self.jobs[5].operations[6], None, None),
                (self.jobs[2].operations[3], None, None),
                (self.jobs[0].operations[1], None, None),
                (self.jobs[4].operations[1], None, None),
                (self.jobs[3].operations[0], None, None),
            ]
        }

        sched = Random.create_schedule(operations=self.operations, rand_gen=random.Random(5823))
        self.print_schedule(sched)
        self.check_schedule(schedule=sched, test_schedule=res_schedule)

    def test_random_sub_schedule(self):
        res_schedule = {
            self.resources[2]: [
                (self.jobs[4].operations[0], None, None),
                (self.jobs[4].operations[2], None, None),
                (self.jobs[1].operations[4], None, None),
                (self.jobs[5].operations[2], None, None),
            ],
            self.resources[3]: [
                (self.jobs[2].operations[2], None, None),
            ],
            self.resources[4]: [
                (self.jobs[1].operations[8], None, None),
            ],
        }

        # Pick a couple of operations to simulate that only a couple of
        # operations are left to be rescheduled.
        ops = OrderedSet()
        rand = random.Random(79493)
        for _ in range(6):
            ops.add(rand.choice(self.operations))

        # Create the schedule and test it.
        sched = Random.create_schedule(operations=ops, rand_gen=random.Random(658))
        self.print_schedule(sched)
        self.check_schedule(sched, res_schedule)


class Test_ShortestProcessingTime(CommonEnvironment):
    """Tests the `ShortestProcessingTime` class of the `jobshop.sim.scheduler` module"""

    def test_spt(self):
        res_schedule = {
            self.resources[0]: [
                (self.jobs[0].operations[3], None, None),
                (self.jobs[4].operations[1], None, None),
                (self.jobs[4].operations[3], None, None),
                (self.jobs[4].operations[4], None, None),
                (self.jobs[3].operations[3], None, None),
                (self.jobs[5].operations[4], None, None),
                (self.jobs[1].operations[0], None, None),
                (self.jobs[1].operations[4], None, None),
                (self.jobs[1].operations[5], None, None),
                (self.jobs[1].operations[7], None, None),
            ],
            self.resources[1]: [
                (self.jobs[0].operations[4], None, None),
                (self.jobs[2].operations[3], None, None),
                (self.jobs[2].operations[4], None, None),
                (self.jobs[5].operations[1], None, None),
                (self.jobs[5].operations[7], None, None),
                (self.jobs[1].operations[2], None, None),
            ],
            self.resources[2]: [
                (self.jobs[0].operations[2], None, None),
                (self.jobs[2].operations[0], None, None),
                (self.jobs[4].operations[2], None, None),
                (self.jobs[3].operations[1], None, None),
                (self.jobs[1].operations[6], None, None),
            ],
            self.resources[3]: [
                (self.jobs[0].operations[0], None, None),
                (self.jobs[2].operations[1], None, None),
                (self.jobs[2].operations[2], None, None),
                (self.jobs[4].operations[0], None, None),
                (self.jobs[5].operations[2], None, None),
                (self.jobs[1].operations[3], None, None),
            ],
            self.resources[4]: [
                (self.jobs[0].operations[1], None, None),
                (self.jobs[3].operations[0], None, None),
                (self.jobs[3].operations[2], None, None),
                (self.jobs[5].operations[0], None, None),
                (self.jobs[5].operations[3], None, None),
                (self.jobs[5].operations[5], None, None),
                (self.jobs[5].operations[6], None, None),
                (self.jobs[1].operations[1], None, None),
                (self.jobs[1].operations[8], None, None),
            ]
        }
        sched = ShortestProcessingTime.create_schedule(operations=self.operations)
        self.check_schedule(sched, res_schedule)

    def test_spt_sub_schedule(self):
        res_schedule = {
            self.resources[0]: [
                (self.jobs[4].operations[4], None, None),
                (self.jobs[1].operations[0], None, None),
                (self.jobs[1].operations[7], None, None),
            ],
            self.resources[3]: [
                (self.jobs[5].operations[2], None, None),
                (self.jobs[2].operations[1], None, None),
            ],
            self.resources[4]: [
                (self.jobs[1].operations[1], None, None),
            ],
        }

        # Pick a couple of operations to simulate that only a couple of
        # operations are left to be rescheduled.
        ops = OrderedSet()
        rand = random.Random(85674)
        for _ in range(6):
            ops.add(rand.choice(self.operations))

        # Create the schedule and test it.
        sched = ShortestProcessingTime.create_schedule(operations=ops)
        self.check_schedule(sched, res_schedule)


class Test_ZiaeeHeuristic(CommonEnvironment):
    """Tests the `FirstInFirstOut` class of the `jobshop.sim.scheduler` module"""

    def test_ziaee(self):
        res_schedule = {
            self.resources[0]: [
                (self.jobs[4].operations[1], 66, 207),
                (self.jobs[3].operations[1], 218, 347),
                (self.jobs[0].operations[2], 347, 431),
                (self.jobs[0].operations[3], 431, 565),
                (self.jobs[1].operations[4], 565, 654),
                (self.jobs[1].operations[5], 654, 703),
                (self.jobs[5].operations[5], 703, 802),
                (self.jobs[1].operations[7], 802, 876),
                (self.jobs[1].operations[8], 876, 993),
            ],
            self.resources[1]: [
                (self.jobs[1].operations[0], 0, 107),
                (self.jobs[5].operations[1], 107, 183),
                (self.jobs[2].operations[1], 183, 309),
                (self.jobs[2].operations[2], 309, 442),
                (self.jobs[5].operations[3], 442, 559),
                (self.jobs[2].operations[4], 559, 573),
                (self.jobs[0].operations[4], 573, 613),
                (self.jobs[5].operations[7], 932, 989)
            ],
            self.resources[2]: [
                (self.jobs[4].operations[0], 0, 66),
                (self.jobs[1].operations[1], 107, 233),
                (self.jobs[5].operations[2], 233, 292),
                (self.jobs[4].operations[2], 292, 372),
                (self.jobs[3].operations[3], 464, 597),
                (self.jobs[4].operations[4], 597, 674),
                (self.jobs[1].operations[6], 703, 801),
                (self.jobs[5].operations[6], 802, 932),
            ],
            self.resources[3]: [
                (self.jobs[5].operations[0], 0, 106),
                (self.jobs[0].operations[0], 106, 189),
                (self.jobs[0].operations[1], 189, 316),
                (self.jobs[1].operations[3], 342, 408),
                (self.jobs[4].operations[3], 408, 505),
                (self.jobs[5].operations[4], 559, 683),
            ],
            self.resources[4]: [
                (self.jobs[2].operations[0], 0, 125),
                (self.jobs[3].operations[0], 125, 218),
                (self.jobs[1].operations[2], 233, 342),
                (self.jobs[3].operations[2], 347, 464),
                (self.jobs[2].operations[3], 464, 553),
            ]
        }
        # Create the schedule and test it.
        sched = Ziaee.create_schedule(self.operations)
        self.print_schedule(sched)
        self.check_schedule(sched, res_schedule, True)

    def test_ziaee_sub_schedule(self):
        res_schedule = {
            self.resources[0]: [
                (self.jobs[5].operations[2], 0, 118),
                (self.jobs[3].operations[1], 118, 247),
                (self.jobs[5].operations[4], 290, 373),
                (self.jobs[1].operations[4], 373, 462),
                (self.jobs[4].operations[4], 462, 515),
            ],
            self.resources[1]: [
                (self.jobs[2].operations[2], 0, 133),
                (self.jobs[0].operations[4], 133, 173),
                (self.jobs[5].operations[3], 173, 290),
                (self.jobs[2].operations[4], 329, 343),
            ],
            self.resources[2]: [
                (self.jobs[4].operations[2], 240, 320),
                (self.jobs[3].operations[2], 320, 464),
                (self.jobs[3].operations[3], 464, 597),
                (self.jobs[1].operations[6], 597, 695),
            ],
            self.resources[3]: [
                (self.jobs[1].operations[1], 0, 174),
                (self.jobs[1].operations[2], 174, 275),
                (self.jobs[1].operations[3], 275, 341),
                (self.jobs[4].operations[3], 341, 438),
                (self.jobs[1].operations[5], 462, 596),
            ],
            self.resources[4]: [
                (self.jobs[3].operations[0], 0, 93),
                (self.jobs[4].operations[1], 93, 240),
                (self.jobs[2].operations[3], 240, 329),
                (self.jobs[5].operations[5], 373, 447),
                (self.jobs[5].operations[6], 447, 528),
                (self.jobs[5].operations[7], 528, 612),
                (self.jobs[1].operations[7], 695, 810),
                (self.jobs[1].operations[8], 810, 891),
            ],
        }

        ops = []
        rand = random.Random(9034505)
        for job in self.jobs:
            num_ops = len(job.operations)
            start = rand.randint(0, num_ops)
            ops += job.operations[start:]
            print(job.name, start, num_ops)
        print("Chosen operations: %s" % [op.name for op in ops])
        rand.shuffle(ops)

        # Create the schedule and test it.
        sched = Ziaee.create_schedule(ops)
        self.print_schedule(sched)
        self.check_schedule(sched, res_schedule, True, True)

    def test_ziaee_mean_operation_processing_time(self):
        mean_proc_times = {
            "1-1": 83.0, "1-2": 106.0, "1-3": 74.0, "1-4": 134.0, "1-5": 40.0, "2-1": 88.0, "2-2": 121.0, "2-3": 93.66666666666667, "2-4": 93.25, "2-5": 123.0, "2-6": 88.75, "2-7": 98.0, "2-8": 94.5, "2-9": 107.0, "3-1": 102.0, "3-2": 108.5, "3-3": 112.0, "3-4": 88.0, "3-5": 49.0, "4-1": 93.0, "4-2": 108.66666666666667, "4-3": 130.5, "4-4": 132.0, "5-1": 49.25, "5-2": 144.0, "5-3": 80.0, "5-4": 96.5, "5-5": 65.0, "6-1": 69.33333333333333, "6-2": 111.33333333333333, "6-3": 65.33333333333333, "6-4": 110.5, "6-5": 103.5, "6-6": 86.5, "6-7": 110.0, "6-8": 70.5,
        }

        result = {}
        hclass = Ziaee.HelperClass(self.operations)

        for op in self.operations:
            result[op.name] = hclass.mean_operation_processing_time(op)
            self.assertAlmostEqual(mean_proc_times[op.name], result[op.name])
        self.assertEqual(len(mean_proc_times), len(result))

    def test_ziaee_total_mean_processing_time_of_job(self):
        mean_proc_times = {
            "1": 437.0,
            "2": 907.1666666666667,
            "3": 459.5,
            "4": 464.1666666666667,
            "5": 434.75,
            "6": 727.0
        }

        jobs = OrderedSet()
        for op in self.operations:
            jobs.add(op.job)
        jobs = list(jobs)

        result = {}
        hclass = Ziaee.HelperClass(self.operations)

        for job in jobs:
            result[job.name] = hclass.total_mean_processing_time_of_job(job)
            self.assertAlmostEqual(mean_proc_times[job.name], result[job.name])
        self.assertEqual(len(mean_proc_times), len(result))

    def test_sorted_jobs(self):
        job_order = ['5', '1', '3', '4', '6', '2']
        hclass = Ziaee.HelperClass(self.operations)
        i_sort = hclass.sorted_jobs()
        for result_job, test_job_name in zip(i_sort, job_order):
            self.assertEqual(result_job.name, test_job_name)
        self.assertEqual(len(job_order), len(i_sort))

    def test_ziaee_total_weighted_processing_time_on_machine(self):
        weighted_proc_time = {
            "4": 17.348267671971982,
            "3": 17.774263807261143,
            "2": 19.56381071964175,
            "5": 18.533751143815156,
            "1": 19.77990665730998
        }

        resources = OrderedSet()
        for op in self.operations:
            for res in op.resources:
                resources.add(res)

        result = {}
        hclass = Ziaee.HelperClass(self.operations)

        for res in resources:
            result[res.name] = hclass.total_weighted_processing_time_on_machine(res)
            self.assertAlmostEqual(weighted_proc_time[res.name], result[res.name])
        self.assertEqual(len(weighted_proc_time), len(result))

    def test_sorted_resources(self):
        res_order = ['4', '3', '5', '2', '1']
        hclass = Ziaee.HelperClass(self.operations)
        k_sort = hclass.sorted_resources()
        for result_res, test_res_name in zip(k_sort, res_order):
            self.assertEqual(result_res.name, test_res_name)
        self.assertEqual(len(res_order), len(k_sort))


if __name__ == '__main__':
    unittest.main()
