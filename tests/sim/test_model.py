# -*- coding: utf-8 -*-

from jobshop.sim.data_generator import create_jobs_and_resources
from jobshop.sim.model import StatePool, Operation, Job, OperationState, StateError, JobState, ScheduleTuple, Schedule, Resource

from operator import attrgetter
import unittest
from nose.tools import raises


def create_job(name, num_ops=11):
    job = Job(str(name))
    ops = [Operation(i) for i in range(0, 10 * num_ops, 10)]
    job.operations = ops
    return job


def create_jobs(num_jobs):
    jobs = [create_job("Job%d" % i) for i in range(num_jobs)]
    return jobs


class Test_StatePool(unittest.TestCase):
    """Tests the `StatePool` class of the `jobshop.sim.model` module"""

    def test_addJob(self):
        j1 = create_job("Job1")

        sp = StatePool()
        sp.add_job(j1)

        self.assertEqual(set(j1.operations), sp._opsPending)
        for op in j1.operations:
            self.assertEqual(sp.operation_state(op), OperationState.Pending)
            self.assertIn(op, sp._opsPending)
            self.assertIn(op, sp._opsAll)

    def test_addOperations(self):
        job = create_job("Job1")
        sp = StatePool()
        sp.add_operations(job.operations)

        self.assertEqual(set(job.operations), sp._opsPending)
        for op in job.operations:
            self.assertEqual(sp.operation_state(op), OperationState.Pending)
            self.assertIn(op, sp._opsPending)
            self.assertIn(op, sp._opsAll)
        self.assertCountEqual(sp.operations(), job.operations)

    def test_addOperationReAdding(self):
        # Add the operations of a job
        job = create_job("Job1")
        sp = StatePool()
        sp.add_operations(job.operations)

        # As long as the operation is in state pending, this should not fail.
        op1 = job.operations[0]
        sp.add_operation(op1)

    def test_pending_list(self):
        jobs, _ = create_jobs_and_resources(num_jobs=1)
        job = jobs[0]
        sp = StatePool()
        sp.add_job(job)
        self.assertEqual(job.operations, sp.pending_operations())

    def test_queued_list(self):
        jobs, _ = create_jobs_and_resources(num_jobs=1)
        job = jobs[0]
        sp = StatePool()
        sp.add_job(job)
        for op in job.operations:
            sp.operation_queue(op)
        self.assertEqual(job.operations, sp.queued_operations())

    def test_interrupted_list(self):
        jobs, _ = create_jobs_and_resources(num_jobs=1)
        job = jobs[0]
        sp = StatePool()
        sp.add_job(job)
        for op in job.operations:
            sp.operation_queue(op)
            sp.operation_interrupt(op)
        self.assertEqual(job.operations, sp.interrupted_operations())

    def test_running_list(self):
        jobs, _ = create_jobs_and_resources(num_jobs=1)
        job = jobs[0]
        sp = StatePool()
        sp.add_job(job)
        for op in job.operations:
            sp.operation_queue(op)
            sp.operation_interrupt(op)
            sp.operation_queue(op)
            sp.operation_run(op, "res1")
        self.assertEqual(job.operations, sp.running_operations())

    def test_done_list(self):
        jobs, _ = create_jobs_and_resources(num_jobs=1)
        job = jobs[0]
        sp = StatePool()
        sp.add_job(job)
        for op in job.operations:
            sp.operation_queue(op)
            sp.operation_interrupt(op)
            sp.operation_queue(op)
            sp.operation_run(op, "res1")
            sp.operation_done(op)
        self.assertEqual(job.operations, sp.done_operations())

    @raises(StateError)
    def test_addOperationsFail(self):
        # Add the operations of a job
        job = create_job("Job1")
        sp = StatePool()
        sp.add_operations(job.operations)

        # Set the status of all operations to Queued and try to add the
        # operation op again which should raise an StateError
        for op in job.operations:
            sp.operation_queue(op)

        op1 = job.operations[0]
        sp.add_operation(op1)

    @raises(StateError)
    def test_stateQueueFail(self):
        job = create_job("Job1", 1)
        sp = StatePool()
        op = job.operations[0]
        sp.operation_queue(op)

    @raises(StateError)
    def test_stateRunFail(self):
        job = create_job("Job1", 1)
        sp = StatePool()
        sp.add_job(job)
        op = job.operations[0]

        # raises an StateError because it was not queued.
        sp.operation_run(op, "res1")

    @raises(StateError)
    def test_stateDoneFail(self):
        job = create_job("Job1", 1)
        sp = StatePool()
        sp.add_job(job)
        op = job.operations[0]
        sp.operation_queue(op)
        # raises an StateError because it was not set to running.
        sp.operation_done(op)

    @raises(StateError)
    def test_stateInterruptFail(self):
        job = create_job("Job1", 1)
        sp = StatePool()
        sp.add_job(job)
        op = job.operations[0]
        sp.operation_queue(op)
        sp.operation_run(op, Resource("resource2"))
        sp.operation_interrupt(op)  # Only queued operations can be interrupted.

    def test_OperationResource(self):
        job = create_job("Job1")
        sp = StatePool()
        sp.add_job(job)
        op = job.operations[0]
        sp.operation_queue(op)
        sp.operation_interrupt(op)
        sp.operation_queue(op)

        res2 = Resource("resource2")
        sp.operation_run(op, res2)
        res = sp.operation_resource(op)
        self.assertEqual(res, res2)

    @raises(StateError)
    def test_OperationResourceFail(self):
        job = create_job("Job1")
        sp = StatePool()
        sp.add_job(job)
        op = job.operations[0]
        sp.operation_queue(op)
        # raises an StateError, because the operation is not running
        sp.operation_resource(op)

    def test_through_all_states(self):
        job = create_job("Job1")
        sp = StatePool()
        sp.add_job(job)
        op = job.operations[0]
        sp.operation_queue(op)
        sp.operation_interrupt(op)
        sp.operation_queue(op)
        sp.operation_run(op, Resource("resource2"))
        sp.operation_done(op)

        self.assertEqual(sp.operation_state(op), OperationState.Done)
        for op in job.operations[1:]:
            self.assertEqual(sp.operation_state(op), OperationState.Pending)

    def test_through_all_states_enum(self):
        job = create_job("Job1")
        sp = StatePool()
        for op in job.operations:
            sp.set_operation_state(op, OperationState.Pending)
            sp.set_operation_state(op, OperationState.Queued)
            sp.set_operation_state(op, OperationState.Interrupted)
            sp.set_operation_state(op, OperationState.Queued)
            sp.set_operation_state(op, OperationState.Running, Resource("resource1"))
            sp.set_operation_state(op, OperationState.Done)
            self.assertEqual(sp.operation_state(op), OperationState.Done)

    def test_multiple_jobs(self):
        j1, j2 = create_jobs(2)

        sp = StatePool()
        sp.add_job(j2)
        sp.add_job(j1)
        for op in j2.operations:
            sp.operation_queue(op)
            self.assertEqual(sp.operation_state(op), OperationState.Queued)

        resIn2 = Resource("resource2")
        for op in j1.operations:
            self.assertEqual(sp.operation_state(op), OperationState.Pending)
            sp.operation_queue(op)
            sp.operation_run(op, resIn2)

        resIn1 = Resource("res1")
        for op in j2.operations:
            sp.operation_run(op, resIn1)

        for op in j1.operations:
            res = sp.operation_resource(op)
            self.assertEqual(res, resIn2)
            self.assertEqual(sp.operation_state(op), OperationState.Running)
            sp.operation_done(op)

        for op in j2.operations:
            res = sp.operation_resource(op)
            self.assertEqual(res, resIn1)
            self.assertEqual(sp.operation_state(op), OperationState.Running)
            sp.operation_done(op)

        for op in j1.operations + j2.operations:
            self.assertEqual(sp.operation_state(op), OperationState.Done)

    def test_job_state(self):
        job = create_job("Job2")
        sp = StatePool()
        sp.add_job(job)
        op1 = job.operations[0]
        self.assertEqual(sp.job_state(job), JobState.Pending)

        sp.operation_queue(op1)
        self.assertEqual(sp.job_state(job), JobState.Queued)
        sp.operation_interrupt(op1)
        self.assertEqual(sp.job_state(job), JobState.Pending)
        sp.operation_queue(op1)
        self.assertEqual(sp.job_state(job), JobState.Queued)

        sp.operation_run(op1, Resource("res2"))
        self.assertEqual(sp.job_state(job), JobState.Processing)
        sp.operation_done(op1)
        self.assertEqual(sp.job_state(job), JobState.Processing)

        remOps = job.operations[1:]
        for op in remOps:
            sp.operation_queue(op)
            sp.operation_run(op, Resource('res1'))
            sp.operation_done(op)
        self.assertEqual(sp.job_state(job), JobState.Done)


class Test_Operation(unittest.TestCase):
    """Tests the `Operation` class of the `jobshop.sim.model` module"""

    def test_sequence_number(self):
        op = Operation("432")
        self.assertEqual(op.sequence_number, 432)

    def test_resources(self):
        op = Operation(0)
        rescost = {
            Resource(1): 44,
            Resource(4): 144,
            Resource(6): 74,
            Resource(9): 89
        }
        for res, cost in rescost.items():
            op.add_resource(res, cost)
        self.assertIsInstance(op.resources, list)  # drop if sequence is sufficient
        self.assertCountEqual(list(rescost.keys()), op.resources)

    def test_fastest_resource(self):
        op = Operation(0)
        res1 = Resource(1)

        op.add_resource(res1, 44)
        op.add_resource(Resource(4), 44)
        op.add_resource(Resource(9), 156)
        op.add_resource(Resource(6), 89)

        fres = op.fastest_resource()
        self.assertEqual(fres, res1)

    @raises(ValueError)
    def test_sequence_number_fail(self):
        Operation(sequence_number="12A")

    def test_resource_processing_time(self):
        dummy = object()
        res1 = Resource("res1")
        res2 = Resource("res2")
        resource_cost = {res1: 10.0, res2: 4, dummy: "23"}
        op = Operation(0, resource_cost=resource_cost)
        self.assertIsInstance(op.processing_time(res1), int)
        self.assertEqual(op.processing_time(res1), 10)
        self.assertEqual(op.processing_time(res2), 4)
        self.assertEqual(op.processing_time(dummy), 23)

    @raises(KeyError)
    def test_resource_processing_time_fail(self):
        op = Operation(0)
        op.processing_time(Resource("res1"))

    @raises(ValueError)
    def test_resource_cost_fail2(self):
        resource_cost = {Resource('res1'): "Abc"}
        Operation(0, resource_cost=resource_cost)

    def test_name(self):
        j = Job("234712")
        op = Operation(20, job=j)
        self.assertEqual(op.name, "234712-20")
        self.assertEqual(str(op), "234712-20")
        op.job = Job("1")
        self.assertEqual(op.name, "1-20")

    def test_repr(self):
        op = Operation(40, Job("123"), description="Do this, than that!")
        self.assertEqual(repr(op), 'Operation(40, Job("123"), OrderedDict(), "Do this, than that!")')

    def test_job(self):
        j = Job("1")
        op = Operation(0, j)
        self.assertEqual(j, op.job)

    def test_missing_job(self):
        op = Operation(20)
        self.assertEqual(op.name, None)
        op.job = Job("123")
        self.assertEqual(op.name, "123-20")

    def test_predecessor(self):
        job = create_job("Job1", 4)
        op1, op2, op3, op4 = job.operations
        self.assertEqual(op1.predecessor(), None)
        self.assertEqual(op2.predecessor(), op1)
        self.assertEqual(op3.predecessor(), op2)
        self.assertEqual(op4.predecessor(), op3)

    def test_successor(self):
        job = create_job("Job1", 3)
        op1, op2, op3 = job.operations
        self.assertTrue(op1.is_successor(op2))
        self.assertTrue(op1.is_successor(op3))
        self.assertFalse(op2.is_successor(op1))
        self.assertTrue(op2.is_successor(op3))
        self.assertFalse(op3.is_successor(op1))
        self.assertFalse(op3.is_successor(op2))

        self.assertTrue(op1.has_successor())
        self.assertTrue(op2.has_successor())
        self.assertFalse(op3.has_successor())

    @raises(RuntimeError)
    def test_predecessor_fail(self):
        op = Operation(0)
        op.predecessor()


class Test_Job(unittest.TestCase):
    """Tests the `Job` class of the `jobshop.sim.model` module"""
    def test_job1(self):
        job = Job("1")
        ops = [Operation(i) for i in reversed(range(10))]
        job.operations = ops

        self.assertEqual(job.name, "1")

        minOp = min(job.operations, key=attrgetter('sequence_number'))
        prev = minOp.sequence_number
        for num in job.operation_sequence_numbers():
            self.assertLessEqual(prev, num)
            prev = num

    def test_job2(self):
        job = Job("2", description="Job 2 Desc")

        for i in reversed(range(-10, 100, 10)):
            op = Operation(i)
            job.add_operation(op)

        self.assertEqual(job.name, "2")

        minOp = min(job.operations, key=attrgetter('sequence_number'))
        prev = minOp.sequence_number
        for num in job.operation_sequence_numbers():
            self.assertLessEqual(prev, num)
            prev = num

    def test_job3(self):
        ops = [Operation(i) for i in reversed(range(10))]
        job = Job("3", description="My Job", operations=ops)

        self.assertEqual(job.name, "3")

        minOp = min(job.operations, key=attrgetter('sequence_number'))
        prev = minOp.sequence_number
        for num in job.operation_sequence_numbers():
            self.assertLessEqual(prev, num)
            prev = num

    def test_str_repr(self):
        job = Job("3", "test :)")
        self.assertEqual(repr(job), 'Job("3", "test :)")')
        self.assertEqual(str(job), "3")


class Test_Resource(unittest.TestCase):
    """Tests the `Resource` class of the `jobshop.sim.model` module"""
    def test_basic1(self):
        r = Resource("1234", "This is a description")
        self.assertEqual(r.name, "1234")
        self.assertEqual(r.description, "This is a description")

    def test_basic2(self):
        r = Resource(12345, performance_mean=0.9, performance_stddev=0.341)
        self.assertEqual(r.name, "12345")
        self.assertEqual(r.description, "")
        self.assertAlmostEqual(r.performance_mean, 0.9)
        self.assertAlmostEqual(r.performance_stddev, 0.341)

    def test_performance_mean_none(self):
        r = Resource(1)
        self.assertAlmostEqual(r.performance_mean, None)

    def test_performance_stddev_none(self):
        r = Resource(1)
        self.assertAlmostEqual(r.performance_stddev, None)

    @raises(AttributeError)
    def test_set_fail(self):
        r = Resource("1234")
        r.name = "ThisIsNotAllowed"

    @raises(AttributeError)
    def test_set_fail2(self):
        r = Resource("1234")
        r.description = "ThisIsNotAllowed"

    def test_hash1(self):
        r1 = Resource("1234", "Teest :)")
        r2 = Resource(1234, "Teest :)")
        self.assertEqual(hash(r1), hash(r2))

    def test_hash2(self):
        r1 = Resource("1234", "Teest :)")
        r2 = Resource(1234, "Teest :)")
        d = {}
        d[r1] = 42
        self.assertEqual(d[r2], 42)

    def test_hash3(self):
        r1 = Resource("1234", "Teest :)")
        r2 = Resource(1234, "Teest :)")
        rlist = [r1]
        self.assertIn(r2, rlist)

    def test_equal(self):
        r1 = Resource("1234", "Teest :)")
        r2 = Resource(1234, "Teest :)")
        self.assertEqual(r1, r2)
        self.assertEqual(r2, r1)
        self.assertFalse(r2 != r1)
        self.assertFalse(r1 != r2)

    def test_not_equal(self):
        r1 = Resource("1234", "Teest :)")
        r2 = Resource(1234, "Teest1 :)")
        r3 = Resource("12345", "Teest :)")
        self.assertNotEqual(r1, r2)
        self.assertNotEqual(r2, r1)
        self.assertNotEqual(r1, r3)
        self.assertNotEqual(r3, r1)


class Test_ScheduleTuple(unittest.TestCase):
    """Tests the `ScheduleTuple` class of the `jobshop.sim.model` module"""
    @raises(TypeError)
    def test_ScheduleTupleFail(self):
        ScheduleTuple()

    def test_ScheduleTuple(self):
        op = Operation(0)
        s1 = ScheduleTuple(op=op, start=None, finish=None)
        self.assertEqual(s1.op, op)
        self.assertEqual(s1.start, None)
        self.assertEqual(s1.finish, None)

        s2 = ScheduleTuple(op=op, start=2.0, finish=4.0)
        self.assertEqual(s2.op, op)
        self.assertAlmostEqual(s2.start, 2.0)
        self.assertAlmostEqual(s2.finish, 4.0)


class Test_Schedule(unittest.TestCase):
    """Tests the `Schedule` class of the `jobshop.sim.model` module"""
    def test_Schedule(self):
        s = Schedule()
        j1, j2 = create_jobs(2)

        for i, op in enumerate(j1.operations):
            s.append(op, "res%d" % (i % 3), 2.0, 42)

        for i, op in enumerate(j2.operations):
            s.append(op, "res%d" % (i % 2))

        for res in s.resources:
            tuples = s.schedule_tuples(res)
            for stuple in tuples:
                self.assertEqual(stuple.op.__class__, Operation)
                if stuple.op.job == j2:
                    self.assertEqual(stuple.start, None)
                    self.assertEqual(stuple.start, None)
                    self.assertIn(res, ["res0", "res1"])
                elif stuple.op.job == j1:
                    self.assertAlmostEqual(stuple.start, 2.0)
                    self.assertEqual(stuple.finish, 42)
                    self.assertIn(res, ["res0", "res1", "res2"])

    def test_ScheduleOrder(self):
        s = Schedule()
        j1 = create_job("Job1", num_ops=31)

        resources = {"res0": 0, "res1": 0, "res2": 0, "res3": 0}
        res_objs = {"res0": Resource("res0"), "res1": Resource("res1"), "res2": Resource("res2"), "res3": Resource("res3")}
        for i, op in enumerate(j1.operations):
            res = "res%d" % (i % 4)
            resCount = resources[res]
            s.append(op, res_objs[res], start=resCount, finish=resCount + 13.5)
            resources[res] += 15

        import pprint
        pprint.pprint(s.schedule)
        for res, ops in s.schedule.items():
            for stuple in ops:
                self.assertLess(stuple.start, stuple.finish)
                self.assertAlmostEqual(stuple.finish - stuple.start, 13.5)

            for i in range(len(ops) - 1):
                self.assertLessEqual(ops[i].finish, ops[i + 1].start)

    def test_Resource(self):
        r = Resource("12345", description="This is a test")
        self.assertEqual("12345", str(r))
        self.assertEqual('Resource("12345", "This is a test")', repr(r))
        self.assertEqual(r.description, "This is a test")


if __name__ == '__main__':
    unittest.main()
