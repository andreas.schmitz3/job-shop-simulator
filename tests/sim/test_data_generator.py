# -*- coding: utf-8 -*-

from jobshop.sim.data_generator import create_jobs_and_resources, create_simulation_result_metadata, load_distribution

import unittest
import os.path
from pprint import pprint
from nose.tools import raises
import pkg_resources
# import pdb; pdb.set_trace()


# TODO: Complete tests
class Test_DataGenerator(unittest.TestCase):
    """Tests the `SimulationEnvironment` class of the `jobshop.sim.environment`
    module
    """
    def setUp(self):
        self.jobs, self.resources = create_jobs_and_resources(
            seed=5235,
            num_resources=5,
            num_jobs=6,
            min_ops=4,
            max_ops=10,
            op_min_res=1,
            op_max_res=4
        )

    def test_basic(self):
        res_file = pkg_resources.resource_filename('jobshop', "share/distributions/resource_performance.csv")
        res_perf = load_distribution(res_file)
        print("Resource Performance")
        pprint(res_perf)
        proctime_file = pkg_resources.resource_filename('jobshop', "share/distributions/processing_times.csv")
        proctime = load_distribution(proctime_file)
        print("Processing Times")
        pprint(proctime)
        numops_file = pkg_resources.resource_filename('jobshop', "share/distributions/number_operations.csv")
        numops = load_distribution(numops_file)
        print("Number of Operations per Job")
        pprint(numops)

        jobs, resources = create_jobs_and_resources(
            seed=None,
            num_resources=5,
            num_jobs=16,
            resource_performance_distr=res_perf,
            processing_time_distr=proctime,
            num_operations_distr=numops
        )
        for job in jobs:
            for op in job.operations:
                print("%s" % op.name)
                for res in op.resources:
                    print("    Resource: %s time: %s" % (res, op.processing_time(res)))
        for res in resources:
            print("Resource: %s, mean: %s, stddev: %s" % (res.name, res.performance_mean, res.performance_stddev))


if __name__ == '__main__':
    unittest.main()
