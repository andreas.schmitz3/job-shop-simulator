# -*- coding: utf-8 -*-

from jobshop.sim.deviation import Deviation
from jobshop.sim.model import Resource


import random
import unittest
from nose.tools import raises
from pprint import pprint
from collections import OrderedDict


class Test_DeviationRandom(unittest.TestCase):
    """Tests the `Deviation` class of the `jobshop.sim.deviation` module.
    """
    def setUp(self):
        self.deviation = Deviation()

    def test_resource_performance(self):
        resources = [Resource(1, "", 0.562, 0.123), Resource(1, "", 0.862, 0.234), Resource(3, "", 0.432, 0.167)]
        for res in resources:
            self.deviation.add_resource(resource=res, time=0)
        timeslots = [0, 240, 480]
        for r, t in zip(resources, timeslots):
            delay = self.deviation.resource_performance(r, t)
            self.assertIsInstance(delay, float)


class Test_Deviation(unittest.TestCase):
    """Tests the `Deviation` class of the `jobshop.sim.deviation` module.
    """
    def setUp(self):
        self.deviation = Deviation(rand=random.Random(77466))

    def test_resource_performance_simple(self):
        resources = [Resource(1, "", 0.562, 0.123), Resource(2, "", 0.862, 0.234), Resource(1, "", 0.432, 0.167)]
        for res in resources:
            self.deviation.add_resource(resource=res, time=0)

        timeslots = [0, 240, 480]
        values = [0.5565841139125295, 0.9021849264840597, 0.5587405186825016]
        for r, t, v in zip(resources, timeslots, values):
            delay = self.deviation.resource_performance(r, t)
            self.assertEqual(delay, v)

    def test_resource_performance_repetition(self):
        r1 = Resource(1, "Welding", 0.654, 0.143)
        self.deviation.add_resource(r1, 0)
        f1 = self.deviation.resource_performance(r1, 0)
        f2 = self.deviation.resource_performance(r1, 120)
        f3 = self.deviation.resource_performance(r1, 240)
        test_value = 0.6477034820283879
        self.assertAlmostEqual(f1, test_value)
        self.assertAlmostEqual(f2, test_value)
        self.assertAlmostEqual(f3, test_value)

        r2 = Resource(2, "Bolting", 0.613, 0.203)
        self.deviation.add_resource(r2, 480)
        f4 = self.deviation.resource_performance(r1, 480)
        f5 = self.deviation.resource_performance(r2, 482)
        self.assertAlmostEqual(f4, 0.6785574550735921)
        self.assertAlmostEqual(f5, 0.607620530833722)

    def test_resource_performance_no_deviation(self):
        r1 = Resource(1, "Welding", 0.654, 0.143)
        self.deviation.add_resource(r1, 0)
        factor = self.deviation.resource_performance(r1, 120)
        self.deviation.no_deviations = True
        self.assertAlmostEqual(factor, 0.6477034820283879)

    @raises(ValueError)
    def test_resource_performance_val_error1(self):
        r1 = Resource(1, "Welding", 0.654, 0.143)
        r2 = Resource(2, "Bolting", 0.613, 0.203)
        self.deviation.add_resource(r1, 0)
        self.deviation.add_resource(r2, 480)
        # time precedes registration time
        self.deviation.resource_performance(r2, 479)

    @raises(ValueError)
    def test_resource_performance_val_error2(self):
        r1 = Resource(1, "Welding", 0.654, 0.143)
        r2 = Resource(2, "Bolting", 0.613, 0.203)
        self.deviation.add_resource(r1, 0)
        # resource 2 not registered
        self.deviation.resource_performance(r2, 479)

    def test_resource_performance_complex(self):
        job_selector = {
            0: [
                Resource(1, "", 0.562, 0.123),
                Resource(2, "", 0.862, 0.234),
                Resource(3, "", 0.432, 0.167),
                Resource(4, "", 0.632, 0.014)
            ],
            1800: [
                Resource(5, "", 0.854, 0.075),
                Resource(6, "", 0.756, 0.374)
            ]
        }
        for t, resources in job_selector.items():
            for res in resources:
                self.deviation.add_resource(res, t)
        pprint(self.deviation._cache)
        pprint(self.deviation._current_interval)
        test_data = OrderedDict([
            (Resource("1"), {
                0: 0.5565841139125295,
                1: 0.6627342356525048,
                2: 0.7298859968240873,
                3: 0.4788579624378893
            }),
            (Resource("2"), {
                0: 0.9021849264840597,
                1: 0.7922373602492906,
                2: 0.7345726784069874,
                3: 1.0290228664844634
            }),
            (Resource("3"), {
                0: 0.4275745253656728,
                1: 0.27376542940386017,
                2: 0.49866829436703736,
                3: 0.28840524404724244
            }),
            (Resource("4"), {
                0: 0.6362021826690426,
                1: 0.6432591464215612,
                2: 0.6252936754499512,
                3: 0.6154042101371536
            }),
            (Resource("5"), {3: 0.8082399401035093}),
            (Resource("6"), {3: 0.8045374630094081})
        ])
        self.assertEqual(self.deviation._cache, test_data)
        self.assertEqual(self.deviation._current_interval, 3)

        # Extend test
        self.deviation.resource_performance(Resource("1"), 2100)
        test_data[Resource('1')][4] = 0.6337480605041667
        test_data[Resource('2')][4] = 1.094254105897903
        test_data[Resource('3')][4] = 0.36494406850331723
        test_data[Resource('4')][4] = 0.6108170925828041
        test_data[Resource('5')][4] = 0.9714383209870976
        test_data[Resource('6')][4] = 0.29520229657605473
        self.assertEqual(self.deviation._cache, test_data)
        self.assertEqual(self.deviation._current_interval, 4)
        pprint(self.deviation._cache)


class Test_NoDeviation(unittest.TestCase):
    """Tests the `Deviation` class of the `jobshop.sim.deviation` module.
    """
    def setUp(self):
        self.deviation = Deviation(no_deviations=True)

    def test_resource_performance_no_deviation(self):
        r1 = Resource(1, "Welding", 0.654, 0.143)
        self.deviation.add_resource(r1, 0)
        factor = self.deviation.resource_performance(r1, 240)
        self.assertAlmostEqual(factor, 1.0)


if __name__ == '__main__':
    unittest.main()
