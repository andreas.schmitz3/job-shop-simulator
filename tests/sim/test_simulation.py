# -*- coding: utf-8 -*-

from jobshop.sim.environment import SimulationEnvironment
from jobshop.sim.data_generator import create_jobs_and_resources

import unittest
from nose.tools import raises
# import pdb; pdb.set_trace()


class Test_SimulationEnvironment(unittest.TestCase):
    """Tests the `SimulationEnvironment` class of the `jobshop.sim.environment`
    module
    """
    def setUp(self):
        self.jobs, self.resources = create_jobs_and_resources(
            seed=5235,
            num_resources=5,
            num_jobs=6,
            min_ops=4,
            max_ops=10,
            op_min_res=1,
            op_max_res=4
        )

    def test_configuration(self):
        sim = SimulationEnvironment(**{'seed': 5296})
        self.assertEqual(sim.seed, 5296)

    def test_configuration_default(self):
        sim = SimulationEnvironment()
        print(sim.seed)
        self.assertIsNotNone(sim.seed)
        self.assertIsInstance(sim.seed, int)
        self.assertEqual(sim.shop_id, 1)

    @raises(RuntimeError)
    def test_no_scheduler_executor_fail1(self):
        sim = SimulationEnvironment()
        sim.run()

    @raises(ValueError)
    def test_wrong_configuration_1(self):
        js = {
            "strategy": 4
        }
        sim = SimulationEnvironment(job_selector=js, database_file=":memory:")
        sim.add_simulation('FirstInFirstOut', 'NoCommunication')
        sim.run()

    def test_basic_simulation(self):
        sim = SimulationEnvironment(database_file=":memory:")
        # TODO: Create a shop and run a basic simulation test
        sim.add_simulation('FirstInFirstOut', 'NoCommunication')
        # sim.run()


if __name__ == '__main__':
    unittest.main()
