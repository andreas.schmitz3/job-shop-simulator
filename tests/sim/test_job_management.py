# -*- coding: utf-8 -*-
"""
"""
from jobshop.sim.job_management import JobSelector, JobDistributor
from jobshop.common.database import CommonDatabase
from jobshop.sim.data_generator import create_jobs_and_resources

import unittest
import random
import os.path
import os
from collections import OrderedDict
from nose.tools import raises


class Test_JobSelector(unittest.TestCase):
    """Tests the `~jobshop.sim.job_management.JobSelector` class of the
    `~jobshop.sim.job_management` module
    """
    def setUp(self):
        self.jd = CommonDatabase()
        jobs, resources = create_jobs_and_resources(seed=675622, num_resources=5, num_jobs=10, min_ops=4, max_ops=10, op_min_res=2, op_max_res=5)
        self.shop_id = self.jd.create_shop(jobs, resources)
        print(jobs)
        print(self.jd.all_job_names(shop_id=self.shop_id))

    # general tests
    @raises(ValueError)
    def test_strategy_fail(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        js.job_names("NotAvailable")

    # -------------------------------------------------------------------------
    # STRATEGY 0 - Mirror
    # -------------------------------------------------------------------------
    def test_strategy_mirror(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        innames = ["1", "2", "10"]
        names = js.job_names(0, innames)
        self.assertEqual(innames, names)

    @raises(ValueError)
    def test_strategy_mirror_not_available_fail(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        # 11 does not occur in the list of jobs.
        # Mirroring it will raise an exception.
        innames = ["1", "11", "2"]
        js.job_names(0, innames)

    @raises(ValueError)
    def test_strategy_mirror_empty_list_fail(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        js.job_names(0, None)

    # -------------------------------------------------------------------------
    # STRATEGY 1 - Absolute number of Jobs
    # -------------------------------------------------------------------------
    # Absolute Jobs
    def test_strategy_absolute_5(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        innames = ['1', '2', '3', '4', '5']
        names = js.job_names(1, 5)
        self.assertEqual(innames, names)

    def test_strategy_absolute_random(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id, rand=random.Random(23))
        innames = ['8', '9', '6', '7', '4', '3', '10', '1', '2', '5']
        names = js.job_names(1, absolute=10, random=True)
        self.assertEqual(innames, names)

    def test_strategy_absolute_more_than_available(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id, rand=random.Random(42))
        innames = ['8', '4', '3', '9', '6', '7', '10', '5', '1', '2']
        # If the specified absolute value exceeds the number of available
        # jobs, simply the whole list is returned instead of throwing an error.
        names = js.job_names(1, absolute=20, random=True)
        self.assertEqual(innames, names)

    @raises(ValueError)
    def test_strategy_absolute_nan_fail(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        js.job_names(1, absolute="NotAnInt")

    @raises(ValueError)
    def test_strategy_absolute_negative_fail(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        js.job_names(1, absolute=-10)

    # -------------------------------------------------------------------------
    # STRATEGY 2 - Percentage of all jobs
    # -------------------------------------------------------------------------
    def test_strategy_percentage(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        innames = ['1', '2', '3']
        names = js.job_names(2, 0.3)
        self.assertEqual(innames, names)

    def test_strategy_percentage_random(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id, rand=random.Random(23))
        innames = ['8', '9', '6', '7', '4']
        names = js.job_names(2, percentage=0.56, random=True)
        self.assertEqual(innames, names)

    def test_strategy_percentage_100(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id, rand=random.Random(23))
        innames = ['8', '9', '6', '7', '4', '3', '10', '1', '2', '5']
        names = js.job_names(2, percentage=1.0, random=True)
        self.assertEqual(innames, names)

    @raises(ValueError)
    def test_strategy_percentage_fail_not_float(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        js.job_names(2, percentage="NotFloat")

    @raises(ValueError)
    def test_strategy_percentage_fail_less_0(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        js.job_names(2, percentage=-0.0001)

    @raises(ValueError)
    def test_strategy_percentage_fail_greater_1(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        js.job_names(2, percentage=1.001)

    @raises(ValueError)
    def test_strategy_percentage_empty_result(self):
        js = JobSelector(database=self.jd, shop_id=self.shop_id)
        js.job_names(2, percentage=0.0001)


class Test_JobDistributor(unittest.TestCase):
    """Tests the `~jobshop.sim.job_management.JobDistributor` class of the
    `~jobshop.sim.job_management` module
    """
    def setUp(self):
        self.database = CommonDatabase()
        jobs, resources = create_jobs_and_resources(seed=675622, num_resources=5, num_jobs=10, min_ops=4, max_ops=10, op_min_res=2, op_max_res=5)
        self.shop_id = self.database.create_shop(jobs, resources)
        js = JobSelector(self.database, self.shop_id)
        self.names = js.job_names(1, absolute=20)

    def string_equal(self, target, ret):
        result = "%s == %s " % (str(target), str(ret))
        if (str(target) == str(ret)):
            result += "✔"
        else:
            result += "❌"
        return result

    def check_data_for_timeslot(self, jd, target):
        for t in jd.timeslots():
            jobs, resources = jd.new_data_for_timeslot(t)
            print("Time: %d" % t)
            print("   Jobs: %s", jobs)
            print("   Resources: %s", resources)
            for i, job in enumerate(jobs):
                print("Job: %s" % self.string_equal(target[t]['jobs'][i], job))
                self.assertEqual(target[t]['jobs'][i], job.name)
            for i, res in enumerate(resources):
                print("Resource: %s" % self.string_equal(target[t]['resources'][i], res))
                self.assertEqual(target[t]['resources'][i], res.name)

    # -------------------------------------------------------------------------
    # Time slice Strategy Tests
    # -------------------------------------------------------------------------
    def test_time_slice_strat0(self):
        ts_config = {"duration": 240}
        dist_config = {"chunk_size": 2}
        jd = JobDistributor(self.database, self.shop_id, self.names, time_slice=0, time_slice_config=ts_config, distribution=1, distribution_config=dist_config)

        target = [0, 240, 480, 720, 960]
        res = []
        for t in jd.timeslots():
            res.append(t)
        self.assertEqual(res, target)

    def test_time_slice_strat1(self):
        ts_config = {"min_days": 1, "max_days": 4}
        dist_config = {"chunk_size": 2}
        jd = JobDistributor(self.database, self.shop_id, self.names, time_slice=1, time_slice_config=ts_config, distribution=1, distribution_config=dist_config, rand=random.Random(42))

        target = [0, 480, 960, 2400, 3360]
        res = []
        for t in jd.timeslots():
            res.append(t)
        self.assertEqual(res, target)

    def test_time_slice_strat1_randomseed(self):
        ts_config = {"min_days": 1, "max_days": 4}
        dist_config = {"chunk_size": 1}
        rand_seed = hash(os.urandom(128))
        r = random.Random(rand_seed)
        jd = JobDistributor(self.database, self.shop_id, self.names, time_slice=1, time_slice_config=ts_config, distribution=1, distribution_config=dist_config, rand=r)

        res = []
        prev = -1
        for t in jd.timeslots():
            res.append(t)
            self.assertLess(prev, t, "Random test failed. To reproduce the results, use seed: %s. Current list: %s" % (rand_seed, res))
            prev = t

    @raises(ValueError)
    def test_time_strategy_fixed_argument_missing_fail(self):
        JobDistributor(self.database, self.shop_id, self.names, time_slice=0, distribution=0)
        # Duration argument missing

    @raises(ValueError)
    def test_time_strategy_fixed_argument_invalid_fail(self):
        ts_config = {"duration": "NaN"}
        JobDistributor(self.database, self.shop_id, self.names, time_slice=0, distribution=0, time_slice_config=ts_config)
        # Duration argument missing

    @raises(ValueError)
    def test_time_strategy_interval_argument_missing_fail(self):
        JobDistributor(self.database, self.shop_id, self.names, time_slice=1, distribution=0)
        # configuration for time slice strategy 1 missing (min_days, max_days)

    @raises(ValueError)
    def test_time_strategy_interval_argument_invalid_fail(self):
        ts_config = {"min_days": -1, "max_days": 10}
        JobDistributor(self.database, self.shop_id, self.names, time_slice=1, distribution=0, time_slice_config=ts_config)
        # min_days and max_days must be greater than 0.

    @raises(ValueError)
    def test_time_strategy_interval_argument_invalid_fail2(self):
        ts_config = {"min_days": "NaN", "max_days": 10}
        JobDistributor(self.database, self.shop_id, self.names, time_slice=1, distribution=0, time_slice_config=ts_config)
        # min_days and max_days must be integer convertible.

    @raises(ValueError)
    def test_time_strategy_interval_argument_invalid_fail3(self):
        ts_config = {"min_days": 10, "max_days": 5}
        JobDistributor(self.database, self.shop_id, self.names, time_slice=1, distribution=0, time_slice_config=ts_config)
        # max_days must be greater than or equal to min_days

    # -------------------------------------------------------------------------
    # Distribution Strategy Tests
    # -------------------------------------------------------------------------
    def test_distribution_strategy_all_at_once(self):
        rand = random.Random(3583941)
        rand_names = list(self.names)
        rand.shuffle(rand_names)
        ts_config = {"duration": 240}
        jd = JobDistributor(self.database, self.shop_id, rand_names, time_slice=0, time_slice_config=ts_config, distribution=0)

        target = {}
        target[0] = {
            "jobs": ['2', '5', '1', '9', '7', '4', '8', '10', '3', '6'],
            "resources": ['3', '5', '1', '2', '4']
        }
        self.check_data_for_timeslot(jd, target)

    def test_distribution_chunk_wise(self):
        ts_config = {"duration": 240}
        dist_config = {"chunk_size": 2}

        jd = JobDistributor(self.database, self.shop_id, self.names, time_slice=0, time_slice_config=ts_config, distribution=1, distribution_config=dist_config)

        target = OrderedDict()
        target[0] = {
            "jobs": ['1', '2'],
            "resources": ['3', '5', '1', '2', '4']
        }
        target[240] = {
            "jobs": ['3', '4'],
            "resources": []
        }
        target[480] = {
            "jobs": ['5', '6'],
            "resources": []
        }
        target[720] = {
            "jobs": ['7', '8'],
            "resources": []
        }
        target[960] = {
            "jobs": ['9', '10'],
            "resources": []
        }

        self.check_data_for_timeslot(jd, target)

    def test_distribution_min_max_chunk_sizes(self):
        ts_config = {"duration": 240}
        dist_config = {"min_size": 0, "max_size": 5}

        jd = JobDistributor(self.database, self.shop_id, self.names, time_slice=0, time_slice_config=ts_config, distribution=2, distribution_config=dist_config, rand=random.Random(5755))

        target = OrderedDict()
        target[0] = {
            "jobs": ['1', '2', '3', '4'],
            "resources": ['3', '5', '1', '2', '4']
        }
        target[240] = {
            "jobs": ['5', '6'],
            "resources": []
        }
        target[480] = {
            "jobs": ['7'],
            "resources": []
        }
        target[720] = {
            "jobs": ['8', '9', '10'],
            "resources": []
        }

        self.check_data_for_timeslot(jd, target)

    @raises(ValueError)
    def test_new_data_for_timeslot_fail(self):
        ts_config = {"duration": 240}
        dist_config = {"min_size": 0, "max_size": 5}
        jd = JobDistributor(self.database, self.shop_id, self.names, time_slice=0, time_slice_config=ts_config, distribution=2, distribution_config=dist_config, rand=random.Random(5755))
        jd.new_data_for_timeslot(120)

    @raises(ValueError)
    def test_distribution_strat0_fail(self):
        ts_config = {"duration": 240}
        dist_config = {"doesNotHaveFunctionParameters": None}
        JobDistributor(self.database, self.shop_id, self.names, time_slice=0, time_slice_config=ts_config, distribution=1, distribution_config=dist_config)

    @raises(ValueError)
    def test_distribution_strat1_fail(self):
        ts_config = {"duration": 240}
        dist_config = {"chunk_size": 0}
        JobDistributor(self.database, self.shop_id, self.names, time_slice=0, time_slice_config=ts_config, distribution=1, distribution_config=dist_config)

    @raises(ValueError)
    def test_distribution_strat1_fail2(self):
        ts_config = {"duration": 240}
        dist_config = {"chunk_size": "NaN"}
        JobDistributor(self.database, self.shop_id, self.names, time_slice=0, time_slice_config=ts_config, distribution=1, distribution_config=dist_config)

    @raises(ValueError)
    def test_distribution_strat2_fail(self):
        ts_config = {"duration": 240}
        dist_config = {"min_size": 4, "max_size": 3}
        JobDistributor(self.database, self.shop_id, self.names, time_slice=0, time_slice_config=ts_config, distribution=2, distribution_config=dist_config)

    # -------------------------------------------------------------------------
    # Complex/Mixed Tests
    # -------------------------------------------------------------------------
    def test_complex1(self):
        ts_config = {"min_days": 1, "max_days": 5}
        dist_config = {"min_size": 0, "max_size": 1}

        seed = hash(os.urandom(128))
        rand = random.Random(seed)
        jd = JobDistributor(self.database, self.shop_id, self.names, time_slice=1, time_slice_config=ts_config, distribution=2, distribution_config=dist_config, rand=rand)

        hit_job_names = []
        res = OrderedDict()
        for t in jd.timeslots():
            res[t] = jd.new_data_for_timeslot(t)
            jobs, _ = res[t]
            for j in jobs:
                self.assertNotIn(j.name, hit_job_names, "Complex random test failed. To reproduce the results, use seeed: %s" % (seed))
                hit_job_names.append(j.name)

        self.assertListEqual(hit_job_names, self.names, "Complex random test failed. To reproduce the results, use seeed: %s" % (seed))


if __name__ == '__main__':
    unittest.main()
