# -*- coding: utf-8 -*-

from jobshop.sim.data_generator import create_simulation_result_metadata, create_jobs_and_resources
from jobshop.sim.environment import SimulationEnvironment
from jobshop.common.database import CommonDatabase
from jobshop.common.security import pwd_context

from collections import OrderedDict
import unittest
from nose.tools import raises
import os
import random


class Test_SqliteDatabaseConnection(unittest.TestCase):
    """
    """
    def setUp(self):
        cur_dir = os.path.dirname(__file__)
        self.test_dir = os.path.join(cur_dir, os.pardir, "files")
        self.test_file = os.path.join(self.test_dir, 'simulations.sqlite3')

    def tearDown(self):
        if os.path.isfile(self.test_file):
            os.remove(self.test_file)

    def test_file_db(self):
        CommonDatabase(database_file=self.test_file)
        self.assertTrue(os.path.isfile(self.test_file))

    def test_temp_db(self):
        CommonDatabase()


class Test_CommonDatabase(unittest.TestCase):
    """Advanced test cases."""
    def setUp(self):
        self.db = CommonDatabase()

    def test_all_job_names_list(self):

        config = [
            {
                "seed": 9544,
                "num_resources": 5,
                "num_jobs": 10,
                "min_ops": 2,
                "max_ops": 10,
                "op_min_res": 1,
                "op_max_res": 1
            },
            {
                "seed": 9544,
                "num_resources": 7,
                "num_jobs": 20,
                "min_ops": 1,
                "max_ops": 10,
                "op_min_res": 2,
                "op_max_res": 2
            },
            {
                "seed": 9544,
                "num_resources": 9,
                "num_jobs": 30,
                "min_ops": 4,
                "max_ops": 10,
                "op_min_res": 1,
                "op_max_res": 6
            },
        ]

        self.shop_ids = []
        for c in config:
            jobs, resources = create_jobs_and_resources(**c)
            shop_id = self.db.create_shop(jobs, resources)
            self.shop_ids.append(shop_id)

        all_jobs = self.db.all_job_names(self.shop_ids[0])
        self.assertIsInstance(all_jobs, list, "all_job_names should return a list.")
        self.assertEqual(len(all_jobs), len(set(all_jobs)), "The returned list contained duplicates.")

    def test_insert_simulation(self):
        pass

    # TODO: Implement more tests

    # --------------------------------------------------------------------------
    #  SHOPS
    # --------------------------------------------------------------------------
    def test_all_shops(self):
        all_shops = self.db.all_shops()
        self.assertCountEqual(all_shops, [])

        self.assertFalse(self.db.shop_exists(0))

        jobs, resources = create_jobs_and_resources()
        shop_id1 = self.db.create_shop(jobs, resources)
        self.assertTrue(self.db.shop_exists(shop_id1))
        # TODO: Implement

    # --------------------------------------------------------------------------
    #  USER CREDENTIALS
    # --------------------------------------------------------------------------
    def test_user_credentials(self):
        dummy_user = CommonDatabase._dummy_user
        dummy_pw = self.db.user_credentials(dummy_user)
        also_dummy_pw = self.db.user_credentials("not_a_user")
        self.assertEqual(dummy_pw, also_dummy_pw)

    def test_user(self):
        test_user = "test user 🤓"
        test_pw = "test123$∑🤘 2"

        # Test that the user is not in the database.
        self.assertFalse(self.db.user_exists(test_user))

        # Create the new user
        hashed_pw = pwd_context.hash(test_pw)
        self.db.create_user(
            test_user,
            hashed_pw
        )

        # Check that user exists works and returns true for the new user.
        self.assertTrue(self.db.user_exists(test_user))

        all_users = self.db.all_users()
        self.assertCountEqual(all_users, ['fjssp', test_user])

        returned_pw = self.db.user_credentials(test_user)
        # Check if it is not the pw of the dummy.
        dummy_user = CommonDatabase._dummy_user
        dummy_pw = self.db.user_credentials(dummy_user)
        self.assertNotEqual(returned_pw, dummy_pw)
        # now check the password against the one of the user.
        self.assertEqual(hashed_pw, returned_pw)
        self.assertTrue(pwd_context.verify(test_pw, returned_pw))

        # Now we update the password and check it again.
        new_password = "This one is not hashed. (Just for test reasons)"
        self.db.update_user_password(test_user, new_password)
        returned_pw = self.db.user_credentials(test_user)
        self.assertEqual(returned_pw, new_password)

        # Finally, delete the user.
        self.db.delete_user(test_user)
        all_users = self.db.all_users()
        self.assertCountEqual(all_users, ['fjssp'])

    def all_users(self):
        all_users = self.db.all_users()
        self.assertCountEqual(all_users, ["fjssp"])


if __name__ == '__main__':
    unittest.main()
