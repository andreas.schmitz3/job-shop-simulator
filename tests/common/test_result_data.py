# -*- coding: utf-8 -*-

from jobshop.common.result_data import SimulationResult, ResultTuple, SimulationMetadata, AnalyzeError
from jobshop.sim.data_generator import create_simulation_result_metadata, create_jobs_and_resources

import unittest
from nose.tools import raises


class Test_SimulationResult(unittest.TestCase):
    """Tests the `SimulationResult` class of the
    `jobshop.common.result_data` module.
    """
    def setUp(self):
        self.simres = SimulationResult()

    def test_append(self):
        rt1 = ResultTuple(op="Operation 1", start=10, finish=54, planned_start=None, planned_finish=None)
        rt2 = ResultTuple(op="Operation 2", start=72, finish=154, planned_start=None, planned_finish=None)
        self.simres.append("res1", rt1)
        self.simres.append("res1", rt2)
        self.assertEqual(self.simres.schedule["res1"], [rt1, rt2])


class Test_SimulationResulErrors(unittest.TestCase):
    """Error tests cases for the `SimulationResult` class of the
    `jobshop.common.result_data` module.
    """
    def setUp(self):
        self.simres = SimulationResult()

    def test_is_failed(self):
        self.assertTrue(self.simres.is_failed())

    def test_is_failed_2(self):
        jobs, resources = create_jobs_and_resources(num_jobs=1, min_ops=2, max_ops=2, num_resources=2, op_min_res=2, op_max_res=2)
        op1, op2 = jobs[0].operations
        rt1 = ResultTuple(op=op1, start=10, finish=54, planned_start=None, planned_finish=None)
        rt2 = ResultTuple(op=op2, start=53, finish=154, planned_start=None, planned_finish=None)
        self.simres.append(resources[0], rt1)
        self.assertTrue(self.simres.is_failed())
        self.simres.append(resources[1], rt2)
        self.assertTrue(self.simres.is_failed())

    @raises(ValueError)
    def test_equal_time(self):
        rt1 = ResultTuple(op="1", start=0, finish=0, planned_start=None, planned_finish=None)
        # raises an error since the finish time equals the start time.
        self.simres.append("res1", rt1)

    @raises(ValueError)
    def test_negative_time(self):
        rt1 = ResultTuple(op="1", start=10, finish=0, planned_start=None, planned_finish=None)
        # raises an error since the finish time is less than the start time.
        self.simres.append("res1", rt1)

    @raises(ValueError)
    def test_preceeding_start_time(self):
        rt1 = ResultTuple(op="1", start=10, finish=54, planned_start=None, planned_finish=None)
        rt2 = ResultTuple(op="2", start=53, finish=154, planned_start=None, planned_finish=None)
        self.simres.append("res1", rt1)
        # raises an error since the start time of the second result tuple is
        # less than the finish time of the first.
        self.simres.append("res1", rt2)

    @raises(AnalyzeError)
    def test_analyze_check_failed_error(self):
        self.simres.metric_objective_time()

    @raises(AnalyzeError)
    def test_simulation_metadata_fail(self):
        jobs, resources = create_jobs_and_resources(num_jobs=1, min_ops=2, max_ops=2, num_resources=2, op_min_res=2, op_max_res=2)
        op1, op2 = jobs[0].operations
        rt1 = ResultTuple(op=op1, start=10, finish=54, planned_start=None, planned_finish=None)
        rt2 = ResultTuple(op=op1, start=53, finish=154, planned_start=None, planned_finish=None)
        self.simres.append(resources[0], rt1)
        self.simres.append(resources[1], rt2)
        self.simres.metric_mean_job_flow_time()


class Test_SimulationResulPerformanceMetrics(unittest.TestCase):
    """Analyzer metric calculation test cases"""

    def setUp(self):
        self.simres = create_simulation_result_metadata(seed=5235, num_resources=5, num_jobs=6, min_ops=4, max_ops=10)

    def test_metric_methods(self):
        target = {
            'metric_objective_time': 3867,
            'metric_mean_job_flow_time': 1383.5,
            'metric_stdev_job_flow_time': 830.34,
            'metric_mean_operation_flow_time': 95.29,
            'metric_stdev_operation_flow_time': 27.52,
            'metric_mean_operation_wait_time': 0.0,
            'metric_stdev_operation_wait_time': 0.0,
            'metric_mean_machine_idle_time_percentage': 0.74,
            'metric_mean_total_machine_idle_time': 2329.0,
            'metric_average_queue_length': 1,
        }
        metrics = self.simres.metrics()
        for metric, value in metrics.items():
            self.assertEqual(metrics[metric], target[metric])
        self.assertAlmostEqual(len(target), len(metrics), "The number of entries in the returned list of metrics does not match the expected one.")

    def test_metric_caching(self):
        # The first time, the result is returned immediately, the second
        # time by the decorator.
        # The decorator creates an attribute with the name of the method
        # prefixed by an underscore (_). The attribute is used to store the
        # value of the metric.
        self.assertFalse(hasattr(self.simres, "_metric_mean_job_flow_time"))
        self.assertAlmostEqual(self.simres.metric_mean_job_flow_time(), 1383.5)
        self.assertAlmostEqual(self.simres._metric_mean_job_flow_time, 1383.5)
        self.assertAlmostEqual(self.simres.metric_mean_job_flow_time(), 1383.5)


class Test_SimulationMetdata(unittest.TestCase):
    """Analyzer metric calculation test cases"""

    def setUp(self):
        self.metadata = SimulationMetadata()

    def test_basic(self):
        jobs, resources = create_jobs_and_resources(num_jobs=5)
        target = {
            jobs[0]: 0,
            jobs[1]: 0,
            jobs[2]: 420,
            jobs[3]: 420,
            jobs[4]: 420
        }
        for job in jobs[:2]:
            self.metadata.add_job_entry_time(job, 0)
        for job in jobs[2:]:
            self.metadata.add_job_entry_time(job, 420)

        for job, entry in self.metadata.job_entry_times.items():
            self.assertEqual(target[job], entry)


if __name__ == '__main__':
    unittest.main()
