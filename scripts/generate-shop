#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pkg_resources
import os
from appdirs import AppDirs
import random
from datetime import datetime

from jobshop.common.database import CommonDatabase
from jobshop.sim.data_generator import create_jobs_and_resources, load_distribution

__appname__ = "JobShopSimulator"
__appauthor__ = "Andreas Schmitz"
__app_dirs__ = AppDirs(__appname__, __appauthor__)

# Default values
__distr_resource_performance__ = pkg_resources.resource_filename('jobshop', "share/distributions/resource_performance.csv")
__distr_processing_time__ = pkg_resources.resource_filename('jobshop', "share/distributions/processing_times.csv")
__distr_number_of_operations__ = pkg_resources.resource_filename('jobshop', "share/distributions/number_operations.csv")


def run():
    options = commandline_arguments()
    print("Shop generation started.")

    res_perf = load_distribution(options.resource_performance)
    proctime = load_distribution(options.processing_time)
    numops = load_distribution(options.number_of_operations)

    if not options.seed:
        options.seed = "%s" % random.getrandbits(64)

    proc_time_settings = {}
    if options.mean_processing_time is not None and options.stddev_processing_time is not None:
        proc_time_settings['mean_proc_time'] = options.mean_processing_time
        proc_time_settings['stddev_proc_time'] = options.stddev_processing_time
    else:
        proc_time_settings['processing_time_distr'] = proctime

    jobs, resources = create_jobs_and_resources(
        seed=options.seed,
        num_resources=options.resources,
        num_jobs=options.jobs,
        resource_performance_distr=res_perf,
        num_operations_distr=numops,
        op_min_res=options.min_flexibility,
        op_max_res=options.max_flexibility,
        **proc_time_settings
    )

    if options.verbose:
        print("Generated resources:")
        for res in resources:
            print("R_%s, Performance mean: %s, stddev: %s" % (res.name, res.performance_mean, res.performance_stddev))
        print("")
        print("Generated jobs:")
        for job in jobs:
            for op in job.operations:
                print("O_%s" % op.name)
                for res in op.resources:
                    print("   R_%s, p: %s" % (res, op.processing_time(res)))

    db = CommonDatabase(database_file=options.database)
    shop_id = db.create_shop(
        jobs=jobs,
        resources=resources,
        name=options.name,
        description=options.description,
        remark=options.remark,
        seed=options.seed
    )
    print("id of generated shop: %d" % shop_id)
    print("Shop generation finished.")


def commandline_arguments():
    """Parses the command line arguments and returns them bundled in an object
    that makes the values attribute accessible.

    Returns:
        Returns the parsed command line options

    """
    parser = argparse.ArgumentParser(
        description="This program is used to generate new FJSSP test instances and import them into the database."
    )
    parser.add_argument(
        'jobs',
        help='Number of jobs to be generated',
        type=int
    )
    parser.add_argument(
        'resources',
        help='Number of resources to be generated',
        type=int
    )
    parser.add_argument(
        'min_flexibility',
        help='Minimal number of resources an operation can be processed on.',
        type=int
    )
    parser.add_argument(
        'max_flexibility',
        help='Maximum number of resources an operation can be processed on.',
        type=int
    )
    parser.add_argument(
        '-v', '--verbose',
        help='Verbose output of the generated data.',
        action="store_true",
    )
    parser.add_argument(
        '--database',
        help='A path where the SQLite database is stored. (default: %(default)s)',
        default=os.path.join(__app_dirs__.user_data_dir, "fjss-sim.sqlite3"),
        metavar="file_path"
    )
    parser.add_argument(
        '--name',
        help='A test instance name. By default: "Test Instance <CurrentDateTime>"',
        default="Test Instance %s" % datetime.today(),
    )
    parser.add_argument(
        '--remark',
        help='A database internal remark for the created test instances.',
    )
    parser.add_argument(
        '--description',
        help='A user visible description of the test instance.',
    )
    parser.add_argument(
        '--seed',
        help='An optional seed that is used to initialize the PRNG. Is generated randomly if not provided',
    )
    parser.add_argument(
        '--processing-time',
        help='A processing time distribution. (default: %(default)s)',
        default=__distr_processing_time__,
        metavar='file_path'
    )
    parser.add_argument(
        '--resource-performance',
        help='A resource performance distribution. (default: %(default)s)',
        default=__distr_resource_performance__,
        metavar='file_path'
    )
    parser.add_argument(
        '--number-of-operations',
        help='A number of operations distribution. (default: %(default)s)',
        default=__distr_number_of_operations__,
        metavar='file_path'
    )
    parser.add_argument(
        '--mean-processing-time',
        help='Fixed mean processing time, instead of the usage of a distribution.',
        type=int
    )
    parser.add_argument(
        '--stddev-processing-time',
        help='Fixed processing time standard deviation, instead of the usage of a distribution.',
        type=int
    )

    options = parser.parse_args()
    return options


if __name__ == "__main__":
    run()
